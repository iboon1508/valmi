/**
ecpmSaveProducts = function(url, page=1){
    // alert("ecpmSaveProducts");
    form = $('product_edit_form');
    data = form.serialize();
    
    urlModif = url + "page/"+page;
    
    new Ajax.Request(urlModif, {
      onSuccess: function(transport) {
          var json = transport.responseText.evalJSON();
          
          if(json.success == true){
            // $('product_info_tabs_ecpm_content').innerHTML = json.msg+$('product_info_tabs_ecpm_content').innerHTML;
            // $('product_info_tabs_ecpm_content').insert({top:json.msg});
            
            $('product_info_tabs_ecpm_content').innerHTML = json.msg+json.matrix;
            super_product_linksJsObject.doFilter();
            setTimeout('ecpmHideMessages()', 5000);
            if(!json.stop){
                ecpmSaveProducts(url, parseInt(page+1));
            }
          }
      }, method: "post", parameters: data
    });
}

*/

ecpmUploadDone = function (a1,a2, additional) {
    var frame = null;
    if(additional){
        id = a1+'_'+a2+'_'+additional;  
    } else {
        id = a1+'_'+a2;
    }

    for (var i = 0; i < frames.length; i++)    if (frames[i].name == 'hidden_upload'+id)    frame=frames[i];
    if (frame) {
        ret = frame.document.getElementsByTagName("body")[0].innerHTML;
        if (ret.length) {
            var json = eval("("+ret+")");
            if(json.error == 0){
                var html = '<input type="hidden" name="ecpm[image]['+a1+']['+a2+']';
                    if(additional){
                        html = html + '['+additional+']';
                    }

                    html = html + '" value="'+json.file+'"/><input type="hidden" name="ecpm[url]['+a1+']['+a2+']';
                    if(additional){
                        html = html + '['+additional+']';
                    }

                    html = html + '" value="'+json.url+'"/><img class="ecpm_image" src="'+json.url+'" width="60"/>';
                var butt = $('img_'+id);
                if(butt){
                    butt.update(html);
                }
            }

            $('ecpmAjaxUploadForm'+id).remove();
        }
    }
 
    $('loading-mask').hide();
}

ecpmBrowseUploadBind = function (url,formkey,a,b,additional) { 
        if(additional){
            id = a+'_'+b+'_'+additional;
            value = a+','+b+','+additional;
        } else {
            id = a+'_'+b;
            value = a+','+b;
        }

        if(!$('ecpmAjaxUploadForm'+id)) {
            var html = '<FORM action="'+url+'" method="post" enctype="multipart/form-data" target="hidden_upload'+id+'" class="no-display" id="ecpmAjaxUploadForm'+id+'" onsubmit="javascript:$(\'loading-mask\').show();"><input id="ecpmFileUploader'+id+'" type="file" name="image" onchange="javascript:$(\'ecpmAjaxUploadFormSubmit'+id+'\').click();"/><input type="hidden" name="form_key" value="'+formkey+'" /><INPUT type="submit" name="submit" value="Upload" id="ecpmAjaxUploadFormSubmit'+id+'"/><IFRAME id="hidden_upload'+id+'" name="hidden_upload'+id+'" src="" onLoad="javascript:ecpmUploadDone('+value+');"></IFRAME></FORM>';
            $('html-body').insert(html);
        }

        $('ecpmFileUploader'+id).click();
}

ecpmBindUnbindValidation = function (ele) {
    ele.up('td').select('.input-text, textarea').each(
        function (item) {
        if(ele.checked){
            item.removeClassName('validation-passed');
            item.addClassName('ecpm-entry required-entry');
        } else {
            item.removeClassName('ecpm-entry required-entry');
            var advice = Validation.getAdvice('required-entry', item);
            if(advice) advice.hide();
        }
        }
    );
}

ecpmAddBrowseButton = function (ele) {
    if(ele.next().next().next()){
        ele.next().next().next().show();    
    }
}

ecpmToggleAllOptions = function (eObj) {
    var eChecked = eObj.checked ? true : false;
    $$('input.optionof_'+eObj.value).each(
        function (item) {
        item.checked = eChecked;
        }
    );
}

ecpmSelColRow = function (eObj, attrId) {
    var eChecked = eObj.checked ? true : false;
    $$('.'+eObj.value+'_'+attrId).each(
        function (item) {
        item.checked = eChecked;
        ecpmBindUnbindValidation(item);
        }
    );
}

ecpmUploadRow = function (eObj) {
    var eChecked = eObj.checked ? true : false;
    $$('.row_upload_button').each(
        function (item) {
        if(eChecked){
            item.show();
        } else {
            item.hide();
        }
        }
    );
}

ecpmDelRowColProduct = function (url) {
    if(window.confirm('Are you sure?')){
        var spids = nattr = '';
        $$('.ecpmRowColCheckBoxProduct:checked').each(
            function (item) {
            spids += ','+item.value;
            }
        );
        nattr = 'ids='+spids;
        form = $('product_edit_form'); 
        if($(form['next_step_matrix_data']))
        {
            nattr = '&next_step_matrix_data='+$(form['next_step_matrix_data']).getValue();
        }

        if($(form['middle_step_matrix_data']))
        {
            nattr += '&middle_step_matrix_data='+$(form['middle_step_matrix_data']).getValue();
        }

        if(spids != '')
        {
            spids = spids.substring(1);
            new Ajax.Request(
                url, {
                onSuccess: function (transport) {
                  var json = transport.responseText.evalJSON();
                  if(json.success == true){
                    $('product_info_tabs_ecpm_content').innerHTML = json.msg+json.matrix;
                    superProduct.links = $H(json.linkp);
                    superProduct.attributes = json.attributep;
                    superProduct.createAttributes();
                    superProduct.updateGrid();
                    super_product_linksJsObject.doFilter();
                    setTimeout('ecpmSetLimitToAssociatedGrid()', 4000);
                    setTimeout('ecpmCheckUncheckCheckbox()', 7000);
                    setTimeout('ecpmHideMessages()', 7000);
                  }
                }, method: "post", parameters: nattr
                }
            );
        }
    }
}

ecpmNext = function (url, skip) {
    var error = false, nattr = '';
    form = $('product_edit_form'); 
    $$('input.ecpm_attribute').each(
        function (items) {
        var itemsValue = items.value;
        if($$('.optionof_'+itemsValue+':checked').length == 0){
            error = true;
            return $break;
        }

        var i = 0;
        $$('.optionof_'+itemsValue+':checked').each(
            function (cItem) {
            nattr += '&nattr['+itemsValue+'][]='+cItem.value;
            }
        );
        }
    );
    
    if(error && skip){
        alert('Please select at least one option for each attribute.');
        return false;
    }
    
    if(skip == false && $(form['next_step_matrix_data'])){
        nattr += '&nattr='+$(form['next_step_matrix_data']).getValue();
    }
    
    new Ajax.Request(
        url, {
        onSuccess: function (transport) {
          var json = transport.responseText.evalJSON();
          if(json.success == true){
            $('product_info_tabs_ecpm_content').innerHTML = json.matrix;
          }
        }, method: "post", parameters: nattr.substring(1, nattr.length)
        }
    );
}

ecpmMiddle = function (url, skip) {
    form = $('product_edit_form'); 
    var nattr = 'nattr='+$(form['middle_step_matrix_data']).getValue();
    $$('.other_attr input:checked').each(
        function (items) {
        nattr += '&middle_step_matrix_data[]='+items.getValue();
        }
    );
    
    new Ajax.Request(
        url, {
        onSuccess: function (transport) {
          var json = transport.responseText.evalJSON();
          if(json.success == true){
            $('product_info_tabs_ecpm_content').innerHTML = json.matrix;
          }
        }, method: "post", parameters: nattr
        }
    );
}

// 
ecpmSaveProducts = function (url, page) {
    if(!page){
        page = 1;
        if(!(productForm.validator && productForm.validator.validate($$('.ecpm-entry')))) return false;
    }

    // alert("ecpmSaveProducts");
    form = $('product_edit_form');
    data = form.serialize();
    
    urlModif = url + "page/"+page;
    
    new Ajax.Request(
        urlModif, {
        onSuccess: function (transport) {
          
        // ecpmSaveProducts(url, parseInt(page+1));
          
        var json = transport.responseText.evalJSON();
          if(json.success == true){
            // $('product_info_tabs_ecpm_content').innerHTML = json.msg+$('product_info_tabs_ecpm_content').innerHTML;
            // $('product_info_tabs_ecpm_content').insert({top:json.msg});	
            if(json.stop == true){
                $('product_info_tabs_ecpm_content').innerHTML = json.msg+json.matrix;
                if(json.linkp.length != 0){
                    superProduct.links = $H(json.linkp);
                    superProduct.updateGrid();
                }

                super_product_linksJsObject.doFilter();
                setTimeout('ecpmSetLimitToAssociatedGrid()', 4000);
                setTimeout('ecpmCheckUncheckCheckbox()', 7000);
                setTimeout('ecpmHideMessages()', 7000);
                // setTimeout("$('product_edit_form').submit()", 6000);
                // setTimeout("ecpmSaveMainProduct()", 6000);
            }else{
                ecpmHideMessages();
                $('product_info_tabs_ecpm_content').insert({top:json.msg});    
                ecpmSaveProducts(url, parseInt(page+1));
            }
          }else if(json.success == false){
                ecpmHideMessages();
                $('product_info_tabs_ecpm_content').insert({top:json.msg});
            }
        }, method: "post", parameters: data
        }
    );
}

ecpmSaveMainProduct = function () {
    $$(".content-buttons-placeholder .form-buttons button:last").each(
        function (btn) {
        btn.click();
        }
    );
}

ecpmEditProduct = function (url) {
    var nattr = mattr = '';
    form = $('product_edit_form'); 
    if($(form['next_step_matrix_data']))
    {
        nattr = 'next_step_matrix_data='+$(form['next_step_matrix_data']).getValue();
    }

    if($(form['middle_step_matrix_data']))
    {
        mattr += 'middle_step_matrix_data='+$(form['middle_step_matrix_data']).getValue();
    }

    if(mattr != '') nattr += '&'+mattr;
    
    new Ajax.Request(
        url, {
        onSuccess: function (transport) {
          var json = transport.responseText.evalJSON();
          $('ecpm_edit_single_container').innerHTML = json.edit_single_form;
          $("ecpm_edit_single_container").show();
        }, method: "post", parameters: nattr
        }
    );
}

ecpmDeleteProduct = function (url) {
    var nattr = mattr = '';
    form = $('product_edit_form'); 
    if($(form['next_step_matrix_data']))
    {
        nattr = 'next_step_matrix_data='+$(form['next_step_matrix_data']).getValue();
    }

    if($(form['middle_step_matrix_data']))
    {
        mattr += 'middle_step_matrix_data='+$(form['middle_step_matrix_data']).getValue();
    }

    if(mattr != '') nattr += '&'+mattr;
    
    new Ajax.Request(
        url, {
        onSuccess: function (transport) {
          var json = transport.responseText.evalJSON(true);
          if(json.success == true){
            // $('product_info_tabs_ecpm_content').insert({top:json.msg});
            
            $('product_info_tabs_ecpm_content').innerHTML = json.msg+json.matrix;
            superProduct.links = $H(json.linkp);
            superProduct.updateGrid();
            super_product_linksJsObject.doFilter();
            setTimeout('ecpmSetLimitToAssociatedGrid()', 4000);
                setTimeout('ecpmCheckUncheckCheckbox()', 15000);
                setTimeout('ecpmHideMessages()', 7000);
            // setTimeout('ecpmCheckUncheckCheckbox()', 5000);
            // setTimeout('ecpmHideMessages()', 5000);
          }
        }, method: "post", parameters: nattr
        }
    );
}

ecpmSaveSingleProduct = function (url) {
    form = $('product_edit_form');
    data = form.serialize();
    
    new Ajax.Request(
        url, {
        onSuccess: function (transport) {
          var json = transport.responseText.evalJSON();
          // alert(json.success);
          if(json.success == true){
            // $('ecpm_edit_single_container').innerHTML = json.msg;
            ecpmHidePopup();
            $('product_info_tabs_ecpm_content').innerHTML = json.msg+json.matrix;
            setTimeout('ecpmHideMessages()', 5000);
          }
          
        }, method: "post", parameters: data
        }
    );
}

ecpmHidePopup = function () {
    ecpmCancelSaveSingleProduct();
}

ecpmCancelSaveSingleProduct = function () {
    $('ecpm_edit_single_container').hide();
}

ecpmHideMessages = function () {
    $$("#product_info_tabs_ecpm_content #messages").each(
        function (elem) {
        elem.remove();
        }
    );
}

ecpmShowDefaultValuesForm = function () {
    $('ecpm_default_values_form_container').show();
}

ecpmApplyDefaultFormValues = function () {
    $('ecpm_default_values_form_container').select('input,textarea,select').each(
        function (inputElem) {
        fieldName = inputElem.name;
        fieldNameToBeSearched = fieldName.sub('ecpm_default[', '');
        fieldNameToBeSearched = fieldNameToBeSearched.sub(']', '');
        
        $$('.ecpmRowColCheckBoxProduct:checked').each(
            function (checkedElements) {
            checkedElements.up('td').select('input,textarea,select').each(
                function (inputElem2) {
                inputElem2Name = inputElem2.name;
                if(inputElem2Name.match(fieldNameToBeSearched)){
                    if(inputElem2.value==""){
                        inputElem2.value = inputElem.value;
                    }
                }
                }
            );
            }
        );
        }
    );

    $('ecpm_default_values_form_container').hide();
}

ecpmCancelDefaultValuesForm = function () {
    $('ecpm_default_values_form_container').hide();
}

ecpmSetLimitToAssociatedGrid = function () {
    $$('#super_product_links select').each(
        function (slct) {
        slct.value = 200;
        if (typeof slct.onchange== "function") {
            slct.onchange.apply(slct);
        }

        return;
        }
    );
}

ecpmCheckUncheckCheckbox = function () {
    
    var chkBx = $("super_product_links_table").down(".headings .checkbox");
    super_product_linksJsObject.checkCheckboxes(chkBx);
    return;
}

