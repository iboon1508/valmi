<?php

include('conn.php');

if(isset($_POST['submit'])){
	$email = $_POST['email'];

	$owner = 'bhargav.iboon@gmail.com';
	$from = 'appiboon@gmail.com';

	$ToEmail = $_POST['email']; 
	$EmailSubject = 'Thank you for subscribing in VALMI 18:00'; 
	$mailheader = "From: ".$from."\r\n";
	$mailheader .= "Reply-To: ".$ToEmail."\r\n"; 
	$mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n"; 

	$MESSAGE_BODY .= '
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title></title>
			</head>
			<body>
				<div class="email-content" style="text-align: center">
					<div class="email-image">
						<img src="http://valmi.in/UCONE/images/thankyou.jpg" height="252" width="367" alt="email-image"/>
					</div>
					<div class="email-content" style="text-align: center">
						<p style="font-size=16px;font-weight: bold;font-family: IndieFlower;">
							We just wanted to say Thank you for being a part of "Valmi" family.
						</p>
						<p style="font-size=12px;font-family: IndieFlower;">
							You will be first to know about or launch, new releases, offers and promotions.
						</p>
						<p style="font-size=16px;font-weight: bold;font-family: IndieFlower;">
							Stay Tuned.
						</p>
					</div>
					<div calss="email-bottom">
						<div style="background-color: black;height: 150px;width: 150px;margin: 0 auto;">
							<img src="http://valmi.in/UCONE/images/logo.png" width="150" height="150"/> 
						</div>
						<div class="valmi-contact">
								<p>Email id: india@valmi.in,clientcare@valmi.in</p>
								<p>Contact No: +919898412313</p>
						</div>
					</div>
				</div>
			</body>
			</html>
				'; 
	mail($owner, "Subscripiton Request", "Subscripiton request from $ToEmail", $mailheader );
	mail($ToEmail, $EmailSubject, $MESSAGE_BODY, $mailheader) or die ("Failure");

	$store_id = 1;
	$subscriber_email = $_POST['email'];
	$subscriber_status = 1;

	$sql = "INSERT INTO newsletter_subscriber (store_id, subscriber_email, subscriber_status) VALUES ($store_id, '$subscriber_email', $subscriber_status)";

	if ($conn->query($sql) === TRUE) {
	    echo "Success";
	} else {
    	echo "Error: " . $sql . "<br>" . $conn->error;
	}

	$conn->close();

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Coming Soon 2</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<!--  -->
	<div class="simpleslide100" style="display:none;">
		<div class="simpleslide100-item bg-img1" style="background-image: url('images/bg01.jpg');"></div>
		<div class="simpleslide100-item bg-img1" style="background-image: url('images/bg02.jpg');"></div>
		<div class="simpleslide100-item bg-img1" style="background-image: url('images/bg03.jpg');"></div>
	</div>

	<div class="size1 overlay1">
		<!--  -->
		<div class="size1 flex-col-c-m p-l-15 p-r-15 p-t-50 p-b-50">
		
			<div class="valmi-logo">
				<img src="/UCONE/images/logo.png" height="300" width="300" alt="logo" />
			</div>
		
			<h3 class="l1-txt1 txt-center p-b-25">
				Coming Soon
			</h3>

			<p class="m2-txt1 txt-center p-b-48">
				Our website is under construction, follow us for update now!
			</p>

			<form class="w-full flex-w flex-c-m validate-form" action="" method="post">

				<div class="wrap-input100 validate-input where1" data-validate = "Valid email is required: ex@abc.xyz">
				
				<!--<input class="input100 placeholder0 s2-txt2" type="text" name="email" placeholder="Enter Email Address"> -->

				<input type="text" name="email" id="newsletter" title="Sign up for our newsletter" class="input-text required-entry validate-email input100 placeholder0 s2-txt2" placeholder="Enter Email Address">

					<span class="focus-input100"></span>
				</div>
				
				
				<button class="flex-c-m size3 s2-txt3 how-btn1 trans-04 where1" style="display: none;">
					Subscribe
				</button>
				<input type="submit" value="SUBSCRIBE" name="submit" class="flex-c-m size3 s2-txt3 how-btn1 trans-04 where1" /> 
			</form>
		</div>
	</div>



	

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/moment.min.js"></script>
	<script src="vendor/countdowntime/moment-timezone.min.js"></script>
	<script src="vendor/countdowntime/moment-timezone-with-data.min.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<script>
		$('.cd100').countdown100({
			/*Set Endtime here*/
			/*Endtime must be > current time*/
			endtimeYear: 0,
			endtimeMonth: 0,
			endtimeDate: 35,
			endtimeHours: 18,
			endtimeMinutes: 0,
			endtimeSeconds: 0,
			timeZone: "" 
			// ex:  timeZone: "America/New_York"
			//go to " http://momentjs.com/timezone/ " to get timezone
		});
	</script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>

<style>
@font-face {
  font-family: IndieFlower;
  src: url('fonts/indie-flower/IndieFlower.ttf');
}
</style>