function customersonline(url, hideafter) {
    new Ajax.Request(url, {
        method: 'GET',
        onSuccess: function (response) {
            if (response.responseText != '[]') {
                var data = JSON.parse(response.responseText);
                if (Object.getOwnPropertyNames(data).length > 0) {
                    var popups = new Element('div', {'class': 'mageworx-customersonline-popups'});
                    for (var i in data) {
                        var singlePopup = new Element('div', {'class': 'mageworx-customersonline-popup'});
                        var cnt = new Element('div', {'class': 'mageworx-customersonline-cnt'}).insert(new Element('div', {'class': 'mageworx-customersonline-' + i}).update(data[i]));
                        (function (pp) {
                            cnt.insert(new Element('span', {'class': 'mageworx-customersonline-close'}).update('&times;').observe('click', function () {
                                pp.remove();
                                if (!popups.childElements().length) {
                                    popups.remove();
                                }
                            }));
                        })(singlePopup);
                        popups.insert(singlePopup.insert(cnt));
                    }
                    $$('body').first().insert(popups);
                    if (hideafter > 0) {
                        setTimeout(function () {
                            popups.remove();
                        }, hideafter * 1000);
                    }
                }
            }
        }
    });
}