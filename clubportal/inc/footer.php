   
<footer class="footer footer-static footer-light navbar-shadow">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2018 <a class="text-bold-800 grey darken-2" href="../../../../../marketplace.envato.com/indexbf7d.html?ref=pixinvent" target="_blank">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-block d-md-inline-block d-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span></p>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="../../app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
	<!--DATATABLE JS-->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
		<script type="text/javascript" src="../../app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
		 <script src="../../app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/dataTables.autoFill.min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/dataTables.fixedColumns.min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/dataTables.select.min.js" type="text/javascript"></script>
	<script src="../../app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/jszip.min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/pdfmake.min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/vfs_fonts.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/tables/buttons.print.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
	 <script src="../../app-assets/vendors/js/charts/morris.min.js"  type="text/javascript"></script>
	  <script src="../../app-assets/vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
	<!--DATATABLE JS-->
	<!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="../../app-assets/vendors/js/ui/jquery.sticky.js"></script>
	
    
    <!-- END PAGE VENDOR JS-->
	 <script src="../../app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
	 <script src="../../app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
	  <script src="../../app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/forms/toggle/switchery.min.js" type="text/javascript"></script>
    <!-- BEGIN STACK JS-->
	
	  
    <script src="../../app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="../../app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="../../app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
	   <script src="../../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js" type="text/javascript"></script>
	 <script src="../../app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <!-- END STACK JS-->
    <script src="../../app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
	<script src="../../app-assets/js/scripts/forms/validation/form-validation.js" type="text/javascript"></script>
	<script src="../../app-assets/js/scripts/pickers/dateTime/bootstrap-datetime.js" type="text/javascript"></script>
		  
   <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="../../app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js"></script>
	<script src="../../app-assets/js/scripts/navs/navs.min.js" type="text/javascript"></script>
	<script src="../../app-assets/js/scripts/tables/datatables-extensions/datatable-autofill.min.js" type="text/javascript"></script>
	<script src="../../app-assets/js/scripts/tables/datatables/datatable-advanced.min.js" type="text/javascript"></script>
	<!-- START SELECT2 JS-->
	 <script src="../../app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
	
	  <script src="../../app-assets/vendors/js/timeline/horizontal-timeline.js" type="text/javascript"></script>
	 <script src="../../app-assets/js/scripts/pages/dashboard-ecommerce.min.js" type="text/javascript"></script>
	
	<!-- end SELECT2 JS-->