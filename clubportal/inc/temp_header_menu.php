<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border" role="navigation" data-menu="menu-wrapper">
      <!-- Horizontal menu content-->
      <div class="navbar-container main-menu-content" data-menu="menu-container">
        <!-- include includes/mixins-->
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
          <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="index-2.html" data-toggle="dropdown"><i class="ft-home"></i><span>DASHBOARD</span></a>
          </li>
          <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="ft-monitor"></i><span>MASTER</span></a>
            <ul class="dropdown-menu">
              <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/country/" data-toggle="dropdown">COUNTRY
                  </a>               
              </li>
              <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/state/" data-toggle="dropdown">STATE
                  </a>               
              </li>
			  <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/city/" data-toggle="dropdown">CITY
                  </a>               
              </li>
			  <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/area/" data-toggle="dropdown">AREA
                  </a>  
				</li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/society/" data-toggle="dropdown">SOCIETY
                  </a>               
              </li>
			<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/organization/" data-toggle="dropdown">ORGANIZATION</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/membership/" data-toggle="dropdown">MEMBERSHIP TYPE</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">MASTER NAME</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/referenceBy/" data-toggle="dropdown">REFERENCE BY</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/surname/" data-toggle="dropdown">SURNAME</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/group/" data-toggle="dropdown">GROUP</a>  
		      </li>
            </ul>
          </li>
          <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="ft-layout"></i><span>ATS</span></a>
            <ul class="dropdown-menu">
             <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/category/" data-toggle="dropdown">CATEGORY
                  </a>  
				</li>
			<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/clubMembershipMst/" data-toggle="dropdown">CLUB MEMBERSHIP</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">CLUB MASTER</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">EMAIL MANAGE</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">DATAENTRY FORM</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">UPDATE SOCIETY/AREA</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="corePersonmst.php" data-toggle="dropdown">CORE PERSON MASTER</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">CORE PERSON MAPPING</a>  
		      </li>
			 </ul>
          </li>
          <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="ft-zap"></i><span>GCCI</span></a>
            <ul class="dropdown-menu">
              <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">SYSTEM ENTRY
                  <submenu class="name"></submenu></a>
                <ul class="dropdown-menu">
                  <li data-menu=""><a class="dropdown-item" href="searchGCCIMembership.php" data-toggle="dropdown">GCCI MEMBERSHIP
                      <submenu class="name"></submenu></a>
                  </li>
                </ul>
              </li>
			   <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">REPORTS
                  <submenu class="name"></submenu></a>
                <ul class="dropdown-menu">
                  <li data-menu=""><a class="dropdown-item" href="content-grid.html" data-toggle="dropdown">GCCI REPORTS
                      <submenu class="name"></submenu></a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="dropdown nav-item" data-menu="megamenu"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="ft-box"></i><span>SECURITY</span></a>
            <ul class="dropdown-menu ">
             <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">COMPANY</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">USERS</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="../../pages/users/" data-toggle="dropdown">CREATE USER</a>  
		      </li>
            </ul>
          </li>
          <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="ft-bar-chart-2"></i><span>REPORTS</span></a>
             <ul class="dropdown-menu report">
             <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">1.ENVELOP LIST</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">2.ADDRESS LIST</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">3.TELEPHONE DIRECTORY</a>  
		      </li>
			  <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">4.EMAIL LIST</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">5.GENERAL LIST</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">6.MEMBERSHIP REFERENCEWISE REPORT</a>  
		      </li>
			  <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">7.MEMBERSHIP CATEGORYWISE REPORT</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">8.MEMBERSHIP CITYWISE REPORT</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">9.MEMBERSHIP SURNAMEWISE REPORT</a>  
		      </li>
			  <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">10.GROUPWISE REPORT</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">11.MEMBERSHIP AREA WISE REPORT</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">12.LABLE PRINTING REPORT</a>  
		      </li>
			  <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">13.MEMBERSHIP SOCIETY WISE</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">14.DATAENTRY REPORT</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">15.SOCIETYWISE LISTING REPORT</a>  
		      </li>
			  <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">16.SOCIETYWISE REFERENCE ALLOTMENT</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">17.REFERENCEWISE SOCIETYWISE SUMMARY</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">18.REFERENCEWISE SOCIETY REPORT</a>  
		      </li>
			  <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">19.GENERAL REPORT</a>  
		      </li>
				<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">20.COMMON REPORT</a>  
		      </li>
			  	<li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">21.CORE PERSONWISE MEMBER LIST</a>  
		      </li>
			  <li class="dropdown" data-menu="dropdown-submenu"><a class="dropdown-item " href="#" data-toggle="dropdown">22.SOCIETY WISE NO OF MEMBER LIST</a>  
		      </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /horizontal menu content-->
    </div>
		<?php if (isset($_SESSION['alert'])) { ?>
		<div class="alert alert-<?php echo  $_SESSION['alert'];?> alert-dismissible mb-2" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<?php echo  $_SESSION['msg'];?> 
						</div>
		<?php 
	 						$_SESSION['alert'] = NULL;
						 $_SESSION['msg'] = NULL; 	} ?>