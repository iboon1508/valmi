<?php 
include_once("inc/config.php");
@session_start();
//die;
 if(isset($_SESSION['username'])!=''){
	 header("Location:../dashboard/");
} 
if(isset($_POST['submit'])){
	
	 $username = mysqli_real_escape_string($obj->CONN, $_POST['user_name']);
    $password = md5($_POST['password']);
	$sql = "SELECT * FROM mst_user WHERE user_name = '" . $username . "' AND password = '" . $password . "' AND disflag='1'";
	$res = $obj->select($sql);
	if (count($res) > 0) {
	    if($res[0]['disflag'] =='1' ){
			 session_regenerate_id(true);
            $_SESSION['username'] = $res[0]['user_name'];
            $_SESSION['email'] = $res[0]['email_id'];
            $_SESSION['userid'] = $res[0]['user_id'];
			if($res[0]['user_image']==''){
				$_SESSION['profileImage'] = "placeholder.png";
			}else{
				$_SESSION['profileImage'] = $res[0]['user_image'];
			}
            $_SESSION['baseurl'] = '../../assets/uploadImages/';
			$role_id=explode(',',$res[0]['role_id']);
            $_SESSION['roleid'] = $role_id;
			
            header("Location:../dashboard/");
		}else{
           $msg = "<p style='color:red'>Oops! Your account is still inactive...</p>"; 
        }
        
    } else {
        $msg = "<p style='color:red'>Invalid username or password...</p>";
    }
} else {
    @session_start();
    @session_destroy();
}

?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  
<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/login-with-bg-image.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Jul 2018 08:29:57 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>CLUB LOGIN</title>
    <link rel="apple-touch-icon" href="../../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
    <link href="../../../../../fonts.googleapis.com/css9764.css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/icheck/custom.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/app.min.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/pages/login-register.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body class="horizontal-layout horizontal-menu 1-column  bg-full-screen-image menu-expanded blank-page blank-page" data-open="hover" data-menu="horizontal-menu" data-col="1-column">
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="flexbox-container">
    <div class="col-12 d-flex align-items-center justify-content-center">
        <div class="col-md-4 col-10 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                <div class="card-header border-0">
                    <div class="card-title text-center">
					<h1>CLUB LOGIN</h1>
                        <!--<img src="../../app-assets/images/logo/stack-logo-dark.png" alt="branding logo">-->
                    </div>
                </div>
                <div class="card-content">
                 
                    <div class="card-body">
                        <form class="form-horizontal" action="" method="post" novalidate>
						<div class="msg"><?php echo $msg; ?></div>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="user-name" name="user_name" placeholder="Your Username" required>
                                <div class="form-control-position">
                                    <i class="ft-user"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="password" name="password" class="form-control" id="user-password" placeholder="Enter Password" required>
                                <div class="form-control-position">
                                    <i class="fa fa-key"></i>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-md-6 col-12 float-sm-left text-center text-sm-right"><a href="recover-password.html" class="card-link">Forgot Password?</a></div>
                            </div>
                            <button type="submit" name="submit" class="btn btn-outline-primary btn-block"><i class="ft-unlock"></i> Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    
    <!-- BEGIN VENDOR JS-->
    <script src="../../app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="../../app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script type="text/javascript" src="../../app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
    <script src="../../app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
    <script src="../../app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN STACK JS-->
    <script src="../../app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="../../app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="../../app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="../../app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js"></script>
    <script src="../../app-assets/js/scripts/forms/form-login-register.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
  </body>

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/login-with-bg-image.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Jul 2018 08:29:58 GMT -->
</html>