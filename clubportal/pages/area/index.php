<?php 
include_once("inc/config.php");
	
if (isset($_GET["id"])) {
     $id = $_GET["id"];
    
   $sql = "SELECT * FROM `mst_area` WHERE area_id = '".$id."'";

    if ($_DEBUG == 1)
        echo $sql;
    $areaDetail = $obj->select($sql);
    $area_id = $areaDetail[0]['area_id'];
	$area = $areaDetail[0]['area'];
	$city_id = $areaDetail[0]['city_id'];
	$updated_by=$areaDetail[0]['updated_by'];
   
    
    if (isset($_GET['disp']) && $_GET['disp'] == "edt") {
		$heading = "Edit";
		$button = "Update";
		
    }elseif (isset($_GET['disp']) && $_GET['disp'] == "del") {
		$sqlset = "";
		$sqlarr = array();
		$i = 0;
		$updatedby = $_SESSION['username'];
		       
		//if ($updated_by != $updatedby) {
			$sqlarr[$i] = " updated_by = '" . $updatedby . "'";
			$i++;
		//}       
		$sqlarr[$i] = " updated_date = '" . $tdatetime . "'";
		$i++;
		$sqlarr[$i] = " disflag = '0'";
		$i++;
		for ($j = 0; $j < count($sqlarr); $j++)
			if ($j == (count($sqlarr) - 1))
					$sqlset .= $sqlarr[$j];
				else
					$sqlset .= $sqlarr[$j] . ",";
		$delsql = "UPDATE mst_area SET " . $sqlset . "  WHERE area_id = " . $id . "";
					if ($_DEBUG == 0)
					var_dump($delsql);
				$cdel = $obj->edit($delsql);
        //$delsql = "DELETE FROM mst_country WHERE country_id = '".$id."'";
        //echo $delsql; die;
       // $cdel = $obj->delete($delsql);
        
        $_SESSION['alert'] = "danger";
        $_SESSION['msg'] = "Area successfully deleted!";
        header("Location:../../pages/area/"); die;
    }

}
else{
	$button="Save";
}

if (isset($_POST['crtStore'])) {
		if (isset($_GET['disp']) && $_GET['disp'] == "edt") {
			
			$sqlset = "";
			$sqlarr = array();
			$i = 0;
			
			$area = mysqli_real_escape_string($obj->CONN, trim($_POST['area']));
			$city = mysqli_real_escape_string($obj->CONN, trim($_POST['city']));
			$updatedby = $_SESSION['username'];
			
		
			//	$edtsql = "UPDATE mst_country SET  WHERE country_id = " . $country_id . "";
			$edtsql ="UPDATE `mst_area` SET `area` = '".$area."', `city_id` = '".$city."', `updated_by` = '".$updatedby."', `updated_date` = '".$tdatetime."' WHERE `area_id` = ".$area_id."";
			if ($_DEBUG == 0)
					var_dump($edtsql);
				$edit = $obj->edit($edtsql);
			
			$_SESSION['alert'] = "success";
			$_SESSION['msg'] = "Area successfully Updated!";
		
			header("Location:../../pages/area/"); die;
			
			
		  
		}else {
			$city = mysqli_real_escape_string($obj->CONN, trim($_POST['city']));
			$area=mysqli_real_escape_string($obj->CONN, trim($_POST['area']));
	
			$inssql = "INSERT INTO `mst_area` (`area_id`, `area`, `city_id`, `isActive`, `created_by`, `created_date`, `updated_by`, `updated_date`, `disflag`) VALUES (NULL, '".$area."', '".$city."', '1', '". $_SESSION['username'] ." ', '".$tdatetime."', NULL, NULL,'1');";
		//	echo $inssql; die;
			$ins = $obj->insert($inssql);
					
			$_SESSION['alert'] = "success";
			$_SESSION['msg'] = "Area successfully Created!";
						
				header("Location:../../pages/area/"); die;
		}
	}

?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  
<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/table-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Jul 2018 08:37:04 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>AREA MASTER</title>
    <link rel="apple-touch-icon" href="../../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
    <link href="fonts.googleapis.com/css9764.css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/vendors.min.css">
	<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/selects/select2.min.css">
    <!-- END VENDOR CSS-->
    <!-- START DATATABLE CSS-->
	<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/autoFill.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/fixedColumns.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/select.dataTables.min.css">
    <!-- END DATATABLE CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/app.min.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/colors/palette-callout.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">

    <!-- fixed-top-->
   
<?php include("../../inc/nav_head.php");?>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- Horizontal navigation-->
     <?php include("../../inc/nav.php");?>
    <!-- Horizontal navigation-->

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">AREA MASTER</h3>
            <div class="row breadcrumbs-top">
            </div>
          </div>
        </div>
<div class="content-body"><!-- Basic Tables start -->
<div class="row">
		<div class="col-md-12">
	        <div class="card">
	            <div class="card-content collpase show">
	                <div class="card-body">
					<form class="form form-horizontal" method="post" action="">
	                    	<div class="form-body">
	                    		<div class="row">
	                    			<div class="col-md-4">
				                        <div class="form-group row">
											<label class="col-md-4 label-control">Area Name</label>
											<span class="required">*</span>
				                        	<div class="col-md-7">
				                        <input type="text"  required class="form-control border-primary" placeholder="Please Area Name" name="area" <?php if(isset($area)) echo "value = '" . $area . "'"; ?> required>
				                            </div>
				                        </div>
				                    </div>
									<div class="col-md-4">
										<div class="form-group row">
								
								<label class="col-md-3 label-control" >City</label>
								<span class="required">*</span>
								<div class="col-md-8">
								<?php
									$sql = "SELECT * FROM mst_city where disflag='1'";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$cityDetail = $obj->select($sql);
									?>
								<select class="select2 form-control" name="city" required>
									<option value="" selected>Please select City</option>
									<?php foreach($cityDetail as $cityDetail) { ?>
											<option value="<?php echo $cityDetail['city_id'];?>" <?php if(isset($city_id) && $city_id == $cityDetail['city_id']) echo "selected = selected"; ?>><?php echo $cityDetail['city']; ?></option>
									<?php }?>	
								</select>
							</div>
						</div>
								</div>
								<div class="col-md-3 buttons">
									  <button type="submit" name="crtStore"  class="btn btn-primary">
											<i class="fa fa-check-square-o"></i> <?php echo $button; ?>
										</button>
										<a href="../area/" class="btn btn-default m-t-15 waves-effect"><button type="button"   class="btn btn-primary">Cancel</button></a>
									</div>
		                        </div>
		                    </div>

	                       
	                    </form>

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<?php
		$sql = "SELECT s.*,c.* FROM mst_area s INNER JOIN mst_city c ON s.city_id = c.city_id where s.disflag='1' ORDER BY s.area ASC	";
		if ($_DEBUG == 1)
		{ echo $sql;}
		$areaDetail = $obj->select($sql);
			//print_r($stateDetail);
			
		?>
	<section id="autofill">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-content collapse show">
					<div class="card-body card-dashboard">
						<table class="table table-striped table-bordered file-export">
							<thead>
								<tr>
									<th>Id</th>
									<th>Area</th>
									<th>City</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php	foreach($areaDetail as $areaDetail) { ?>
								<tr>
									<td><?php echo $areaDetail['area_id'];?></td>
									<td><?php echo $areaDetail['area']; ?></td>
									<td><?php echo $areaDetail['city']; ?></td>
									<td><?php if($areaDetail['isActive']=='1') { echo "Active";}else{ echo "InActive";} ?></td>
									<td>
									<div class="fonticon-wrap" >
								<?php 	//if ($_GET['disp'] != "edt") { ?>
									<a type="" title="Edit" class="" href="index.php?disp=edt&amp;id=<?php echo $areaDetail['area_id'];?>"> <i class="ft-edit" title="Edit"></i></a>
								<?php// } ?>
									<a type="" title="Delete" href="index.php?disp=del&amp;id=<?php echo $areaDetail['area_id'];?>">  <i class="ft-delete" title="Delete"></i></a>
                               
                                  
                                </div></td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Id</th>
									<th>Area</th>
									<th>City</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Responsive tables end -->

        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
 <?php include("../../inc/footer.php");?>
    <!-- END PAGE LEVEL JS-->
  </body>

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/table-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Jul 2018 08:37:04 GMT -->
</html>