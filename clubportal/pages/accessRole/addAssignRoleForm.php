<?php 
include_once("inc/config.php");
$buttonTitle='Save';
if (isset($_GET["id"])) {
	if (isset($_GET['disp']) && $_GET['disp'] == "edt") {
		$buttonTitle='Update';
		$id = $_GET["id"];
	$sql = "SELECT r.*,a.* FROM mst_role r INNER JOIN  mst_access_role a ON a.role_id = r.role_id WHERE a.isActive='1' AND r.role_id=$id";
		
		$permissionDetail = $obj->select($sql);
		$view=array();
		$edit=array();
		$delete=array();
		foreach($permissionDetail as $permissionDetail){
			if($permissionDetail['view_access']=='1'){
				array_push($view,$permissionDetail['child_category_id']);
			}
			if($permissionDetail['add_edit_access']=='1'){
				array_push($edit,$permissionDetail['child_category_id']);
			}
			if($permissionDetail['delete_access']=='1'){
				array_push($delete,$permissionDetail['child_category_id']);
			}
		}
		
	
		
	}
}
if (isset($_GET["id"])) {
	if (isset($_GET['disp']) && $_GET['disp'] == "del") {
		$id = $_GET["id"];
		$sql="UPDATE `mst_membership` SET `disflag` = '0' WHERE `mst_membership`.`membership_id` = $id;";
		$ins = $obj->edit($sql);
		$_SESSION['alert'] = "danger";
		$_SESSION['msg'] = "Member successfully Deleted!";
		header("Location:../../pages/clubMembershipMst/"); die;
	}
}
if(isset($_POST['submit'])){
	if (isset($_GET['disp']) && $_GET['disp'] == "add") {
		$buttonTitle='Save';
		$role_id=$_POST['role_id'];
		$category_ids=$_POST['category_id'];
		$view=$_POST['view'];
		$edit=$_POST['edit'];
		$delete=$_POST['delete'];
		
		foreach($category_ids as $key=>$category_ids){
			if(isset($view[$key]))
			{
				$inssql="INSERT INTO `mst_access_role` (`access_role_id`, `child_category_id`, `role_id`, `view_access`, `add_edit_access`, `delete_access`, `isActive`, `created_by`, `created_date`, `updated_by`, `updated_date`, `disflag`) 
				VALUES (NULL, '".$category_ids."', '".$role_id."', '".$view[$key]."', '".$edit[$key]."', '".$delete[$key]."', '1','". $_SESSION['username'] ." ', '".$tdatetime."', NULL, NULL, '1');";	
				 $ins = $obj->insert($inssql);
			}
		}
		$_SESSION['alert'] = "success";
		$_SESSION['msg'] = "User role assign successfully!";
		header("Location:../../pages/accessRole/"); die;
	}			
		
			
		if (isset($_GET["id"])) {
					if (isset($_GET['disp']) && $_GET['disp'] == "edt") {
						
						$role_id=$_POST['role_id'];
						$category_ids=$_POST['category_id'];
						$view=$_POST['view'];
						$edit=$_POST['edit'];
						$delete=$_POST['delete'];
						$delsql="DELETE FROM mst_access_role where role_id=$role_id;";
						$cdel = $obj->delete($delsql);
						foreach($category_ids as $key=>$category_ids){
							if(isset($view[$key]))
							{
								$inssql="INSERT INTO `mst_access_role` (`access_role_id`, `child_category_id`, `role_id`, `view_access`, `add_edit_access`, `delete_access`, `isActive`, `created_by`, `created_date`, `updated_by`, `updated_date`, `disflag`) 
								VALUES (NULL, '".$category_ids."', '".$role_id."', '".$view[$key]."', '".$edit[$key]."', '".$delete[$key]."', '1','". $_SESSION['username'] ." ', '".$tdatetime."', NULL, NULL, '1');";	
								 $ins = $obj->insert($inssql);
							}
						}
						// contact person add detail
						//print_r($_POST);
						$_SESSION['alert'] = "success";
						$_SESSION['msg'] = "User role successfully Updated!";
						header("Location:../../pages/accessRole/"); die;
					
					}
				}
}
	


?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  
<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/table-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Jul 2018 08:37:04 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>PERMISSION MASTER</title>
    <link rel="apple-touch-icon" href="../../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
    <link href="fonts.googleapis.com/css9764.css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/vendors.min.css">
	<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/selects/select2.min.css">
	 <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- START DATATABLE CSS-->
	<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/autoFill.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/fixedColumns.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/select.dataTables.min.css">
    <!-- END DATATABLE CSS-->
	 <link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/pickers/daterange/daterange.min.css">
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/app.min.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/colors/palette-callout.min.css">
    <!-- END Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/validation/form-validation.css">
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">

    <!-- fixed-top-->
   
<?php include("../../inc/nav_head.php");?>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- Horizontal navigation-->
     <?php include("../../inc/nav.php");?>
    <!-- Horizontal navigation-->

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">PERMISSION MASTER</h3>
            <div class="row breadcrumbs-top">
            </div>
          </div>
       <div class="content-header-right col-md-6 col-12">
            <div class="media width-250 float-right">
              
             <div class="media-body media-right text-right">
               <a href="index.php"> <button type="submit" class="btn btn-warning btn-block">Search Role</button></a>
              </div>
            </div>
          </div>
        </div>
<div class="content-body"><!-- Basic Tables start -->
<div class="row">
		<div class="col-md-12">
	        <div class="card">
	            <div class="card-content collpase show">
	                <div class="card-body">
						<form class="form form-horizontal" method="post" action="">
	                    	<div class="form-body">
	                    <div class="row">
						  <div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control">Role<span class=" col-md-4 required">*</span></label>
								<div class="col-md-7">
								<?php
										$sql = "SELECT * FROM mst_role where disflag='1' AND isActive='1' ";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$roleDetail = $obj->select($sql);
										?>
										
									<select class="select2 form-control " <?php if(isset($id)) echo "disabled"; ?>  required id="club" name="role_id">
									<option value="">Select Role</option>
									<?php	foreach($roleDetail as $roleDetail) { ?>
									
										<option value="<?php echo $roleDetail['role_id'];?>" <?php if(isset($id) && $id == $roleDetail['role_id']) echo "selected = selected"; ?>><?php echo $roleDetail['role'];?></option>
									<?php } ?>
										
									</select>
									<?php if(isset($id)) echo "<input type='hidden' name='role_id' value='".$id."'>"; ?>
								</div>
							</div>
							</div>
								
							</div>
							<div class="row">
							<div class="col-md-1">
							</div>
							<?php
								$sql = "SELECT * FROM chl_headercategory where isActive='1' ";
								if ($_DEBUG == 1)
								{ echo $sql;}
								$categoryDetail = $obj->select($sql);
							?>
							<div class="col-md-8 table-responsive">
								<table border='1' class="table mb-0">
									<thead>
										<tr>
											<th>Category</th>
											<th>View &nbsp; &nbsp;<input type="checkbox" id="view" class="parent" data-group=".group1" /></th>
											<th>Add/Edit &nbsp; &nbsp;<input type="checkbox" id="edit" class="parent" data-group=".group2" /></th>
											<th>Delete &nbsp; &nbsp;<input type="checkbox" id="delete" class="parent" data-group=".group3" /></th>
										</tr>
									</thead>
									<tbody>
									<?php foreach($categoryDetail as $key=>$categoryDetail) { ?>
										<tr>
											<td><input type="hidden" name='category_id[<?php echo $categoryDetail['chl_category_id']; ?>]' value="<?php echo $categoryDetail['chl_category_id']; ?>"><?php echo strtoupper($categoryDetail['category_name']); ?></td>
											<td><input type="checkbox" <?php if($permissionDetail[$key]['child_category_id']==$categoryDetail['chl_category_id'] && $permissionDetail[0]['view_access']=='1'){ echo "checked='check'"; }?> id="view_<?php echo $categoryDetail['chl_category_id']; ?>" class="group1" name='view[<?php echo $categoryDetail['chl_category_id']; ?>]' value="1"></td>
											<td><input type="checkbox" id="edit_<?php echo $categoryDetail['chl_category_id']; ?>" name='edit[<?php echo $categoryDetail['chl_category_id']; ?>]' class="group2" value="1"></td>
											<td><input type="checkbox" id="delete_<?php echo $categoryDetail['chl_category_id']; ?>" name='delete[<?php echo $categoryDetail['chl_category_id']; ?>]' class="group3" value="1"></td>
										</tr>
										
									<?php }?>
									<tbody>
									<tfoot>
										<tr>
										<th>Category</th>
											<th>View</th>
											<th>Add/Edit</th>
											<th>Delete</th>
										</tr>
									</tfoot>
								</table>
							</div>
							</div>
				
								<div class="form-group " style="align:center;">
									  <div class="col-md-1">
				                       <button type="submit" name="submit" class="btn btn-primary">
											<i class="fa fa-check-square-o"></i> <?php echo $buttonTitle; ?>
										</button>
									
			                        </div>
		                        </div>
		                        
								</div>
	                    </form>

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	

        </div>
      </div>
    </div>
	
    <!-- ////////////////////////////////////////////////////////////////////////////-->
 <?php include("../../inc/footer.php");?>
    <!-- END PAGE LEVEL JS-->
  </body>
  <?php   
  if (isset($_GET["id"])) {
	if (isset($_GET['disp']) && $_GET['disp'] == "edt") {
	foreach($view as $view){
			echo '<script>$("#view_'.$view.'").prop("checked",true); </script>';
			if (in_array($view, $edit)){
				echo '<script>$("#edit_'.$view.'").prop("checked",true); </script>';
			}
			if (in_array($view, $delete)){
				echo '<script>$("#delete_'.$view.'").prop("checked",true); </script>';
			}
		}
	}
}		
			
	?>
  <script type="text/javascript">




$(".parent").each(function(index){
	var group = $(this).data("group");
	var parent = $(this);
	

	
	
	parent.change(function(){  //"select all" change 
	
		if($(this).prop("checked") == true){
              if(group=='.group2'){
				$("#view").prop('checked', true); 
				$(".group1").prop('checked', true);
			  }else if(group=='.group3'){
				$(".group1").prop('checked', true);
				$(".group2").prop('checked', true);
				$("#view").prop('checked', true); 
				$("#edit").prop('checked', true); 
			  }

            }
            else if($(this).prop("checked") == false){
				if(group=='.group2'){
					$(".group3").prop('checked', false);
					$(".group1").prop('checked', true);
					$("#view").prop('checked', true); 
					$("#delete").prop('checked', false); 
			  }else if(group=='.group3'){
				$("#view").prop('checked', true); 
				$("#edit").prop('checked', true); 
			  }else{
				$(".group2").prop('checked', false);
				$(".group3").prop('checked', false);
				$("#delete").prop('checked', false); 
				$("#edit").prop('checked', false);
			  }
			    //alert("Checkbox is unchecked.");
            }
	
		 $(group).prop('checked', parent.prop("checked"));
	});
	$(group).change(function(){ 
		parent.prop('checked', false);
		if ($(group+':checked').length == $(group).length ){
			parent.prop('checked', true);
		}
	});
});


 	$(document).ready(function(e) {
		$('input[type="checkbox"]').click(function(){
			var checkId=$(this).attr('id');
			if(checkId){
				var splitVal=checkId.split('_');
					if(splitVal[0]=='edit'){
					$("#view_"+splitVal[1]).prop('checked', true); 
					$("#delete_"+splitVal[1]).prop('checked', false);
				}else if(splitVal[0]=='delete'){
					$("#view_"+splitVal[1]).prop('checked', true); 
					$("#edit_"+splitVal[1]).prop('checked', true); 
					
				}else{
					$("#delete_"+splitVal[1]).prop('checked', false); 
					$("#edit_"+splitVal[1]).prop('checked', false); 
				}
			}
			
			
           
        });

});

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}
</script>
<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/table-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Jul 2018 08:37:04 GMT -->
</html>