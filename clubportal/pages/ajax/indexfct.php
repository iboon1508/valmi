<?php 
include_once("inc/config.php");
$country_id='';
//echo "ronak";
if(isset($_POST['contry_id'])){
	$country_id = $_POST['contry_id'];
	$sql = "SELECT * FROM mst_state where disflag='1' AND isActive='1' AND country_id= $country_id ORDER BY `state` ASC";
	if ($_DEBUG == 1)
	{ echo $sql;}
	$state = $obj->select($sql);
	$stateOption='';
	$stateOption.= "<option value=''>Select Your State</option>";
		foreach($state as $state) { 
		$stateOption.="<option value=".$state['state_id'].">".$state['state']."</option>";
	} 
	echo  $stateOption;
	die;
}
if(isset($_POST['state_id'])){
	$state_id = $_POST['state_id'];
	$sql = "SELECT * FROM mst_city where disflag='1' AND isActive='1' AND state_id= $state_id ORDER BY `city` ASC";
	if ($_DEBUG == 1)
	{ echo $sql;}
	$city = $obj->select($sql);
	$cityOption='';
	$cityOption.= "<option value=''>Select Your City</option>";
		foreach($city as $city) { 
		$cityOption.="<option value=".$city['city_id'].">".$city['city']."</option>";
	} 
	echo  $cityOption;
	die;
}
if(isset($_POST['city_id'])){
	$city_id = $_POST['city_id'];
	$sql = "SELECT * FROM mst_area where disflag='1' AND isActive='1' AND city_id= $city_id ORDER BY `area` ASC";
	if ($_DEBUG == 1)
	{ echo $sql;}
	$area = $obj->select($sql);
	$areaOption='';
	$areaOption.= "<option value=''>Select Your Area</option>";
		foreach($area as $area) { 
		$areaOption.="<option value=".$area['area_id'].">".$area['area']."</option>";
	} 
	echo  $areaOption;
	die;
}
if(isset($_POST['area_id'])){
	$area_id = $_POST['area_id'];
	$sql = "SELECT * FROM mst_society where disflag='1' AND isActive='1' AND area_id= $area_id ORDER BY `society` ASC";
	if ($_DEBUG == 1)
	{ echo $sql;}
	$society = $obj->select($sql);
	$societyOption='';
	$societyOption.= "<option value=''>Select Your Society</option>";
		foreach($society as $society) { 
		$societyOption.="<option value=".$society['society_id'].">".$society['society']."</option>";
	} 
	echo  $societyOption;
	die;
}

?>
<div class="row">
<div class="col-md-4">
  <div class="form-group row">
	  <label class="col-md-5 label-control">Country</label>
	  <div class="col-md-7">
	  <?php
			  $sql = "SELECT * FROM mst_country where disflag='1' AND isActive='1' ORDER BY `country` ASC";
			  if ($_DEBUG == 1)
			  { echo $sql;}
			  $countryDetail = $obj->select($sql);
			  ?>
		 <select class="select2 form-control " onChange="getfctState(this.value)" id="fctcountry" name="fctcountry">
		  <!--<select class="select2 form-control " id="country_list">-->
		  <option value="">Select Your Country</option>
		  <?php	foreach($countryDetail as $countryDetail) { ?>
		  
			  <option value="<?php echo $countryDetail['country_id'];?>" <?php if(isset($memberFactoryDetail[0]['country_id']) && $memberFactoryDetail[0]['country_id'] == $countryDetail['country_id']) echo "selected = selected"; ?>><?php echo $countryDetail['country'];?></option>
		  <?php } ?>
		  </select>
	  </div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="form-group row">
	  <label class="col-md-5 label-control">State<span class="required col-md-1">*</span></label>
	  <div class="col-md-7 ">
	   <select class="select2 form-control " id="fctstate" name="fctstate" onChange="getfctCity(this.value)">
	  	 <option value="">Select Your State</option>
		  </select>
	  </div>
  </div>
  </div> 	
	<div class="col-md-4">
	<div class="form-group row">
		<label class="col-md-5 label-control">City<span class="required col-md-1">*</span></label>
		<div class="col-md-7 ">
		<select class="select2 form-control " id="fctcity" name="fctcity" onChange="getfctArea(this.value)">
			<option value="">Select Your City</option>	
			</select>
		</div>
	</div>
	</div>
	<div class="col-md-4">
	<div class="form-group row">
		<label class="col-md-5 label-control">Area<span class="required col-md-1">*</span></label>
		<div class="col-md-7 ">
		<select class="select2 form-control " id="fctarea" name="fctarea" onChange="getfctSociety(this.value)">
			<option value="">Select Your Area</option>
		</select>
		</div>
	</div>
	</div>
	<div class="col-md-4">
	<div class="form-group row">
		<label class="col-md-5 label-control">Society<span class="required col-md-1">*</span></label>
		<div class="col-md-7 ">
		<select class="select2 form-control " id="fctsociety" name="fctsociety">
			<option value="">Select Your Society</option>	
		</select>
		</div>
	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group row">
			<label class="col-md-5 label-control">House No</label>
	<div class="col-md-7">
		<input type="text"  name="fcthouseNo" class="form-control border-primary" <?php if(isset($memberFactoryDetail[0]['houseNo'])) echo "value=".$memberFactoryDetail[0]['houseNo'];?> placeholder="Please enter House no.">
	</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group row">
			<label class="col-md-5 label-control" >Pin Code<span class=" col-md-4 required">*</span></label>
			<div class="col-md-7">
				<input type="number" name="fctPincode"  class="form-control border-primary" <?php if(isset($memberFactoryDetail[0]['pincode'])) echo "value=".$memberFactoryDetail[0]['pincode'];?> placeholder="Please enter Pin Code" >
			</div>
	</div>
	</div>
 </div> 	
<script>
function getfctState(contry_id) {
	console.log(contry_id);
	jQuery('#fctarea').html('');
	jQuery('#fctsociety').html('');
	jQuery('#fctcity').html('');
	if(contry_id){
            jQuery.ajax({
                type:'POST',
                url:'../ajax/indexfct.php',
                data:'contry_id='+contry_id,
                success:function(html){
					console.log(html);
                   jQuery('#fctstate').html(html);
                     
                }
            }); 
        }
}
function getfctCity(state_id) {
	jQuery('#fctsociety').html('');
	jQuery('#fctarea').html('');
	if(state_id){
            jQuery.ajax({
                type:'POST',
                url:'../ajax/indexfct.php',
                data:'state_id='+state_id,
                success:function(html){
					console.log(html);
                   jQuery('#fctcity').html(html);
                     
                }
            }); 
        }
}
function getfctArea(city_id) {
	if(city_id){
            jQuery.ajax({
                type:'POST',
                url:'../ajax/indexfct.php',
                data:'city_id='+city_id,
                success:function(html){
					console.log(html);
                   jQuery('#fctarea').html(html);
                     
                }
            }); 
        }
}
function getfctSociety(area_id) {
	if(area_id){
            jQuery.ajax({
                type:'POST',
                url:'../ajax/indexfct.php',
                data:'area_id='+area_id,
                success:function(html){
					console.log(html);
                   jQuery('#fctsociety').html(html);
                     
                }
            }); 
        }
}
</script>