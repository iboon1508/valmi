<?php 
include_once("inc/config.php");
$buttonTitle='Save';
if (isset($_GET["id"])) {
	if (isset($_GET['disp']) && $_GET['disp'] == "edt") {
		$buttonTitle='Update';
		$id = $_GET["id"];
		 $sql = "SELECT * FROM mst_membership WHERE 	membership_id = '".$id."' and disflag='1'";
		
		$memberDetail = $obj->select($sql);
		$category_ids=explode(",",$memberDetail[0]['category_ids']);
		$reference_ids=explode(",",$memberDetail[0]['reference_ids']);
		
		
		// get contact person detail
		$sql = "SELECT * FROM mst_gccicontactpersons WHERE 	membership_id = '".$id."' and disflag='1'";
		$memberPersonDetail = $obj->select($sql);
		// Gcci member id wise get office detail
		$sql = "SELECT * FROM mst_gccimemberofficedetail WHERE 	membership_id = '".$id."' and disflag='1'";
		$memberOffficeDetail = $obj->select($sql);
		// Gcci member id wise get factory detail
		$sql = "SELECT * FROM mst_gccimemberfactorydetail WHERE 	membership_id = '".$id."' and disflag='1'";
		$memberFactoryDetail = $obj->select($sql);
	}
}
if (isset($_GET["id"])) {
	if (isset($_GET['disp']) && $_GET['disp'] == "del") {
		$id = $_GET["id"];
		$sql="UPDATE `mst_membership` SET `disflag` = '0' WHERE `mst_membership`.`membership_id` = $id;";
		$ins = $obj->edit($sql);
		$_SESSION['alert'] = "danger";
		$_SESSION['msg'] = "Member successfully Deleted!";
		header("Location:../../pages/clubMembershipMst/"); die;
	}
}
if(isset($_POST['submit'])){
	if (isset($_GET['disp']) && $_GET['disp'] == "add") {
		$buttonTitle='Save';
		$organization_id = mysqli_real_escape_string($obj->CONN, trim($_POST['organization_id']));
		$membershipType_id = mysqli_real_escape_string($obj->CONN, trim($_POST['membershipType_id']));
		$membershipNo = mysqli_real_escape_string($obj->CONN, trim($_POST['membershipNo']));
		$memberFirstName = mysqli_real_escape_string($obj->CONN, trim($_POST['memberFirstName']));
		$memberMiddleName = mysqli_real_escape_string($obj->CONN, trim($_POST['memberMiddleName']));
		$memberLastName = mysqli_real_escape_string($obj->CONN, trim($_POST['memberLastName']));
		$country_id	 = mysqli_real_escape_string($obj->CONN, trim($_POST['country_id']));
		$state_id = mysqli_real_escape_string($obj->CONN, trim($_POST['state_id']));
		$city_id = mysqli_real_escape_string($obj->CONN, trim($_POST['city_id']));
		$area_id = mysqli_real_escape_string($obj->CONN, trim($_POST['area_id']));
		$society_id = mysqli_real_escape_string($obj->CONN, trim($_POST['society_id']));
		$houseNo = mysqli_real_escape_string($obj->CONN, trim($_POST['houseNo']));
		$pincode = mysqli_real_escape_string($obj->CONN, trim($_POST['pincode']));
		$mobileNo1 = mysqli_real_escape_string($obj->CONN, trim($_POST['mobileNo1']));
		$mobileNo2 = mysqli_real_escape_string($obj->CONN, trim($_POST['mobileNo2']));
		$landlineNo1 = mysqli_real_escape_string($obj->CONN, trim($_POST['landlineNo1']));
		$landlineNo2 = mysqli_real_escape_string($obj->CONN, trim($_POST['landlineNo2']));
		$email = mysqli_real_escape_string($obj->CONN, trim($_POST['email']));
		$kyRegister = mysqli_real_escape_string($obj->CONN, trim($_POST['kyRegister']));
		$cast = mysqli_real_escape_string($obj->CONN, trim($_POST['cast']));
		$website = mysqli_real_escape_string($obj->CONN, trim($_POST['website']));
		$dateOfBirth = mysqli_real_escape_string($obj->CONN, trim($_POST['dateOfBirth']));
		$spouseName = mysqli_real_escape_string($obj->CONN, trim($_POST['spouseName']));
		$dateOfAnniversary = mysqli_real_escape_string($obj->CONN, trim($_POST['dateOfAnniversary']));
		$profression = mysqli_real_escape_string($obj->CONN, trim($_POST['profression']));
		$companyName = mysqli_real_escape_string($obj->CONN, trim($_POST['companyName']));
		$category_ids=implode(",",$_POST['category_ids']);
		$reference_ids=implode(",",$_POST['reference_ids']);

		$inssql="INSERT INTO `mst_membership` (`membership_id`, `organization_id`, `membershipType_id`, `membershipNo`, `memberFirstName`, `memberMiddleName`, `memberLastName`, `country_id`, `state_id`, `city_id`, `area_id`, `society_id`, `houseNo`, `pincode`, `mobileNo1`, `mobileNo2`, `landlineNo1`, `landlineNo2`, `email`, `kyRegister`, `category_ids`, `cast`, `website`, `dateOfBirth`, `spouseName`, `dateOfAnniversary`, `profression`, `reference_ids`, `companyName`, `isActive`, `created_by`, `created_date`, `updated_by`, `updated_date`)	VALUES (NULL,  '".$organization_id."',  '".$membershipType_id."',  '".$membershipNo."',  '".$memberFirstName."',  '".$memberMiddleName."',  '".$memberLastName."',  '".$country_id."',  '".$state_id."',  '".$city_id."',  '".$area_id."',  '".$society_id."',  '".$houseNo."',  '".$pincode."',  '".$mobileNo1."',  '".$mobileNo2."',  '".$landlineNo1."',  '".$landlineNo2."',  '".$email."',  '".$kyRegister."',  '".$category_ids."',  '".$cast."',  '".$website."',  '".$dateOfBirth."',  '".$spouseName."',  '".$dateOfAnniversary."',  '".$profression."',  '".$reference_ids."',  '".$companyName."',  '1',  '". $_SESSION['username'] ." ', '".$tdatetime."', NULL, NULL);";
		$ins = $obj->insert($inssql);
			if($organization_id=='1'){
				$membership_id=$obj->lastInsertedId();
				// contact person add detail
				$surname_id =array_values(array_filter($_POST['surname_id']));
				$name = array_values(array_filter($_POST['contactName']));
				$mobile_no =array_values(array_filter($_POST['contactmobileno']));
				for($i=0;$i<sizeof($name);$i++){
					$inssql="INSERT INTO `mst_gccicontactpersons` (`contactPerson_id`, `membership_id`, `surname_id`, `name`, `mobile_no`, `isActive`, `created_by`, `created_date`, `updated_by`, `updated_date`, `disflag`) VALUES (NULL, '".$membership_id."', '".$surname_id[$i]."', '".$name[$i]."', '".$mobile_no[$i]."', '1', '". $_SESSION['username'] ."', '".$tdatetime."', NULL, NULL, '1');";
					$ins = $obj->insert($inssql);
				}
				/// GCCCI office detail insert 
				if($_POST['ofcPersonName']!=''){

					$officeSurname = mysqli_real_escape_string($obj->CONN, trim($_POST['officeSurname']));
					$ofcPersonName	 = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcPersonName']));
					$ofcPersonCast = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcPersonCast']));
					$ofccountry = mysqli_real_escape_string($obj->CONN, trim($_POST['ofccountry']));
					$ofcstate = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcstate']));
					$ofccity = mysqli_real_escape_string($obj->CONN, trim($_POST['ofccity']));
					$ofcarea = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcarea']));
					$ofcsociety = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcsociety']));
					$ofchouseNo = mysqli_real_escape_string($obj->CONN, trim($_POST['ofchouseNo']));
					$ofcPincode = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcPincode']));
					$ofcEmail = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcEmail']));
					$ofcMobile = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcMobile']));
					$ofcPhoneno = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcPhoneno']));
					$ofcFaxno = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcFaxno']));
					$inssql="INSERT INTO `mst_gccimemberofficedetail` (`memberOffice_id`, `membership_id`, `surname_id`, `name`, `cast`, `country_id`, `state_id`, `city_id`, `area_id`, `society_id`, `houseNo`, `pincode`, `email`, `mobileNo`, `poneNo`, `faxNo`, `isActive`, `created_by`, `created_date`, `updated_by`, `updated_date`, `disflag`) VALUES (NULL, '".$membership_id."', '".$officeSurname."', '".$ofcPersonName."', '".$ofcPersonCast."', '".$ofccountry."', '".$ofcstate."', '".$ofccity."', '".$ofcarea."', '".$ofcsociety."', '".$ofchouseNo."', '".$ofcPincode."', '".$ofcEmail."', '".$ofcMobile."', '".$ofcPhoneno."', '".$ofcFaxno."',  '1', '".$_SESSION['username']."', '".$tdatetime."', NULL, NULL, '1');";
					$ins = $obj->insert($inssql);
				}
					// GCCCI factory detail inserted
				if($_POST['fctName']!=''){
				
					$fctName = mysqli_real_escape_string($obj->CONN, trim($_POST['fctName']));
					$fctEmail	 = mysqli_real_escape_string($obj->CONN, trim($_POST['fctEmail']));
					$fctMobile = mysqli_real_escape_string($obj->CONN, trim($_POST['fctMobile']));
					$fctcountry = mysqli_real_escape_string($obj->CONN, trim($_POST['fctcountry']));
					$fctstate = mysqli_real_escape_string($obj->CONN, trim($_POST['fctstate']));
					$fctcity = mysqli_real_escape_string($obj->CONN, trim($_POST['fctcity']));
					$fctarea = mysqli_real_escape_string($obj->CONN, trim($_POST['fctarea']));
					$fctsociety = mysqli_real_escape_string($obj->CONN, trim($_POST['fctsociety']));
					$fcthouseNo = mysqli_real_escape_string($obj->CONN, trim($_POST['fcthouseNo']));
					$fctPincode = mysqli_real_escape_string($obj->CONN, trim($_POST['fctPincode']));
					$fctPhoneNo = mysqli_real_escape_string($obj->CONN, trim($_POST['fctPhoneNo']));
					$fctFaxNo = mysqli_real_escape_string($obj->CONN, trim($_POST['fctFaxNo']));
				
					$inssql="INSERT INTO `mst_gccimemberfactorydetail` (`memberFactory_id`, `membership_id`, `name`, `country_id`, `state_id`, `city_id`, `area_id`, `society_id`, `houseNo`, `pincode`, `email`, `mobileNo`, `poneNo`, `faxNo`, `isActive`, `created_by`, `created_date`, `updated_by`, `updated_date`, `disflag`) 
					VALUES (NULL, '".$membership_id."', '".$fctName."', '".$fctcountry."', '".$fctstate."', '".$fctcity."', '".$fctarea."', '".$fctsociety."', '".$fcthouseNo."', '".$fctPincode."', '".$fctEmail."', '".$fctMobile."', '".$fctPhoneNo."', '".$fctFaxNo."', '1', '".$_SESSION['username']."', '".$tdatetime."', NULL, NULL, '1');";
					$ins = $obj->insert($inssql);
				}

			}			
		
				$_SESSION['alert'] = "success";
				$_SESSION['msg'] = "Member successfully Created!";
				header("Location:../../pages/clubMembershipMst/"); die;
	}
	if (isset($_GET["id"])) {
		if (isset($_GET['disp']) && $_GET['disp'] == "edt") {
			
			$organization_id = mysqli_real_escape_string($obj->CONN, trim($_POST['organization_id']));
			$membershipType_id = mysqli_real_escape_string($obj->CONN, trim($_POST['membershipType_id']));
			$membershipNo = mysqli_real_escape_string($obj->CONN, trim($_POST['membershipNo']));
			$memberFirstName = mysqli_real_escape_string($obj->CONN, trim($_POST['memberFirstName']));
			$memberMiddleName = mysqli_real_escape_string($obj->CONN, trim($_POST['memberMiddleName']));
			$memberLastName = mysqli_real_escape_string($obj->CONN, trim($_POST['memberLastName']));
			$country_id	 = mysqli_real_escape_string($obj->CONN, trim($_POST['country_id']));
			$state_id = mysqli_real_escape_string($obj->CONN, trim($_POST['state_id']));
			$city_id = mysqli_real_escape_string($obj->CONN, trim($_POST['city_id']));
			$area_id = mysqli_real_escape_string($obj->CONN, trim($_POST['area_id']));
			$society_id = mysqli_real_escape_string($obj->CONN, trim($_POST['society_id']));
			$houseNo = mysqli_real_escape_string($obj->CONN, trim($_POST['houseNo']));
			$pincode = mysqli_real_escape_string($obj->CONN, trim($_POST['pincode']));
			$mobileNo1 = mysqli_real_escape_string($obj->CONN, trim($_POST['mobileNo1']));
			$mobileNo2 = mysqli_real_escape_string($obj->CONN, trim($_POST['mobileNo2']));
			$landlineNo1 = mysqli_real_escape_string($obj->CONN, trim($_POST['landlineNo1']));
			$landlineNo2 = mysqli_real_escape_string($obj->CONN, trim($_POST['landlineNo2']));
			$email = mysqli_real_escape_string($obj->CONN, trim($_POST['email']));
			$kyRegister = mysqli_real_escape_string($obj->CONN, trim($_POST['kyRegister']));
			$cast = mysqli_real_escape_string($obj->CONN, trim($_POST['cast']));
			$website = mysqli_real_escape_string($obj->CONN, trim($_POST['website']));
			$dateOfBirth = mysqli_real_escape_string($obj->CONN, trim($_POST['dateOfBirth']));
			$spouseName = mysqli_real_escape_string($obj->CONN, trim($_POST['spouseName']));
			$dateOfAnniversary = mysqli_real_escape_string($obj->CONN, trim($_POST['dateOfAnniversary']));
			$profression = mysqli_real_escape_string($obj->CONN, trim($_POST['profression']));
			$companyName = mysqli_real_escape_string($obj->CONN, trim($_POST['companyName']));
			$category_ids=implode(",",$_POST['category_ids']);
			$reference_ids=implode(",",$_POST['reference_ids']);
		
			$sql="UPDATE `mst_membership` SET `organization_id` = '".$organization_id."', `membershipType_id` = '".$membershipType_id."', `membershipNo` = '".$membershipNo."', `memberFirstName` = '".$memberFirstName."', `memberMiddleName` = '".$memberMiddleName."', `memberLastName` = '".$memberLastName."', `country_id` = '".$country_id."', `state_id` = '".$state_id."', `city_id` = '".$city_id."', `area_id` = '".$area_id."', `society_id` = '".$society_id."', `houseNo` = '".$houseNo."', `pincode` = '".$pincode."', `mobileNo1` = '".$mobileNo1."', `mobileNo2` = '".$mobileNo2."',  `landlineNo1` = '".$landlineNo1."', `landlineNo2` = '".$landlineNo2."', `email` = '".$email."', `category_ids` = '".$category_ids."', `cast` = '".$cast."', `website` = '".$website."', `dateOfBirth` = '".$dateOfBirth."', `spouseName` = '".$spouseName."', `dateOfAnniversary` = '".$dateOfAnniversary."',`profression` = '".$profression."', `reference_ids` = '".$reference_ids."', `companyName` = '".$companyName."', `updated_by` = '". $_SESSION['username'] ."', `updated_date` = '".$tdatetime."' WHERE `mst_membership`.`membership_id` = $id;";
			//die;
			$ins = $obj->edit($sql);
					// contact person add detail
					$contactPerson_id =array_values(array_filter($_POST['contactPerson_id']));
					$surname_id =array_values(array_filter($_POST['surname_id']));
					$name = array_values(array_filter($_POST['contactName']));
					$mobile_no =array_values(array_filter($_POST['contactmobileno']));
					for($i=0;$i<sizeof($contactPerson_id);$i++){
						$edtsql="UPDATE `mst_gccicontactpersons` SET `membership_id` = '".$id."', `surname_id` = '".$surname_id[$i]."', `name` = '".$name[$i]."', `mobile_no` = '".$mobile_no[$i]."',`updated_by` = '". $_SESSION['username'] ."', `updated_date` = '".$tdatetime."' WHERE `contactPerson_id` = $contactPerson_id[$i];";					
						$ins = $obj->edit($edtsql);
					}
				
					if($_POST['ofcPersonName']!=''){
	
						$officeSurname = mysqli_real_escape_string($obj->CONN, trim($_POST['officeSurname']));
						$ofcPersonName	 = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcPersonName']));
						$ofcPersonCast = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcPersonCast']));
						$ofccountry = mysqli_real_escape_string($obj->CONN, trim($_POST['ofccountry']));
						$ofcstate = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcstate']));
						$ofccity = mysqli_real_escape_string($obj->CONN, trim($_POST['ofccity']));
						$ofcarea = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcarea']));
						$ofcsociety = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcsociety']));
						$ofchouseNo = mysqli_real_escape_string($obj->CONN, trim($_POST['ofchouseNo']));
						$ofcPincode = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcPincode']));
						$ofcEmail = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcEmail']));
						$ofcMobile = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcMobile']));
						$ofcPhoneno = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcPhoneno']));
						$ofcFaxno = mysqli_real_escape_string($obj->CONN, trim($_POST['ofcFaxno']));
				echo 		$edtsql="UPDATE `mst_gccimemberofficedetail` SET `surname_id` = '".$officeSurname."', `name` = '".$ofcPersonName."', `cast` = '".$ofcPersonCast."', `country_id` = '".$ofccountry."', `state_id` =  '".$ofcstate."', `city_id` =  '".$ofccity."', `area_id` =  '".$ofcarea."', `society_id` =  '".$ofcsociety."', `houseNo` =  '".$ofchouseNo."', `pincode` =  '".$ofcPincode."', `email` =  '".$ofcEmail."', `mobileNo` =  '".$ofcMobile."', `poneNo` =  '".$ofcPhoneno."', `faxNo` =  '".$ofcFaxno."', `updated_by` = '".$_SESSION['username']."', `updated_date` = '".$tdatetime."' WHERE `membership_id` = $id;";
						$ins = $obj->edit($edtsql);
					}
						// GCCCI factory detail inserted
					if($_POST['fctName']!=''){
					
						$fctName = mysqli_real_escape_string($obj->CONN, trim($_POST['fctName']));
						$fctEmail	 = mysqli_real_escape_string($obj->CONN, trim($_POST['fctEmail']));
						$fctMobile = mysqli_real_escape_string($obj->CONN, trim($_POST['fctMobile']));
						$fctcountry = mysqli_real_escape_string($obj->CONN, trim($_POST['fctcountry']));
						$fctstate = mysqli_real_escape_string($obj->CONN, trim($_POST['fctstate']));
						$fctcity = mysqli_real_escape_string($obj->CONN, trim($_POST['fctcity']));
						$fctarea = mysqli_real_escape_string($obj->CONN, trim($_POST['fctarea']));
						$fctsociety = mysqli_real_escape_string($obj->CONN, trim($_POST['fctsociety']));
						$fcthouseNo = mysqli_real_escape_string($obj->CONN, trim($_POST['fcthouseNo']));
						$fctPincode = mysqli_real_escape_string($obj->CONN, trim($_POST['fctPincode']));
						$fctPhoneNo = mysqli_real_escape_string($obj->CONN, trim($_POST['fctPhoneNo']));
						$fctFaxNo = mysqli_real_escape_string($obj->CONN, trim($_POST['fctFaxNo']));
					
						$edtsql="UPDATE `mst_gccimemberfactorydetail` SET `name` = '".$fctName."', `country_id` = '".$fctcountry."',`state_id` = '".$fctstate."', `city_id` = '".$fctcity."', `area_id` = '".$fctarea."', `society_id` = '".$fctsociety."', `houseNo` = '".$fcthouseNo."', `pincode` = '".$fctPincode."', `email` = '".$fctEmail."', `mobileNo` = '".$fctMobile."', `poneNo` = '".$fctPhoneNo."', `faxNo` = '".$fctFaxNo."', `updated_by` = '".$_SESSION['username']."', `updated_date` = '".$tdatetime."' WHERE `membership_id` = $id;";
						$ins = $obj->edit($edtsql);
					}
	
			//print_r($_POST);
			$_SESSION['alert'] = "success";
			$_SESSION['msg'] = "Member Detail successfully Updated!";
			header("Location:../../pages/clubMembershipMst/"); die;
		
		}
	}
}

?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  
<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/table-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Jul 2018 08:37:04 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>CLUB MEMBERSHIP MASTER</title>
    <link rel="apple-touch-icon" href="../../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
    <link href="fonts.googleapis.com/css9764.css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/vendors.min.css">
	<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/selects/select2.min.css">
	 <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- START DATATABLE CSS-->
	<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/autoFill.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/fixedColumns.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/select.dataTables.min.css">
    <!-- END DATATABLE CSS-->
	 <link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/pickers/daterange/daterange.min.css">
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/app.min.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/colors/palette-callout.min.css">
    <!-- END Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/validation/form-validation.css">
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">

    <!-- fixed-top-->
   
<?php include("../../inc/nav_head.php");?>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- Horizontal navigation-->
     <?php include("../../inc/nav.php");?>
    <!-- Horizontal navigation-->

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">CLUB MEMBERSHIP</h3>
            <div class="row breadcrumbs-top">
            </div>
          </div>
       <div class="content-header-right col-md-6 col-12">
            <div class="media width-250 float-right">
              
             <div class="media-body media-right text-right">
               <a href="index.php"> <button type="submit" class="btn btn-warning btn-block">Search Membership</button></a>
              </div>
            </div>
          </div>
        </div>
<div class="content-body"><!-- Basic Tables start -->
<div class="row">
		<div class="col-md-12">
	        <div class="card">
	            <div class="card-content collpase show">
	                <div class="card-body">
						<form class="form form-horizontal" method="post" action="">
	                    	<div class="form-body">
	                    <div class="row">
						  <div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control">Membership Club<span class=" col-md-4 required">*</span></label>
								<div class="col-md-7">
								<?php
										$sql = "SELECT * FROM mst_organization where disflag='1' AND isActive='1' ORDER BY `organization` ASC";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$organizationDetail = $obj->select($sql);
										?>
									<select class="select2 form-control " required id="club" name="organization_id">
									<option value="">Select Your Club</option>
									<?php	foreach($organizationDetail as $organizationDetail) { ?>
									
										<option value="<?php echo $organizationDetail['organization_id'];?>" <?php if(isset($memberDetail[0]['organization_id']) && $memberDetail[0]['organization_id'] == $organizationDetail['organization_id']) echo "selected = selected"; ?>><?php echo $organizationDetail['organization'];?></option>
									<?php } ?>
										
									</select>
								</div>
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control">Membership Type<span class=" col-md-4 required">*</span></label>
								<div class="col-md-7">
								<?php
										$sql = "SELECT * FROM mst_membershiptype where disflag='1' AND isActive='1' ORDER BY `membershipType` ASC";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$membershipDetail = $obj->select($sql);
										?>
									<select class="select2 form-control " required name="membershipType_id">
									<option value="">Select Your Member Type</option>
									<?php	foreach($membershipDetail as $membershipDetail) { ?>
									
										<option value="<?php echo $membershipDetail['membershipType_id'];?>" <?php if(isset($memberDetail[0]['membershipType_id']) && $memberDetail[0]['membershipType_id'] == $membershipDetail['membershipType_id']) echo "selected = selected"; ?>><?php echo $membershipDetail['membershipType'];?></option>
									<?php } ?>
										
										
									</select>
								</div>
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Membership No<span class="required col-md-1">*</span></label>
								<div class="col-md-7">
									<input type="text" required class="form-control border-primary" placeholder="Please enter Membership no." name="membershipNo" <?php if(isset($memberDetail[0]['membershipNo'])) echo "value = ".$memberDetail[0]['membershipNo']; ?> >
								</div>
							</div>
								</div> 	
								
							</div>
							<div class="row">
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >First Name<span class="required col-md-1">*</span></label>
								<div class="col-md-7">
									<input type="text"  name="memberFirstName" required class="form-control border-primary" placeholder="Please enter First Name" <?php if(isset($memberDetail[0]['memberFirstName'])) echo "value = ".$memberDetail[0]['memberFirstName']; ?>>
								</div>
							</div>
								</div> 
								<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Middle Name<span class="required col-md-1">*</span></label>
								<div class="col-md-7">
									<input type="text" name="memberMiddleName" required class="form-control border-primary" placeholder="Please enter Middle Name"  <?php if(isset($memberDetail[0]['memberMiddleName'])) echo "value = ".$memberDetail[0]['memberMiddleName']; ?> >
								</div>
							</div>
								</div> 
						  <div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Lastname/Surname<span class=" col-md-4 required">*</span></label>
								<div class="col-md-7">
								<?php
										$sql = "SELECT * FROM mst_surname where disflag='1' AND isActive='1' ORDER BY `surname` ASC";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$surnameDetail = $obj->select($sql);
										?>
									<select class="select2 form-control " required name="memberLastName">
									<option value="">Select Your Surname</option>
									<?php	foreach($surnameDetail as $surnameDetail) { ?>
									
										<option value="<?php echo $surnameDetail['surname_id'];?>" <?php if(isset($memberDetail[0]['memberLastName']) && $memberDetail[0]['memberLastName'] == $surnameDetail['surname_id']) echo "selected = selected"; ?>><?php echo $surnameDetail['surname'];?></option>
									<?php } ?>
									</select>
								</div>
							</div>
							</div>
							</div>
							<?php	include_once('../ajax/index.php'); ?>
							
						<div class="row">
						  <div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Mobile No 1<span class="required col-md-1">*</span></label>
								<div class="col-md-7">
								<input type="text" name="mobileNo1" required class="form-control border-primary"  <?php if(isset($memberDetail[0]['mobileNo1'])) echo "value = ".$memberDetail[0]['mobileNo1']; ?> placeholder="Please enter Mobile No 1" >
								</div>
							</div>
								</div> 	
								 <div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Mobile No 2</label>
								<div class="col-md-7">
									<input type="number" name="mobileNo2" class="form-control border-primary"  <?php if(isset($memberDetail[0]['mobileNo2'])) echo "value = ".$memberDetail[0]['mobileNo2']; ?> placeholder="Please enter Mobile No 2" >
								</div>
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >LandLine No 1</label>
								<div class="col-md-7">
								<input type="text" name="landlineNo1" class="form-control border-primary"  <?php if(isset($memberDetail[0]['landlineNo1'])) echo "value = ".$memberDetail[0]['landlineNo1']; ?> placeholder="Please enter Land Line No 1" >
								</div>
							</div>
								</div> 	
							</div>
							<div class="row">
						 <div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control">LandLine No 2</label>
								<div class="col-md-7">
									<input type="number" name="landlineNo2" class="form-control border-primary"  <?php if(isset($memberDetail[0]['landlineNo2'])) echo "value = ".$memberDetail[0]['landlineNo2']; ?> placeholder="Please enter Land Line No 2" >
								</div>
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control">Email</label>
								<div class="col-md-7 ">
									<input type="email" name="email" id='txtEmail'  class="form-control border-primary"  <?php if(isset($memberDetail[0]['email'])) echo "value = ".$memberDetail[0]['email']; ?> placeholder="Please enter Email" >
								</div>
							</div>
								</div> 
								<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >KY Register</label>
								<div class="col-md-2">
									<input type="checkbox" name="kyRegister" value='1' class="form-control border-primary" id="kyregister"  <?php if(isset($memberDetail[0]['kyRegister'])) echo "value = ".$memberDetail[0]['kyRegister']; ?> placeholder="Please enter Profession" >
								</div>
							</div>
							</div>
							</div>
							<div class="row">
						  <div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Category<span class="required col-md-1">*</span></label>
								<div class="col-md-7">
								<?php
										$sql = "SELECT * FROM mst_category where disflag='1' AND isActive='1' ORDER BY `category` ASC";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$categoryDetail = $obj->select($sql);
										?>
								<select class="select2-placeholder-multiple form-control" multiple="multiple" name="category_ids[]">
									<option value="">Select Your Category</option>
									<?php	foreach($categoryDetail as $categoryDetail) { ?>
									
										<option value="<?php echo $categoryDetail['category_id'];?>" <?php if(in_array($categoryDetail['category_id'], $category_ids)) echo "selected = selected"; ?>><?php echo $categoryDetail['category'];?></option>
									<?php } ?>
									</select>
								</div>
							</div>
								</div> 	
								 <div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Cast</label>
								<div class="col-md-7">
									<input type="text" name="cast" class="form-control border-primary"  <?php if(isset($memberDetail[0]['cast'])) echo "value = ".$memberDetail[0]['cast']; ?> placeholder="Please enter Cast" >
								</div>
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Website</label>
								<div class="col-md-7">
									<input type="text" name="website" class="form-control border-primary"  <?php if(isset($memberDetail[0]['website'])) echo "value = ".$memberDetail[0]['website']; ?> placeholder="Please enter Website" >
								</div>
							</div>
							</div>
							</div>
							<div class="row">
						  
								 <div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control">	Date Of Birth<span class=" col-md-4 required">*</span></label>
								<div class="col-md-7">
									<input type='text' id="datetimepicker4" name="dateOfBirth"  class="form-control" placeholder="Please enter DateOfBirth"  <?php if(isset($memberDetail[0]['dateOfBirth'])) echo "value = ".$memberDetail[0]['dateOfBirth']; ?> />
								</div>
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control">Spouse Name</label>
								<div class="col-md-7">
								<input type="text" name="spouseName" class="form-control border-primary" placeholder="Please enter Spouse Name"  <?php if(isset($memberDetail[0]['spouseName'])) echo "value = ".$memberDetail[0]['spouseName']; ?> >
								</div>
							</div>
								</div> 	
								<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >	Date Of Anniversery<span class=" col-md-4 required">*</span></label>
								<div class="col-md-7">
									<input type='text' class="form-control" id="datetimepicker41" name="dateOfAnniversary"  <?php if(isset($memberDetail[0]['dateOfAnniversary'])) echo "value = ".$memberDetail[0]['dateOfAnniversary']; ?> placeholder="Please enter Anniversery Date"/>
								</div>
							</div>
							</div>
							</div>
							<div class="row">
						   <div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Profession</label>
								<div class="col-md-7">
									<input type="text" name="profression"  class="form-control border-primary" placeholder="Please enter Profession"  <?php if(isset($memberDetail[0]['profression'])) echo "value = ".$memberDetail[0]['profression']; ?> >
								</div>
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Reference By</label>
								<div class="col-md-7">
								<?php
										$sql = "SELECT * FROM  mst_referenceby where disflag='1' AND isActive='1' ORDER BY `name` ASC";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$referenceDetail = $obj->select($sql);
										?>
								<select class="select2-placeholder-multiple form-control" multiple="multiple" name="reference_ids[]">
									<option value="">Select Your Reference</option>
									<?php	foreach($referenceDetail as $referenceDetail) { ?>
									
										<option value="<?php echo $referenceDetail['reference_id'];?>" <?php if(in_array($referenceDetail['reference_id'], $reference_ids)) echo "selected = selected"; ?>><?php echo $referenceDetail['name'];?></option>
									<?php } ?>
									</select>
								</div>
							</div>
								</div> 
								<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Company Name</label>
								<div class="col-md-7">
									<input type="text" name="companyName"  class="form-control border-primary"  <?php if(isset($memberDetail[0]['companyName'])) echo "value = ".$memberDetail[0]['companyName']; ?> placeholder="Please enter Company Name." >
								</div>
							</div>
								</div>	
							</div>
							<div class="row" id="contactPerson" style="display:none;">
							<div class="col-md-4">
							<div class="form-group row">
						<!--hidden countact id pass-->
							<input type="hidden"  class="form-control border-primary" placeholder="Please enter Name."  <?php if(isset($memberPersonDetail[0]['contactPerson_id'])) echo "value=".$memberPersonDetail[0]['contactPerson_id'] ?> name="contactPerson_id[]">
							<input type="hidden"  class="form-control border-primary" placeholder="Please enter Name."  <?php if(isset($memberPersonDetail[1]['contactPerson_id'])) echo "value=".$memberPersonDetail[1]['contactPerson_id'] ?> name="contactPerson_id[]">
							<input type="hidden"  class="form-control border-primary" placeholder="Please enter Name."  <?php if(isset($memberPersonDetail[2]['contactPerson_id'])) echo "value=".$memberPersonDetail[2]['contactPerson_id'] ?> name="contactPerson_id[]">
								<label class="col-md-5 label-control" >Contact Person 1 Surname</label>
								<div class="col-md-7">
								<?php
										$sql = "SELECT * FROM mst_surname where disflag='1' AND isActive='1' ORDER BY `surname` ASC";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$surnameDetail = $obj->select($sql);
										?>
									<select class="select2 form-control " name="surname_id[]">
									<option value="">Select Your Surname</option>
									<?php	foreach($surnameDetail as $surnameDetail) { ?>
									
										<option value="<?php echo $surnameDetail['surname_id'];?>" <?php if($surnameDetail['surname_id']==$memberPersonDetail[0]['surname_id']) echo "selected = selected"; ?>><?php echo $surnameDetail['surname'];?></option>
									<?php } ?>
									</select>
								</div>
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Name</label>
								<div class="col-md-7">
								<input type="text"  class="form-control border-primary" placeholder="Please enter Name."  <?php if(isset($memberPersonDetail[0]['name'])) echo "value=".$memberPersonDetail[0]['name'] ?> name="contactName[]">
								</div>
							</div>
								</div> 
								<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Mobile No.<span class="required col-md-1">*</span></label>
								<div class="col-md-7">
									<input type="text"  class="form-control border-primary" placeholder="Please enter Mobile No." <?php if(isset($memberPersonDetail[0]['mobile_no'])) echo "value=".$memberPersonDetail[0]['mobile_no'] ?> name="contactmobileno[]">
								</div>
							</div>
								</div>	
								<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Contact Person 2 Surname<span class=" col-md-4 required">*</span></label>
								<div class="col-md-7">
								<?php
										$sql = "SELECT * FROM mst_surname where disflag='1' AND isActive='1' ORDER BY `surname` ASC";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$surnameDetail = $obj->select($sql);
										?>
									<select class="select2 form-control "    name="surname_id[]">
									<option value="">Select Your Surname</option>
									<?php	foreach($surnameDetail as $surnameDetail) { ?>
									
										<option value="<?php echo $surnameDetail['surname_id'];?>" <?php if($surnameDetail['surname_id']==$memberPersonDetail[1]['surname_id']) echo "selected = selected"; ?>><?php echo $surnameDetail['surname'];?></option>
									<?php } ?>
									</select>
								</div>
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Name</label>
								<div class="col-md-7">
								<input type="text"  class="form-control border-primary" placeholder="Please enter Name." <?php if(isset($memberPersonDetail[1]['name'])) echo "value=".$memberPersonDetail[1]['name'] ?> name="contactName[]">
								</div>
							</div>
								</div> 
								<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Mobile No.<span class="required col-md-1">*</span></label>
								<div class="col-md-7">
									<input type="text"  class="form-control border-primary" placeholder="Please enter Mobile No." <?php if(isset($memberPersonDetail[1]['mobile_no'])) echo "value=".$memberPersonDetail[1]['mobile_no'] ?> name="contactmobileno[]">
								</div>
							</div>
								</div>
								<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Contact Person 3 Surname<span class=" col-md-4 required">*</span></label>
								<div class="col-md-7">
									<?php
										$sql = "SELECT * FROM mst_surname where disflag='1' AND isActive='1' ORDER BY `surname` ASC";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$surnameDetail = $obj->select($sql);
										?>
									<select class="select2 form-control " name='surname_id[]'>
									<option value="">Select Your Surname</option>
									<?php	foreach($surnameDetail as $surnameDetail) { ?>
									
										<option value="<?php echo $surnameDetail['surname_id'];?>" <?php if($surnameDetail['surname_id']==$memberPersonDetail[2]['surname_id']) echo "selected = selected"; ?>><?php echo $surnameDetail['surname'];?></option>
									<?php } ?>
									</select>
								</div>
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Name</label>
								<div class="col-md-7">
								<input type="text"  class="form-control border-primary" placeholder="Please enter Name." <?php if(isset($memberPersonDetail[2]['name'])) echo "value=".$memberPersonDetail[2]['name'] ?> name="contactName[]">
								</div>
							</div>
								</div> 
								<div class="col-md-4">
							<div class="form-group row">
								<label class="col-md-5 label-control" >Mobile No.<span class="required col-md-1">*</span></label>
								<div class="col-md-7">
									<input type="text"  class="form-control border-primary" placeholder="Please enter Mobile No." <?php if(isset($memberPersonDetail[2]['mobile_no'])) echo "value=".$memberPersonDetail[2]['mobile_no'] ?> name="contactmobileno[]">
								</div>
							</div>
								</div>
							</div>
							<div class="tab" id="tabs" style="display:none;">
							<ul class="nav nav-tabs nav-underline no-hover-bg">
					<li class="nav-item">
						<a class="nav-link active" id="base-tab31" data-toggle="tab" aria-controls="tab31" 	href="#tab31" aria-expanded="true">Office Detail</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " id="base-tab32" data-toggle="tab" aria-controls="tab32" href="#tab32" aria-expanded="false">Factory Detail</a>
					</li>
				</ul>
						<div class="tab-content px-1 pt-1">
							<div  class="tab-pane active" id="tab31" aria-labelledby="base-tab31">
							<div class="row">
							  <div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control">Surname</label>
									<div class="col-md-7">
									<?php
										$sql = "SELECT * FROM mst_surname where disflag='1' AND isActive='1' ORDER BY `surname` ASC";
										if ($_DEBUG == 1)
										{ echo $sql;}
										$surnameDetail = $obj->select($sql);
									
										?>
										<select class="select2 form-control" name="officeSurname">
										<option value="">Select Your Surname</option>
									<?php	foreach($surnameDetail as $surnameDetail) { ?>
									
										<option value="<?php echo $surnameDetail['surname_id'];?>" <?php if($surnameDetail['surname_id']==$memberOffficeDetail[0]['surname_id']) echo "selected = selected"; ?>><?php echo $surnameDetail['surname'];?></option>
									<?php } ?>
									</select>
									</div>
								</div>
							</div> 	
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Contact Person Name</label>
									<div class="col-md-7">
										<input type="text" name="ofcPersonName" class="form-control border-primary" <?php if(isset($memberOffficeDetail[0]['name'])) echo "value=".$memberOffficeDetail[0]['name'] ?> placeholder="Please enter Website" >
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Cast</label>
									<div class="col-md-7">
										<input type="text" class="form-control border-primary" placeholder="Please enter Cast" name="ofcPersonCast" <?php if(isset($memberOffficeDetail[0]['cast'])) echo "value=".$memberOffficeDetail[0]['cast'] ?>>
									</div>
								</div>
							</div>
							</div>
							<?php	include_once('../ajax/indexofc.php'); ?>
							<div class="row">
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Email ID</label>
									<div class="col-md-7">
										<input name="ofcEmail" type="text" class="form-control border-primary" placeholder="Please enter Email ID" <?php if(isset($memberOffficeDetail[0]['email'])) echo "value=".$memberOffficeDetail[0]['email'];?> >
									</div>
								</div>
							</div>
							
							
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Mobile No</label>
									<div class="col-md-7">
										<input type="text" name="ofcMobile" class="form-control border-primary" placeholder="Please enter Mobile No" <?php if(isset($memberOffficeDetail[0]['mobileNo'])) echo "value=".$memberOffficeDetail[0]['mobileNo'];?> >
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Phone No</label>
									<div class="col-md-7"> 
										<input  name="ofcPhoneno" type="text" class="form-control border-primary" placeholder="Please enter Phone No" <?php if(isset($memberOffficeDetail[0]['poneNo'])) echo "value=".$memberOffficeDetail[0]['poneNo'];?>>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Fax No</label>
									<div class="col-md-7">
										<input type="text" name="ofcFaxno" class="form-control border-primary" placeholder="Please enter Fax No" <?php if(isset($memberOffficeDetail[0]['faxNo'])) echo "value=".$memberOffficeDetail[0]['faxNo'];?> >
									</div>
								</div>
							</div>
							</div>
							</div>


							
							<!--factory tab fields
							<div class="tab-pane" id="tab32" aria-labelledby="base-tab32">-->
							<div role="tabpanel" class="tab-pane" id="tab32"  aria-labelledby="base-tab32">
							<div class="row">
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Contact Person Name</label>
									<div class="col-md-7">
										<input type="text" name="fctName" class="form-control border-primary" <?php if(isset($memberFactoryDetail[0]['name'])) echo "value=".$memberFactoryDetail[0]['name'];?> placeholder="Please enter Website" >
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Email ID</label>
									<div class="col-md-7">
										<input type="text" name="fctEmail" class="form-control border-primary" <?php if(isset($memberFactoryDetail[0]['email'])) echo "value=".$memberFactoryDetail[0]['email'];?> placeholder="Please enter Email ID" >
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Mobile No</label>
									<div class="col-md-7">
										<input type="text" name="fctMobile" class="form-control border-primary" <?php if(isset($memberFactoryDetail[0]['mobileNo'])) echo "value=".$memberFactoryDetail[0]['mobileNo'];?> placeholder="Please enter Mobile No" >
									</div>
								</div>
							</div>
							
							</div>
							<?php	include_once('../ajax/indexfct.php'); ?>
							<div class="row">
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Phone No</label>
									<div class="col-md-7">
										<input type="text" name="fctPhoneNo" class="form-control border-primary" <?php if(isset($memberFactoryDetail[0]['poneNo'])) echo "value=".$memberFactoryDetail[0]['poneNo'];?> placeholder="Please enter Phone No" >
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row">
									<label class="col-md-5 label-control" >Fax No</label>
									<div class="col-md-7">
										<input type="text" name="fctFaxNo" class="form-control border-primary" <?php if(isset($memberFactoryDetail[0]['faxNo'])) echo "value=".$memberFactoryDetail[0]['faxNo'];?> placeholder="Please enter Fax No" >
									</div>
								</div>
							</div>
							
							</div>
							</div>
							</div>
						</div>
								<div class="form-group " style="align:center;">
									  <div class="col-md-1">
				                       <button type="submit" name="submit" class="btn btn-primary">
											<i class="fa fa-check-square-o"></i> <?php echo $buttonTitle; ?>
										</button>
									
			                        </div>
		                        </div>
		                        
								</div>
	                    </form>

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	

        </div>
      </div>
    </div>
	
    <!-- ////////////////////////////////////////////////////////////////////////////-->
 <?php include("../../inc/footer.php");?>
    <!-- END PAGE LEVEL JS-->
  </body>
  <script type="text/javascript">
 	jQuery(document).ready(function(){
<?php if (isset($_GET["id"])) {
			if (isset($_GET['disp']) && $_GET['disp'] == "edt") { ?>
		jQuery("#club").trigger("change");
		jQuery("#country").trigger(getState(<?php echo $memberDetail[0]['country_id'];?>));
		setTimeout("$('#state').val('<?php echo $memberDetail[0]['state_id'];?>').trigger('change');", 1000);	
		setTimeout("$('#city').val('<?php echo $memberDetail[0]['city_id'];?>').trigger('change');", 1100);	
		setTimeout("$('#area').val('<?php echo $memberDetail[0]['area_id'];?>').trigger('change');", 1200);	
		setTimeout("$('#society').val('<?php echo $memberDetail[0]['society_id'];?>').trigger('change');", 1300);	
		//GCCI member
	
		var club_id= $('option:selected', $(this)).val();
			if(club_id=='1'){
			//GCCI memeber office detail
			setTimeout("$('#ofccountry').trigger(getofcState(<?php echo $memberOffficeDetail[0]['country_id'];?>))", 1500);	
			setTimeout("$('#ofcstate').val('<?php echo $memberOffficeDetail[0]['state_id'];?>').trigger('change');", 1600);	
			setTimeout("$('#ofccity').val('<?php echo $memberOffficeDetail[0]['city_id'];?>').trigger('change');", 1700);	
			setTimeout("$('#ofcarea').val('<?php echo $memberOffficeDetail[0]['area_id'];?>').trigger('change');", 1800);	
			setTimeout("$('#ofcsociety').val('<?php echo $memberOffficeDetail[0]['society_id'];?>').trigger('change');", 1900);	
			//GCCI factory detail
			setTimeout("$('#fctcountry').trigger(getfctState(<?php echo $memberFactoryDetail[0]['country_id'];?>))", 2000);	
			setTimeout("$('#fctstate').val('<?php echo $memberFactoryDetail[0]['state_id'];?>').trigger('change');", 2100);	
			setTimeout("$('#fctcity').val('<?php echo $memberFactoryDetail[0]['city_id'];?>').trigger('change');", 2200);	
			setTimeout("$('#fctarea').val('<?php echo $memberFactoryDetail[0]['area_id'];?>').trigger('change');", 2300);	
			setTimeout("$('#fctsociety').val('<?php echo $memberFactoryDetail[0]['society_id'];?>').trigger('change');", 2400);	
			$('#kyregister').prop('checked', true);
			}
	<?php } 
		} ?>
		//jQuery("#ofccountry").trigger(getofcState());
	});
       jQuery("#club").change(function() {
            var club_id= $('option:selected', $(this)).val();
			if(club_id=='1'){
				$('#tabs').show();
				$('#contactPerson').show();
				
			}
			else{
				$('#tabs').hide();
				$('#contactPerson').hide();
			}
        });


		$(document).ready(function(e) {
   
   	 $("#txtEmail").change(function(){
   
        var sEmail = $('#txtEmail').val();
        if ($.trim(sEmail).length == 0) {
            alert('Please enter valid email address');
            
        }
        if (validateEmail(sEmail)) {
            $('#kyregister').prop('checked', true);
        }
        else {
            $('#kyregister').prop('checked', false);
         
        }
    });
});

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}
</script>
<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/table-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Jul 2018 08:37:04 GMT -->
</html>