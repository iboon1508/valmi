<?php 
include_once("inc/config.php");


$sql = "SELECT m.*,CONCAT(m.memberFirstName, ' ',s.surname) AS name,CONCAT(m.houseNo,' ',e.society, ' ' ,a.area,' ',y.city,' ',t.state,' ', c.country,' ',m.pincode) AS address,o.organization FROM mst_membership m INNER JOIN mst_organization o ON m.organization_id=o.organization_id INNER JOIN mst_surname s ON m.memberLastName=s.surname_id INNER JOIN mst_country c ON c.country_id=m.country_id INNER JOIN mst_state t ON t.state_id=m.state_id INNER JOIN mst_city y ON y.city_id=m.city_id INNER JOIN mst_area a ON a.area_id=m.area_id INNER JOIN mst_society e ON e.society_id=m.society_id AND m.disflag='1'";
if ($_DEBUG == 1)
    echo $sql;
$membersDetail = $obj->select($sql); ?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  
<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/table-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Jul 2018 08:37:04 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>CLUB MEMBERSHIP MASTER</title>
    <link rel="apple-touch-icon" href="../../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
    <link href="fonts.googleapis.com/css9764.css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/vendors.min.css">
	<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/selects/select2.min.css">
    <!-- END VENDOR CSS-->
    <!-- START DATATABLE CSS-->
	<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/autoFill.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/extensions/fixedColumns.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/select.dataTables.min.css">
    <!-- END DATATABLE CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/app.min.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/colors/palette-callout.min.css">
    <!-- END Page Level CSS-->
	<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/validation/form-validation.css">
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">

    <!-- fixed-top-->
   
<?php include("../../inc/nav_head.php");?>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- Horizontal navigation-->
     <?php include("../../inc/nav.php");?>
    <!-- Horizontal navigation-->

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">CLUB MEMBERSHIP</h3>
            <div class="row breadcrumbs-top">
            </div>
          </div>
       <div class="content-header-right col-md-6 col-12">
            <div class="media width-250 float-right">
              
             <div class="media-body media-right text-right">
                 <a href="addClubMembershipForm.php?disp=add"> <button type="submit" class="btn btn-warning btn-block">Add New</button></a>
              </div>
            </div>
          </div>
        </div>
<div class="content-body"><!-- Basic Tables start -->
<!--<div class="row">
		<div class="col-md-12">
	        <div class="card">
	            <div class="card-content collpase show">
	                <div class="card-body">
						<form class="form form-horizontal" >
	                    	<div class="form-body">
	                    		<div class="row">
								  <div class="col-md-5">
							<div class="form-group row">
								<label class="col-md-3 label-control" for="userinput1">Organization <span class=" col-md-4 required">*</span></label>
								<div class="col-md-8">
									<select class="select2 form-control " required>
									<option value="">Select Your Organization</option>
										<option value="1">Active</option>
										<option value="2">Pending</option>
										<option value="0">InActive</option>
										
									</select>
								</div>
							</div>
								</div>
								  <div class="col-md-3">
							<div class="form-group row">
								<span class="required col-md-1">*</span>
								<div class="col-md-10 ">
									<select class="select2 form-control" required>
									<option value="">Select Your Option</option>
										<option value="1">Active</option>
										<option value="2">Pending</option>
										<option value="0">InActive</option>
										
									</select>
								</div>
							</div>
								</div> 	
								<div class="col-md-3">
				                        <div class="form-group row">
				                        	<span class="required col-md-1">*</span>
				                        	<div class="col-md-9">
				                            	<input type="text" required data-validation-required-message="This field is required" id="userinput1" class="form-control border-primary" placeholder="Please enter search text" name="firstname">
				                            </div>
				                        </div>
				                    </div>
									<div class="form-group ">
									  <div class="col-md-1">
				                       <button type="submit" class="btn btn-primary">
											<i class="fa fa-check-square-o"></i> Save
										</button>
									
			                        </div>
		                        </div>
								</div>
								</div>
	                    </form>

	                </div>
	            </div>
	        </div>
	    </div>
	</div>-->
	<section id="autofill">
	<div class="row grid">
		<div class="col-12">
			<div class="card">
				<div class="card-content collapse show">
					<div class="card-body card-dashboard">
					<table class="table table-striped table-bordered file-export">
							<thead>
								<tr>
									<th>No.</th>
									<th>Member Name</th>
									<th>Organization</th>
									<th>Address	</th>
									<th>Mobile</th>
									<th>Email</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
						<?php foreach($membersDetail as $membersDetail) { ?>
								<tr>
									<td><?php echo $membersDetail['membershipNo']?></td>
									<td><?php echo $membersDetail['name']?></td>
									<td><?php echo $membersDetail['organization']?></td>
									<td><?php echo $membersDetail['address']?></td>
									<td><?php echo $membersDetail['mobileNo1']?></td>
									<td><?php echo $membersDetail['email']?></td>
									<td>
									<div class="fonticon-wrap" >
								<?php //	if ($_GET['disp'] != "edt") { ?>
									<a type="" title="Edit"  href="addClubMemberShipForm.php?disp=edt&amp;id=<?php echo $membersDetail['membership_id'];?>"> <i class="ft-edit" title="Edit"></i></a>
								<?php //} ?>
									<a type="" title="Delete"  href="addClubMemberShipForm.php?disp=del&amp;id=<?php echo $membersDetail['membership_id'];?>">  <i class="ft-delete" title="Delete"></i></a>
                               
                                  
                                </div></td>
								</tr>
						<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th>No.</th>
									<th>Member Name</th>
									<th>Organization</th>
									<th>Address	</th>
									<th>Mobile</th>
									<th>Email</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Responsive tables end -->

        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
 <?php include("../../inc/footer.php");?>
    <!-- END PAGE LEVEL JS-->
  </body>

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/table-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Jul 2018 08:37:04 GMT -->
</html>