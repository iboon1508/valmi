<?php

date_default_timezone_set("Asia/Calcutta");

@session_start();
if (!isset($_SESSION['username'])){
    header("Location:../../index.php"); die;
}

@ob_start();


include_once("../../config/class.MySQLCN.php");

$obj = new MySQLCN();

//include_once("../inc/functions.php");
$tdate = date("Y-m-d");

$tdatetime = date("Y-m-d H:i:s");

$appname = "Portal | Country";

$title = $appname;

$_DEBUG = 0;

if($_DEBUG == 1)

    error_reporting( E_ALL );

else

    error_reporting(0);

?>
