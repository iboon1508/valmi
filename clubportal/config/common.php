<?php

date_default_timezone_set("Asia/Calcutta");

@session_start();

@ob_start();

include_once("config/class.MySQLCN.php");

$obj = new MySQLCN();

$appname = "Portal";

$title = $appname;

$server = "nirmalwater.co.in";

$server_dir = "";

$tdate = date("Y-m-d");

$tdatetime = date("Y-m-d H:i:s");

$baseurl = "http://".$server."/".$server_dir;

$infoEmailId = "info@mosaicwebsolution.com";

$_DEBUG = 0;

if($_DEBUG == 1){

	error_reporting( E_ALL );

	ini_set('display_errors', 1);

	ini_set('display_startup_errors', 1);

	error_reporting(-1);

}

?>