<?php
class Best4Mage_Ecpm_Model_Attributes
{

    
    public static $_entityTypeId;
    public static $_productAttributes;
    public static $_productAttributeOptions;
    public static $_defAttrId = null;

    public function getAvailableSwatchAttributes($attrSetId)
    {
        if(is_array(self::$_productAttributes))
            return self::$_productAttributes;

        $resource = Mage::getSingleton('core/resource');
        $db = $resource->getConnection('core_read');
        $select = $db->select()->from($resource->getTableName('eav/entity_type'), 'entity_type_id')->where('entity_type_code=?', 'catalog_product')->limit(1);
        
        self::$_entityTypeId = $db->fetchOne($select);
        
        //$allAttrSetId = $db->fetchCol('select `attribute_set_id` FROM '.$resource->getTableName('eav/attribute_set').' WHERE `entity_type_id`='.self::$_entityTypeId);
        if(self::$_defAttrId == null){
            $defAttrIdQuery = 'select eea.`attribute_id` FROM '.$resource->getTableName('eav/entity_attribute').' eea JOIN '.$resource->getTableName('eav/attribute_group').' eag ON (eea.`attribute_set_id`=eag.`attribute_set_id` AND eea.`attribute_group_id`=eag.`attribute_group_id`) JOIN '.$resource->getTableName('catalog/eav_attribute').' cea ON (eea.`attribute_id`=cea.`attribute_id` AND (FIND_IN_SET("simple", cea.`apply_to`) OR cea.`apply_to` IS NULL)) WHERE eea.`attribute_set_id`='.$attrSetId.' AND  eea.`entity_type_id`='.self::$_entityTypeId;
            $defAttrId = $db->fetchCol($defAttrIdQuery);
        } else {
            $defAttrId = self::$_defAttrId;
        }
        
        $select = $db->select()->from(
            $resource->getTableName('eav/attribute'), array(
                    'title' => 'frontend_label',    
                    'id'    => 'attribute_id',      
                    'code'  => 'attribute_code',    
            )
        )
            ->where('frontend_input<>?', 'price')
            ->where('entity_type_id=?', self::$_entityTypeId)->where('frontend_label<>""')->where('find_in_set(is_user_defined, "1")')->order('frontend_label');

        foreach($db->fetchAll($select) as $s)
            if(in_array($s['id'], $defAttrId))
            {
                self::$_productAttributes[$s['id']] = array(
                    'title' => $s['title'],
                    'code'  => $s['code'],
                );
            }

        return self::$_productAttributes;
    }
    
    public function setDefaultAttributeId($atids)
    {
        if(is_string($atids)) $atids = Mage::helper('core')->jsonDecode($atids);
        if(is_array($atids) && count($atids) > 0) self::$_defAttrId = $atids;
        return $this;
    }
    
    public function _getAdditionalElementTypes()
    {
        $result = array(
            'price'    => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_price'),
            'weight'   => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_weight'),
            'gallery'  => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_gallery'),
            'image'    => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_image'),
            'boolean'  => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_boolean'),
            'textarea' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_helper_form_wysiwyg')
        );

        $response = new Varien_Object();
        $response->setTypes(array());
        Mage::dispatchEvent('adminhtml_catalog_product_edit_element_types', array('response' => $response));

        foreach ($response->getTypes() as $typeName => $typeClass) {
            $result[$typeName] = $typeClass;
        }

        return $result;
    }
}
