<?php

class Best4Mage_Ecpm_Model_System_Config_Source_Taxclasses
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $collection = Mage::getModel('tax/class')
            ->getCollection()
            ->setClassTypeFilter('PRODUCT')
            ->toOptionArray();
            
        array_unshift($collection, array('value' => 0, 'label'=>Mage::helper('adminhtml')->__('None')));
        return $collection;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $toOptionArray = $this->toOptionArray();
        $toArray = array();
        foreach($toOptionArray as $toOptionVal){
            $toArray[$toOptionVal['value']] = $toOptionVal['label'];
        }

        return $toArray;
    }

}
