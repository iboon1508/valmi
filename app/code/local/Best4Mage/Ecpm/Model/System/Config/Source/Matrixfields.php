<?php

class Best4Mage_Ecpm_Model_System_Config_Source_Matrixfields
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label'=>Mage::helper('adminhtml')->__('Show only checkboxes')),
            array('value' => 'name', 'label'=>Mage::helper('adminhtml')->__('Name')),
            array('value' => 'sku', 'label'=>Mage::helper('adminhtml')->__('SKU')),
            array('value' => 'qty', 'label'=>Mage::helper('adminhtml')->__('Qty')),
            array('value' => 'price', 'label'=>Mage::helper('adminhtml')->__('Price')),
            array('value' => 'special_price', 'label'=>Mage::helper('adminhtml')->__('Special Price')),
            array('value' => 'weight', 'label'=>Mage::helper('adminhtml')->__('Weight')),
            array('value' => 'status', 'label'=>Mage::helper('adminhtml')->__('Status')),
            array('value' => 'visibility', 'label'=>Mage::helper('adminhtml')->__('Visibility')),
            array('value' => 'image', 'label'=>Mage::helper('adminhtml')->__('Image')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $toOptionArray = $this->toOptionArray();
        $toArray = array();
        foreach($toOptionArray as $toOptionVal){
            $toArray[$toOptionVal['value']] = $toOptionVal['label'];
        }

        return $toArray;
    }

}
