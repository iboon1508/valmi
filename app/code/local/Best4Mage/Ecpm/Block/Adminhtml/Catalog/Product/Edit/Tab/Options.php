<?php

class Best4Mage_Ecpm_Block_Adminhtml_Catalog_Product_Edit_Tab_Options extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Options
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('b4m/ecpm/catalog/product/edit/tab/options.phtml');
    }
}
