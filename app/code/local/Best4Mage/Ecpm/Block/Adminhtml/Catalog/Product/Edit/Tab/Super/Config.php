<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Adminhtml catalog super product configurable tab
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Best4Mage_Ecpm_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Config extends Mage_Adminhtml_Block_Widget
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_matrixFields;
    /**
     * Initialize block
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setProductId($this->getRequest()->getParam('id'));
        $this->setTemplate('b4m/ecpm/catalog/product/edit/tab/super/config.phtml');
        $this->setId('ecpm_config_super_product');
        $this->setCanEditPrice(true);
        $this->setCanReadPrice(true);
    }

    /**
     * Retrieve Tab class (for loading)
     *
     * @return string
     */
    public function getTabClass()
    {
        return 'ajax';
    }

    /**
     * Check block is readonly
     *
     * @return boolean
     */
    public function isReadonly()
    {
        return (bool) $this->_getProduct()->getCompositeReadonly();
    }

    /**
     * Check whether attributes of configurable products can be editable
     *
     * @return boolean
     */
    public function isAttributesConfigurationReadonly()
    {
        return (bool) $this->_getProduct()->getAttributesConfigurationReadonly();
    }

    /**
     * Check whether prices of configurable products can be editable
     *
     * @return boolean
     */
    public function isAttributesPricesReadonly()
    {
        return $this->_getProduct()->getAttributesConfigurationReadonly() ||
            (Mage::helper('catalog')->isPriceGlobal() && $this->isReadonly());
    }

    /**
     * Prepare Layout data
     *
     * @return Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Super_Config
     */
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * Retrieve currently edited product object
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _getProduct()
    {
        return Mage::registry('current_product');
    }

    /**
     * Retrieve attributes data in JSON format
     *
     * @return string
     */
    public function getAttributesJson()
    {
        $attributes = $this->_getProduct()->getTypeInstance(true)
            ->getConfigurableAttributesAsArray($this->_getProduct());
        if(!$attributes) {
            return '[]';
        } else {
            // Hide price if needed
            foreach ($attributes as &$attribute) {
                if (isset($attribute['values']) && is_array($attribute['values'])) {
                    foreach ($attribute['values'] as &$attributeValue) {
                        if (!$this->getCanReadPrice()) {
                            $attributeValue['pricing_value'] = '';
                            $attributeValue['is_percent'] = 0;
                        }

                        $attributeValue['can_edit_price'] = $this->getCanEditPrice();
                        $attributeValue['can_read_price'] = $this->getCanReadPrice();
                    }
                }
            }
        }

        return Mage::helper('core')->jsonEncode($attributes);
    }

    /**
     * Retrieve Links in JSON format
     *
     * @return string
     */
    public function getLinksJson()
    {
        $products = $this->_getProduct()->getTypeInstance(true)
            ->getUsedProducts(null, $this->_getProduct());
        if(!$products) {
            return '{}';
        }

        $data = array();
        foreach ($products as $product) {
            $data[$product->getId()] = $this->getConfigurableSettings($product);
        }

        return Mage::helper('core')->jsonEncode($data);
    }

    /**
     * Retrieve configurable settings
     *
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getConfigurableSettings($product) 
    {
        $data = array();
        $attributes = $this->_getProduct()->getTypeInstance(true)
            ->getUsedProductAttributes($this->_getProduct());
        foreach ($attributes as $attribute) {
            $data[] = array(
                'attribute_id' => $attribute->getId(),
                'label'        => $product->getAttributeText($attribute->getAttributeCode()),
                'value_index'  => $product->getData($attribute->getAttributeCode())
            );
        }

        return $data;
    }

    /**
     * Retrieve Grid child HTML
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }

    /**
     * Retrieve Grid JavaScript object name
     *
     * @return string
     */
    public function getGridJsObject()
    {
        return $this->getChild('grid')->getJsObjectName();
    }

    /**
     * Retrieve Create New Empty Product URL
     *
     * @return string
     */
    public function getNewEmptyProductUrl()
    {
        return $this->getUrl(
            '*/*/new',
            array(
                'set'      => $this->_getProduct()->getAttributeSetId(),
                'type'     => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
                'required' => $this->_getRequiredAttributesIds(),
                'popup'    => 1
            )
        );
    }

    /**
     * Retrieve Create New Product URL
     *
     * @return string
     */
    public function getNewProductUrl()
    {
        return $this->getUrl(
            '*/*/new',
            array(
                'set'      => $this->_getProduct()->getAttributeSetId(),
                'type'     => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
                'required' => $this->_getRequiredAttributesIds(),
                'popup'    => 1,
                'product'  => $this->_getProduct()->getId()
            )
        );
    }

    /**
     * Retrieve Quick create product URL
     *
     * @return string
     */
    public function getQuickCreationUrl()
    {
        return $this->getUrl(
            '*/*/quickCreate',
            array(
                'product'  => $this->_getProduct()->getId()
            )
        );
    }

    /**
     * Retrieve Required attributes Ids (comma separated)
     *
     * @return string
     */
    protected function _getRequiredAttributesIds()
    {
        $attributesIds = array();
        $configurableAttributes = $this->_getProduct()
            ->getTypeInstance(true)->getConfigurableAttributes($this->_getProduct());
        foreach ($configurableAttributes as $attribute) {
            $attributesIds[] = $attribute->getProductAttribute()->getId();
        }

        return implode(',', $attributesIds);
    }

    /**
     * Escape JavaScript string
     *
     * @param string $string
     * @return string
     */
    public function escapeJs($string)
    {
        return addcslashes($string, "'\r\n\\");
    }

    /**
     * Retrieve Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('catalog')->__('ECPM');
    }

    /**
     * Retrieve Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('catalog')->__('ECPM');
    }

    /**
     * Can show tab flag
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Check is a hidden tab
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Show "Use default price" checkbox
     *
     * @return bool
     */
    public function getShowUseDefaultPrice()
    {
        return !Mage::helper('catalog')->isPriceGlobal()
            && $this->_getProduct()->getStoreId();
    }
    
    protected $_dataHelper;
    protected $_dataHelperClass="ecpm";
    protected function getDataHelper()
    {
        if(!$this->_dataHelper){
            $this->_dataHelper = Mage::helper('ecpm');
        }

        return $this->_dataHelper;
    }
    
    protected function getEcpmSaveProductsUrl()
    {
        if($this->_getProduct()->getId()){
            return Mage::helper('adminhtml')->getUrl("adminhtml/ecpm/saveproducts", array('set' => $this->_getProduct()->getAttributeSetId(), 'c_id'=>$this->_getProduct()->getId()));
        }else{
            return Mage::helper('adminhtml')->getUrl("adminhtml/ecpm/saveproducts", array('set' => $this->getRequest()->getParam('set')));
        }
    }
    
    public function getLoadedProduct()
    {
        return Mage::registry('current_product');
    }
    
    protected function getEditSingleProductUrl($productId)
    {
        return Mage::helper('adminhtml')->getUrl("adminhtml/ecpm/editsingle", array('id' => $productId, 'c_id'=>$this->_getProduct()->getId()));
    }
    
    protected function getDeleteSingleProductUrl($productId)
    {
        return Mage::helper('adminhtml')->getUrl("adminhtml/ecpm/deletesingle", array('id' => $productId, 'c_id'=>$this->_getProduct()->getId()));
    }
    
    protected function getSaveSingleProductUrl()
    {
        return Mage::helper('adminhtml')->getUrl("adminhtml/ecpm/savesingle");
    }
    
    protected function getMatrixFields()
    {
        if(!$this->_matrixFields){
            $matrixFields = $this->getDataHelper()->getConfig('matrix_fields');
            if($matrixFields==""){
                $this->_matrixFields = false;
            }else{
                $matrixFields = explode(",", $matrixFields);
                $this->_matrixFields = $matrixFields;
            }
        }

        return $this->_matrixFields;
    }
    
    protected function getMatrixFieldsTd($attribute1OptionValue, $attribute2OptionValue, $product=false)
    {
        $value = "";
        $tdHtml  = "";
        $dropdownElements = array('status', 'visibility');
        $matrixFields = $this->getMatrixFields();
        $addHiddenQty = false;
        $existingValue = "";
        $checkBoxClass = "ecpmRowColCheckBoxProduct row_".$attribute1OptionValue." column_".$attribute2OptionValue;
        if(!$matrixFields){
            // return only checkbox
            // with some indication/hidden element to specify that no stock management is needed.
            $checked = ($product)?"checked":"";
            $checkBoxVal = ($product)?$product->getId():"no_stock";
            $currentQty = ($product)?$product->getStockItem()->getQty():"no_stock";
            $tdHtml .= "<input type='checkbox' class='".$checkBoxClass."' name='ecpm[padd][".$attribute1OptionValue."][".$attribute2OptionValue."]' value='".$checkBoxVal."' ".$checked." onclick='ecpmBindUnbindValidation(this);'/><input type='hidden' name='ecpm[qty][".$attribute1OptionValue."][".$attribute2OptionValue."]' value='".$currentQty."'/><br/>";
        }else{
            $checked = ($product)?"checked":"";
            $checkBoxVal = ($product)?$product->getId():"no_stock";
            $tdHtml .= "<input type='checkbox' class='".$checkBoxClass."' name='ecpm[padd][".$attribute1OptionValue."][".$attribute2OptionValue."]' value='".$checkBoxVal."' ".$checked." onclick='ecpmBindUnbindValidation(this);'/><br />";
            
            if(!in_array('qty', $matrixFields)){ // if qty is not included in matrix fields, then add it as a hidden
                $addHiddenQty = true;
            }
            
            $labelArray = Mage::getModel('ecpm/system_config_source_matrixfields')->toArray();
            
            foreach($matrixFields as $matrixField){
                if($matrixField==""){
                    continue;
                }
                
                $fieldClass = "input-text";
                if($product){
                    if($matrixField=="qty"){
                        $existingValue = (int) $product->getstock_item()->getQty();
                        // $fieldClass = " qty";
                    }else{
                        $existingValue = $product->getData($matrixField);
                    }

                    $fieldClass .= " ecpm-entry required-entry";
                }
                
                if(in_array($matrixField, $dropdownElements)){
                    $options = $this->getDropdownFieldOptions($matrixField);
                    if($options){
                        $tdHtml .= $labelArray[$matrixField]."<br/><select class='".$fieldClass."' name='ecpm[".$matrixField."][".$attribute1OptionValue."][".$attribute2OptionValue."]'>";
                            $tdHtml .= "<option value='' >".__("")."</option>";
                        foreach($options as $key=>$value){
                            $selected = ($existingValue==$key)?"selected":"";
                            $tdHtml .= "<option value='".$key."' ".$selected.">".$value."</option>";
                        }

                        $tdHtml .= "</select><br/>";
                    }
                }elseif($matrixField == 'image'){
                    $haveImg = false;
                    $tdHtml .= $labelArray[$matrixField]."<br/>";
                    $frontImg = 1;
                    $tdHtml .= '<div id="img_'.$attribute1OptionValue.'_'.$attribute2OptionValue.'_'.$frontImg.'">';
                    if($product && $existingValue != '' && $existingValue != 'no_selection') $tdHtml .= '<a href="'.$product->getMediaConfig()->getMediaUrl($existingValue).'" onclick="superProduct.createPopup(this.href);return false;"><img class="ecpm_image" src="'.$product->getMediaConfig()->getMediaUrl($existingValue).'" width="60"/></a>';
                    $tdHtml .= '</div>'.$this->iButtonsHtml($attribute1OptionValue, $attribute2OptionValue, $frontImg);
                    // Start Additional image
					///////////////////////////
					if($product){
                            $product = Mage::getModel('catalog/product')->load($product->getId());
					$galleryimages = [];
					foreach ($product->getMediaGalleryImages() as $image) {
						if($product->getImage() != $image->getFile() && $product->getSmallImage() != $image->getFile() &&     $product->getThumbnail() != $image->getFile()){    
						$galleryimages[] = $image;	
						}
					}
					}
					///////////////////////////
                    for($additional=2;$additional<=4;$additional++)
					{
					//$additional = 2;
                    if($additional){
                        $displayImage = true;
                        //$tdHtml .= '<input type="button" id="addButton" class="ecpm_upload_add_button" title="'.Mage::helper('adminhtml')->__('Add').'" onclick="javascript:ecpmAddBrowseButton(this);" value=""/>';
                        $tdHtml .= "<br/>";
                        $tdHtml .= '<div id="img_'.$attribute1OptionValue.'_'.$attribute2OptionValue.'_'.$additional.'">';  
                        
                            if(isset($galleryimages[$additional-2]))
							{
								$image = $galleryimages[$additional-2];
								$tdHtml .= '<a href="'.$product->getMediaConfig()->getMediaUrl($image->getFile()).'" onclick="superProduct.createPopup(this.href);return false;"><img class="ecpm_image" src="'.$image->getUrl().'" width="60"/></a>';
							
                            }   
                        
 
                        $tdHtml .= '</div>'.$this->iButtonsHtml($attribute1OptionValue, $attribute2OptionValue, $additional, false).'<br/>';
                    }
					}

                    // End Additional image
                }else{
                    $tdHtml .= $labelArray[$matrixField]."<br/><input class='".$fieldClass."' type='text' name='ecpm[".$matrixField."][".$attribute1OptionValue."][".$attribute2OptionValue."]' value='".$existingValue."' /><br/>";
                }
            }
            
            if($addHiddenQty){
                $tdHtml .= "<input type='hidden' name='ecpm[qty][".$attribute1OptionValue."][".$attribute2OptionValue."]' value='no_stock' /><br/>";
            }
        }

        $tdHtml .= $this->getOtherAttributeHtml($product, "[".$attribute1OptionValue."][".$attribute2OptionValue."]");
        return $tdHtml;
    } 
    protected function getDropdownFieldOptions($field)
    {
        switch($field){
            case "status":
                return Mage::getSingleton('catalog/product_status')->getOptionArray();
            break;
            case "visibility":
                return Mage::getModel('catalog/product_visibility')->getOptionArray();
                $visibilityOptions = Mage::getModel('catalog/product_visibility')->getOptionArray();
                $visibilityOptionsKeyValue = array();
                foreach($visibilityOptions as $visibilityOption){
                    $visibilityOptionsKeyValue[$visibilityOption['value']] = $visibilityOption['label'];
                }

                return $visibilityOptionsKeyValue;
            break;
        }

        return false;
    }
    
    protected function getOtherAttributeHtml($product, $isPro)
    {
        $html = '';
        if($this->isOtherAttrEnabled() && $this->hasOtherAttributesArray())
        {
            $dataGenaralAttr = $this->getOtherAttributesArray();
            $form = new Varien_Data_Form();
            $form->setFieldNameSuffix('ecpm');
            foreach($dataGenaralAttr as $ATTRID => $sinGenAttr){
                //if(in_array($sinGenAttr['code'],$attributeCodes)) continue;
                
                $html .= $sinGenAttr['title'].'<br />';

                $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $sinGenAttr['code']);
                $inputType         = $attribute->getFrontend()->getInputType();
                $rendererClass  = $attribute->getFrontend()->getInputRendererClass();
                $defaultClass = $this->_getAdditionalElementTypes();
                if (!empty($rendererClass)) {
                    $className = $rendererClass;
                }
                elseif(array_key_exists($inputType, $defaultClass)) {
                    $className = $defaultClass[$inputType];
                }
                else {
                    $className = 'Varien_Data_Form_Element_'.ucfirst(strtolower($inputType));
                }
                
                $element = new $className(array(
                    'name'      => $attribute->getAttributeCode().$isPro,
                    'id'          => $attribute->getAttributeCode().$isPro,
                    'label'     => $attribute->getFrontend()->getLabel(),
                    'class'     => $attribute->getFrontend()->getClass(),
                    'required'  => $attribute->getIsRequired() && $product,
                    'note'      => $attribute->getNote(),
                ));
                $element->setEntityAttribute($attribute)->setForm($form);
                
                if ($element->getRequired() && $product) {
                    $element->addClass(' required-entry input-text');
                } else {
                    $element->removeClass('required-entry');
                }
                
                if($product)
                {
                    if($value = $product->getData($sinGenAttr['code'])){
                        $element->setValue($value);
                    }
                }
                
                if ($inputType == 'select') {
                    $element->setValues($attribute->getSource()->getAllOptions(true, true));
                } else if ($inputType == 'multiselect') {
                    $element->setValues($attribute->getSource()->getAllOptions(false, true));
                    $element->setCanBeEmpty(true);
                } else if ($inputType == 'date') {
                    $element->setImage($this->getSkinUrl('images/grid-cal.gif'));
                    $element->setFormat(Mage::app()->getLocale()->getDateFormatWithLongYear());
                } else if ($inputType == 'datetime') {
                    $element->setImage($this->getSkinUrl('images/grid-cal.gif'));
                    $element->setTime(true);
                    $element->setStyle('width:50%;');
                    $element->setFormat(
                        Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
                    );
                } else if ($inputType == 'multiline') {
                    $element->setLineCount($attribute->getMultilineCount());
                }

                $html .= $element->getElementHtml();
                $html .= '<br />';
            }
        }

        return $html;
    }
    
    public function _getAdditionalElementTypes()
    {
        $result = array(
            'price'    => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_price'),
            'weight'   => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_weight'),
            'gallery'  => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_gallery'),
            'image'    => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_image'),
            'boolean'  => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_boolean'),
            'textarea' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_helper_form_wysiwyg')
        );

        $response = new Varien_Object();
        $response->setTypes(array());
        Mage::dispatchEvent('adminhtml_catalog_product_edit_element_types', array('response' => $response));

        foreach ($response->getTypes() as $typeName => $typeClass) {
            $result[$typeName] = $typeClass;
        }

        return $result;
    }
    
    public function iButtonsHtml($a,$b, $additional = false, $hide = false)
    {
        $url = Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/catalog_product_gallery/upload');
        $formKey = $this->getFormKey();
        $html = '<input type="button"'.($hide ? ' style="display:none;"' : '').' id="ecpm_'.$a.'_'.$b;
        
        // Start add Additional image value
        if($additional){
            $html .= '_'.$additional;
        }

        // End add Additional image value
        
        $html .= '_browseUploadButton" class="ecpm_upload_button" title="'.Mage::helper('adminhtml')->__('Browse').'" onclick="javascript:ecpmBrowseUploadBind(\''.$url.'\',\''.$formKey.'\','.$a.','.$b;
        
        // Start add Additional image value
        if($additional){
            $html .= ','.$additional;
        }

        // End add Additional image value
        
        $html .= ');" value=""/>';
        return $html;
    }
    
    public function delButtonsHtml()
    {
        $url = Mage::helper('adminhtml')->getUrl("adminhtml/ecpm/deletechecked", array('c_id'=>$this->_getProduct()->getId()));;
        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->addData(
                array(
                'id'      => 'ecpmDeleteAllButton',
                'class'   => 'delete',
                'type'    => 'button',
                'label'   => Mage::helper('adminhtml')->__('Delete All Checked Product'),
                'onclick' => 'javascript:ecpmDelRowColProduct(\''.$url.'\');'
                )
            )
            ->toHtml();
        return $html;
    }
    
    public function getIsShowImage()
    {
        return in_array('image', $this->getMatrixFields());
    }
    
    public function getMatrixFieldHead($attr,$type,$single = false)
    {
        $headTr = "<table style='border:none;width: 100%;'><tr><td style='border:none;'>".$attr['label']."</td></tr>";
        $headTr .= "<tr><td style='border:none;'><input type='checkbox' value='".$type."' onclick='javascript:ecpmSelColRow(this,".$attr['value'].");' style='width: auto;'/></td></tr>";
        if($this->getIsShowImage() && (($single == false && $type == 'row') || ($single == true && $type == 'column'))) {
            $a = $attr['value'];
            $b = 0;
            $url = Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/catalog_product_gallery/upload');
            $formKey = $this->getFormKey();
            $headTr .= "<tr><td class='row_upload_button' style='border:none; display:none;'>".__("Image")."<div id='img_".$a."_".$b."_1'></div>";
            $headTr .= '<input type="button" id="ecpm_'.$a.'_'.$b.'_1_browseUploadButton" class="ecpm_upload_button" title="'.Mage::helper('adminhtml')->__('Browse').'" onclick="javascript:ecpmBrowseUploadBind(\''.$url.'\',\''.$formKey.'\','.$a.','.$b.',1);" value=""/>';
            //$headTr .= '<input type="button" id="addButton" class="ecpm_upload_add_button" title="'.Mage::helper('adminhtml')->__('Add').'" onclick="javascript:ecpmAddBrowseButton(this);" value=""/>';
            $headTr .= "<br/><div id='img_".$a."_".$b."_2'></div>";
            $headTr .= '<input type="button" id="ecpm_'.$a.'_'.$b.'_2_browseUploadButton" class="ecpm_upload_button" title="'.Mage::helper('adminhtml')->__('Browse').'" onclick="javascript:ecpmBrowseUploadBind(\''.$url.'\',\''.$formKey.'\','.$a.','.$b.',2);" value="" />';
            $headTr .= "<br/><div id='img_".$a."_".$b."_3'></div>";
            $headTr .= '<input type="button" id="ecpm_'.$a.'_'.$b.'_3_browseUploadButton" class="ecpm_upload_button" title="'.Mage::helper('adminhtml')->__('Browse').'" onclick="javascript:ecpmBrowseUploadBind(\''.$url.'\',\''.$formKey.'\','.$a.','.$b.',3);" value="" />';
			$headTr .= "<br/><div id='img_".$a."_".$b."_4'></div>";
            $headTr .= '<input type="button" id="ecpm_'.$a.'_'.$b.'_4_browseUploadButton" class="ecpm_upload_button" title="'.Mage::helper('adminhtml')->__('Browse').'" onclick="javascript:ecpmBrowseUploadBind(\''.$url.'\',\''.$formKey.'\','.$a.','.$b.',4);" value="" />';
			$headTr .= "</td></tr>";
        }

        $headTr .= "</table>";
        return $headTr;
    }
    
    public function isUseNext($attributeCodes)
    {
        if(!$this->getDataHelper()->isUsePreselection()) return false;
        $isAjax = Mage::app()->getRequest()->getParam('isAjax');
        if(Mage::app()->getRequest()->getParam('back') == 1 || ($this->isOtherAttrEnabled() && !$this->hasData('others_attr'))) return true;
        if($isAjax == 'true') {
            return false;    
        }

        $limitCount = $this->getDataHelper()->addNextStepFor();
        foreach($attributeCodes as $attr){
            $options = Mage::getModel('eav/config')->getAttribute('catalog_product', $attr)->getSource()->getAllOptions();
            if(count($options) >= $limitCount) return true;
        }

        return false;
    }
    
    public function getFilteredOptions($attribute)
    {
        $defOptions = $this->getData('default_attribute');
        $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setPositionOrder('asc')
                ->setAttributeFilter($attribute->getId())
                ->setStoreFilter($attribute->getStoreId());
        if($defOptions && isset($defOptions[$attribute->getId()]) && count($defOptions[$attribute->getId()])){
            $collection->addFieldToFilter('main_table.option_id', array('in' => $defOptions[$attribute->getId()]));    
        }

        return $collection->toOptionArray();
    }
    
    public function isOtherAttrEnabled()
    {
        if(!$this->getDataHelper()->isUsePreselection()) return false;
        return ($this->getDataHelper()->getConfig('other_attr') == 1);
    }
}
