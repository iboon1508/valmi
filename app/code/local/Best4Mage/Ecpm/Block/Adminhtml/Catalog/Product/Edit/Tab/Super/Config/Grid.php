<?php

/**
 * Adminhtml super product links grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Best4Mage_Ecpm_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Config_Grid extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Super_Config_Grid
{

    protected function _getSelectedProducts()
    {
        //if(!$this->getDataHelper()->isExtensionActive()){
            return parent::_getSelectedProducts();
        //}
        
        //$_products = $this->getRequest()->getPost('products', null);
        $product = Mage::getModel('catalog/product')->load($this->_getProduct()->getId());
        $products = false;
        $products = $this->_getProduct()->getTypeInstance(true)->getUsedProductIds($this->_getProduct());
        return $products;
    }

    protected $_dataHelper;
    protected $_dataHelperClass="ecpm";
    protected function getDataHelper()
    {
        if(!$this->_dataHelper){
            $this->_dataHelper = Mage::helper('ecpm');
        }

        return $this->_dataHelper;
    }
}
