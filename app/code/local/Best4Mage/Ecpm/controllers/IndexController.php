<?php
class Best4Mage_Ecpm_IndexController extends Mage_Core_Controller_Front_Action
{
    public function getgalleryImageAction()
	{
		$stringstr ='';
		$allowedProduct = $this->getRequest()->getParam('allowed',false);
		$configProductId = $this->getRequest()->getPost('product');
		$config_product = Mage::getModel('catalog/product')->load($configProductId);
		$child_product = Mage::getModel('catalog/product_type_configurable')->getProductByAttributes($this->getRequest()->getParam('super_attribute'),$config_product);
		//echo $child_product->getId();exit;
		if($child_product)
		{
			$_product = Mage::getModel('catalog/product')->load($child_product->getId());
		}
		elseif($allowedProduct)
		{
		  $_product = Mage::getModel('catalog/product')->load($allowedProduct);
		}	
		if($_product)
		{		
			$i = 0; 
			foreach($_product->getMediaGalleryImages() as $_image):
			//if ($this->isGalleryImageVisible($_image)):
			//$_image->getFile();
			$helper = Mage::helper('catalog/image')->init($_product, 'image', $_image->getFile())->keepFrame(false);;
			$size = Mage::getStoreConfig(Mage_Catalog_Helper_Image::XML_NODE_PRODUCT_BASE_IMAGE_WIDTH);
			if (is_numeric($size)) {
					$helper->constrainOnly(true)->resize($size);
				}
			$BigImage = (string)$helper;
			$thumb = Mage::helper('catalog/image')->init($_product, 'thumbnail', $_image->getFile())->resize(75);
			$stringstr .='<li><a class="thumb-link" data-image="'.$BigImage.'"href="#" title="'.$_image->getLabel().'" data-image-index="'.$i.'">
			<img src="'.$thumb.'" width="75" height="75" alt="'.($_image->getLabel()).'" /></a></li>';
			//endif;
			$i++;
			endforeach;
			echo $stringstr;
		}
	}   
}
?>
