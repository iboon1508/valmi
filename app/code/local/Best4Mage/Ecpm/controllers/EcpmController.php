<?php
error_reporting('E_ALL & ~(E_WARNING | E_NOTICE)');
class Best4Mage_Ecpm_EcpmController extends Mage_Adminhtml_Controller_Action
{
    protected $_conn = null;

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/products');
    }
    
    protected function _getConnection()
    {
        if($this->_conn == null){
            $this->_conn = Mage::getSingleton('core/resource')->getConnection('core/write');
        }

        return $this->_conn;
    } 
    public function getgalleryImageAction()
	{
		$stringstr ='';
		$i=0; foreach ($this->getGalleryImages() as $_image):
        if ($this->isGalleryImageVisible($_image)):
        $stringstr .='<li>
            <a class="thumb-link" href="#" title="'.$this->escapeHtml($_image->getLabel()).'" data-image-index="'.$i.">
                <img src=".$this->helper('catalog/image')->init($this->getProduct(), 'thumbnail', $_image->getFile())->resize(75).'"
                     width="75" height="75" alt="'.$this->escapeHtml($_image->getLabel()).'" />
            </a>
        </li>';
        endif;
        $i++; endforeach;
		echo $stringstr;
	}
    protected function _copyImage($file)
    {
        $file = str_replace('.tmp', '', $file);
        $_config = Mage::getSingleton('catalog/product_media_config');
        $_helper = Mage::helper('core/file_storage_database');
        $ioObject = new Varien_Io_File();
        $destDirectory = dirname($_config->getTmpMediaPath($file));
        $ioObject->open(array('path'=>$destDirectory));
        
        if ($_helper->checkDbUsage()) {
            $destFile = $_helper->getUniqueFilename(
                $_config->getBaseTmpMediaUrlAddition(),
                $file
            );
            $_helper->copyFile($_config->getTmpMediaShortUrl($file), $_config->getTmpMediaShortUrl($destFile));
            $ioObject->rm($_config->getTmpMediaPath($destFile));    
        } else {
            $destFile = dirname($file) . $ioObject->dirsep() . Mage_Core_Model_File_Uploader::getNewFileName($_config->getTmpMediaPath($file));
            $ioObject->cp($_config->getTmpMediaPath($file), $_config->getTmpMediaPath($destFile));
        }

        return array(str_replace($ioObject->dirsep(), '/', $destFile), $_config->getTmpMediaUrl($destFile));
    }
    
    public function saveProductsAction()
    {
    
        // Mage::getSingleton('core/session')->setData('ecpm_newproducts', array());
        // die('done'); 
        $cId = $this->getRequest()->getParam('c_id');
        $currentPage = $this->getRequest()->getParam('page');
        if($currentPage == 1){
            $newProductIdsInSession = array();
            Mage::getSingleton('core/session')->setData('ecpm_newproducts', $newProductIdsInSession);
        } else {
            $newProductIdsInSession = Mage::getSingleton('core/session')->getData('ecpm_newproducts');
        }
        
        /*$newProductIdsInSession = Mage::getSingleton('core/session')->getData('ecpm_newproducts');
		if(!$newProductIdsInSession){
			$newProductIdsInSession = array();
		}*/
        
        if($next_step_matrix_data = $this->getRequest()->getPost('next_step_matrix_data')){
            Mage::getSingleton('core/session')->setData('next_step_matrix_data', $next_step_matrix_data);
        }
        
        if($middle_step_matrix_data = $this->getRequest()->getPost('middle_step_matrix_data')){
            Mage::getSingleton('core/session')->setData('middle_step_matrix_data', $middle_step_matrix_data);
        }

        $c_product = Mage::getModel('catalog/product')->load($cId);
        $parentCatIds = $c_product->getCategoryIds();
        
        // echo "<pre>";
        // print_r($newProductIdsInSession);
        // echo "</pre>";
        // echo "<pre>";
        // print_r($newProductIdsInSession);
        // echo "</pre>";
    
        $ecpm = $this->getRequest()->getPost('ecpm');      
        // echo '<pre>';print_r($ecpm);die;
        $ecpm_default = $this->getRequest()->getPost('ecpm_default');
        $productPostData = $this->getRequest()->getPost('product');
        $attributeSetId = $this->getRequest()->getParam('set');
        $is_in_stock = 1;
        // $weight = 1;
        // $status = 1; // 1 for enable and 2 for disable
        // $visibility = 1; // 1 for not visible individually, 2 for catalog, 3 for search and 4 for catalog & search.
        $default_qty = (isset($ecpm_default['qty']))?$ecpm_default['qty']:"";
        $default_price = (!empty($ecpm_default['price']))?$ecpm_default['price']:$productPostData['price'];
        $special_price = (!empty($ecpm_default['special_price']))?$ecpm_default['special_price']:$productPostData['special_price'];
        $default_status = (!empty($ecpm_default['status']))?$ecpm_default['status']:$productPostData['status'];
        $default_weight = (!empty($ecpm_default['weight']))?$ecpm_default['weight']:0;
        $default_visibility = (!empty($ecpm_default['visibility']))?$ecpm_default['visibility']:$productPostData['visibility'];
        $newProductIds = array();
        
        //print_r($productPostData);die;
        
        $ecpm_qty_format = $this->getRequest()->getPost('ecpm_qty_format');
        $atrributeCodes = explode("/", $ecpm_qty_format);
        $attribute1 = Mage::getModel('eav/config')->getAttribute('catalog_product', $atrributeCodes[0]);
        $attribute1Options = $this->getKeyValueOptions($attribute1->getSource()->getAllOptions());
        // echo "<pre>";
        // print_r($attribute1Options);
        // echo "</pre>";

        if($atrributeCodes[1]=='NOATTR') {
            $attribute2Options[0] = 0;            
        } else {
            $attribute2 = Mage::getModel('eav/config')->getAttribute('catalog_product', $atrributeCodes[1]);
            $attribute2Options = $this->getKeyValueOptions($attribute2->getSource()->getAllOptions());
        }

        // echo "<pre>";
        // print_r($attribute2Options);
        // echo "</pre>";
        
        $ecpm_loop_count = Mage::getSingleton('adminhtml/session')->getData('ecpm_loop_count');
        if(!$ecpm_loop_count){
            $ecpm_loop_count = -1;
        }

        $ecpm_savedproducts_count = Mage::getSingleton('adminhtml/session')->getData('ecpm_savedproducts_count');
        if(!$ecpm_savedproducts_count){
            $ecpm_savedproducts_count = 0;
        }
        
        // total products need to be created
        $totalProductsCount = 0;
        foreach($ecpm['padd'] as $attribute1Value => $attribute2Values){
            foreach($attribute2Values as $attribute2Value=>$newProductQty){
                $totalProductsCount++;
            }
        }
        
        $perPageProductInsertLimit = 10;
        $maximumAllowedPages = ceil($totalProductsCount/$perPageProductInsertLimit);
        $currentPageStart = (($currentPage - 1) * $perPageProductInsertLimit) + 1;
        $currentPageEnd = $currentPage * $perPageProductInsertLimit;
        
        $processedItemCount = 0;
        $totalProcessedItemCount = 0;
        
        // echo $currentPageStart."<-- currentPageStart\n";
        foreach($ecpm['padd'] as $attribute1Value => $attribute2Values){
            foreach($attribute2Values as $attribute2Value=>$productAdd){
                // new product qty
                $newProductQty = $ecpm['qty'][$attribute1Value][$attribute2Value];
                
                // echo $totalProcessedItemCount."<-- totalProcessedItemCount\n";
                if($totalProcessedItemCount< ($currentPageStart-1)){
                    $totalProcessedItemCount++;
                    continue;
                }

                // echo "proceed to page execution\n";
                
                if($processedItemCount>=$perPageProductInsertLimit){
                    Mage::getSingleton('core/session')->setData('ecpm_newproducts', $newProductIdsInSession);
                    $this->doReturn($cId, $totalProductsCount);
                    return;
                }
                
                
                if($newProductQty==""){
                    // $newProductQty = 0;
                    $processedItemCount++;
                    $totalProcessedItemCount++;
                    continue;
                }

                // echo $processedItemCount."<-- processedItemCount\n";
                
                if($newProductQty===0){
                    $is_in_stock = 0;
                }
                
                $newProductName = $productPostData['name'];
                if($newProductName==""){
                    $newProductName = "New Product";
                }
                
                // echo $attribute1Options[$attribute1Value]."<-- attribute1Value: ".$attribute1Value;
                // echo $attribute1Options[$attribute2Value]."<-- attribute2Value: ".$attribute2Value;exit;
                if($attribute2Options[$attribute2Value] === 0){
                    $newProductName .= "_".$attribute1Options[$attribute1Value];    
                    $newProductSku = $productPostData['sku']."-".strtolower($attribute1Options[$attribute1Value]); // change sku structure (sankhala)
                } else {
                    $newProductName .= "_".$attribute1Options[$attribute1Value]."_".$attribute2Options[$attribute2Value];
                    $newProductSku = $productPostData['sku']."-".strtolower($attribute1Options[$attribute1Value]."-".$attribute2Options[$attribute2Value]); // change sku structure (sankhala)
                }
                
                //$newProductSku = str_replace(" ", "_", strtolower($newProductName));

                $productData = array();
                // set product id if exist
                if(isset($ecpm['p_ids'][$attribute1Value][$attribute2Value]) && !empty($ecpm['p_ids'][$attribute1Value][$attribute2Value]))
                {
                    $productData['product']['id'] = $ecpm['p_ids'][$attribute1Value][$attribute2Value];
                    $productData['product']['entity_id'] = $ecpm['p_ids'][$attribute1Value][$attribute2Value];
                }
                // Check product is exist by sku
                elseif($idBySku = Mage::getModel('catalog/product')->getIdBySku($newProductSku))
                {
                    //die($idBySku);
                    $productData['product']['id'] = $idBySku;
                    $productData['product']['entity_id'] = $idBySku;
                }
                // Otherwise add new product
                else
                {
                    $productData = array(
                        'product' => array(
                            'type'        =>    'simple',
                            'attribute_set'        =>    $attributeSetId,
                            'name'        =>    $newProductName,
                            'sku'        =>    $newProductSku,
                            'description'        =>    $productPostData['description'],
                            'short_description'        =>    $productPostData['short_description'],
                            'weight'    =>    $default_weight,
                            'status'    =>    $default_status,
                            'tax_class_id' => $this->getDefaultProductTaxClassId(),
                            'visibility'=>    $default_visibility,
                            'gift_message_available' => '',
                            'news_from_date'    => '',
                            'news_to_date'    => '',
                            'price'    => $default_price,
                            'cost' => '',
                            'special_price' => $special_price,
                            'special_from_date' => '',
                            'special_to_date' => '',
                            'enable_googlecheckout' => '',
                            'meta_title' => '',
                            'meta_keyword' => '',
                            'meta_description' => '',
                            'website_ids' => $productPostData['website_ids']
                        ),
                    );
                }
                
                // set product id if exist
                // if(isset($ecpm['p_ids'][$attribute1Value][$attribute2Value]) && !empty($ecpm['p_ids'][$attribute1Value][$attribute2Value])){
                    // $productData['product']['id'] = $ecpm['p_ids'][$attribute1Value][$attribute2Value];
                    // $productData['product']['entity_id'] = $ecpm['p_ids'][$attribute1Value][$attribute2Value];
                // }
                
                // get all matrix fields and if they are set in post then save them in product
                $matrixFieldsArray = Mage::getModel('ecpm/system_config_source_matrixfields')->toArray();
                
                foreach($matrixFieldsArray as $matrixFieldKey=>$matrixFieldVal){
                    if($matrixFieldKey=="qty"){
                        continue;
                    }
                    
                    if($matrixFieldKey=="image"){
                        $bothUpload = false;
                        $removed = 0;
                        $madia_img = $madia_val = array();
                        
                        if(isset($ecpm['url'][$attribute1Value][0][1]) && !empty($ecpm['url'][$attribute1Value][0][1]) 
                        && isset($ecpm['image'][$attribute1Value][0][1]) && !empty($ecpm['image'][$attribute1Value][0][1]))
                        {
                            list($iFile, $iImg) = $this->_copyImage($ecpm['image'][$attribute1Value][0][1]);
                            $madia_val = array("thumbnail" => $iFile,"small_image" => $iFile,"image" => $iFile);
                            $madia_img[] = array("url" => $iImg,"file" => $iFile,"label" => "","position" => 0,"disabled" => 0,"removed" => 0);
                            $productData['product']['thumbnail'] = $iFile;
                            $productData['product']['small_image'] = $iFile;
                            $productData['product']['image'] = $iFile;
                            $productData['product']['media_gallery'] = array('values' => json_encode($madia_val), 'images' => $madia_img, 'massRemoved' => []);
                            $bothUpload = true;
							$productData['product']['media_gallery']['massRemoved'][] = 1;
                        }
                        elseif($atrributeCodes[1]=='NOATTR' && isset($ecpm['url'][$attribute2Value][0][1]) && !empty($ecpm['url'][$attribute2Value][0][1]) 
                        && isset($ecpm['image'][$attribute2Value][0][1]) && !empty($ecpm['image'][$attribute2Value][0][1]))
                        {
                            list($iFile, $iImg) = $this->_copyImage($ecpm['image'][$attribute2Value][0][1]);
                            Mage::log('FI ------------ '.$iFile, null, 'malkesh.log');
                            $madia_val = array("thumbnail" => $iFile,"small_image" => $iFile,"image" => $iFile);
                            $madia_img[] = array("url" => $iImg,"file" => $iFile,"label" => "","position" => 0,"disabled" => 0,"removed" => 0);
                            $productData['product']['thumbnail'] = $iFile;
                            $productData['product']['small_image'] = $iFile;
                            $productData['product']['image'] = $iFile;
                            $productData['product']['media_gallery'] = array('values' => json_encode($madia_val), 'images' => $madia_img, 'massRemoved' =>[]);
                            $bothUpload = true;
							$productData['product']['media_gallery']['massRemoved'][] = 1;
                        }
                        elseif(isset($ecpm['url'][$attribute1Value][$attribute2Value][1]) && !empty($ecpm['url'][$attribute1Value][$attribute2Value][1]) 
                        && isset($ecpm['image'][$attribute1Value][$attribute2Value][1]) && !empty($ecpm['image'][$attribute1Value][$attribute2Value][1]))
                        {
                            $iFile = $ecpm['image'][$attribute1Value][$attribute2Value][1];
                            $iImg = $ecpm['url'][$attribute1Value][$attribute2Value][1];
                            $madia_val = array("thumbnail" => $iFile,"small_image" => $iFile,"image" => $iFile);
                            $madia_img[] = array("url" => $iImg,"file" => $iFile,"label" => "","position" => 0,"disabled" => 0,"removed" => 0);
                            $productData['product']['thumbnail'] = $iFile;
                            $productData['product']['small_image'] = $iFile;
                            $productData['product']['image'] = $iFile;
                            $productData['product']['media_gallery'] = array('values' => json_encode($madia_val), 'images' => $madia_img, 'massRemoved' => []);
                            $bothUpload = true;
							$productData['product']['media_gallery']['massRemoved'][] = 1;
                        }
						//@customization---------
                        for($i=2;$i<=4;$i++)
						{
                        if(isset($ecpm['url'][$attribute1Value][0][$i]) && !empty($ecpm['url'][$attribute1Value][0][$i]) 
                        && isset($ecpm['image'][$attribute1Value][0][$i]) && !empty($ecpm['image'][$attribute1Value][0][$i]))
                        {
                            list($iFile, $iImg) = $this->_copyImage($ecpm['image'][$attribute1Value][0][$i]);
                            $madia_img[] = array("url" => $iImg,"file" => $iFile,"label" => "","position" => 0,"disabled" => 0,"removed" => 0);
                            if($bothUpload){
                                $removed = 1;
                            } else {
                                $removed = $i;
                            }
							$removed = $i;	
                            $productData['product']['media_gallery']['images'] = $madia_img;
                            $productData['product']['media_gallery']['massRemoved'][] = $removed;
                        }
                        elseif($atrributeCodes[1]=='NOATTR' && isset($ecpm['url'][$attribute2Value][0][$i]) && !empty($ecpm['url'][$attribute2Value][0][$i]) 
                        && isset($ecpm['image'][$attribute2Value][0][$i]) && !empty($ecpm['image'][$attribute2Value][0][$i]))
                        {
                            list($iFile, $iImg) = $this->_copyImage($ecpm['image'][$attribute2Value][0][$i]);
                            Mage::log($iFile, null, 'malkesh.log');
                            $madia_img[] = array("url" => $iImg,"file" => $iFile,"label" => "","position" => 0,"disabled" => 0,"removed" => 0);
                            if($bothUpload){
                                $removed = 1;
                            } else {
                                $removed = $i;
                            }
							$removed = $i;
                            $productData['product']['media_gallery']['images'] = $madia_img;
                            $productData['product']['media_gallery']['massRemoved'][] = $removed;
                        }
                        elseif(isset($ecpm['url'][$attribute1Value][$attribute2Value][$i]) && !empty($ecpm['url'][$attribute1Value][$attribute2Value][$i]) 
                        && isset($ecpm['image'][$attribute1Value][$attribute2Value][$i]) && !empty($ecpm['image'][$attribute1Value][$attribute2Value][$i]))
                        {
                            $iFile = $ecpm['image'][$attribute1Value][$attribute2Value][$i];
                            $iImg = $ecpm['url'][$attribute1Value][$attribute2Value][$i];
                            $madia_img[] = array("url" => $iImg,"file" => $iFile,"label" => "","position" => 0,"disabled" => 0,"removed" => 0);
                            if($bothUpload){
                                $removed = 1;
                            } else {
                                $removed = $i;
                            }
							$removed = $i;
                            $productData['product']['media_gallery']['images'] = $madia_img;
                            $productData['product']['media_gallery']['massRemoved'][] = $removed;
                        }
					}	
                        continue;
                    
                    }
                    
                    if(isset($ecpm[$matrixFieldKey][$attribute1Value][$attribute2Value]) && !empty($ecpm[$matrixFieldKey][$attribute1Value][$attribute2Value])){
                        $productData['product'][$matrixFieldKey] = $ecpm[$matrixFieldKey][$attribute1Value][$attribute2Value];
                    }
                }
                
                // Save Default Configurable Data to Simple One.
                $bema = new Best4Mage_Ecpm_Model_Attributes();
                if($middle_step_matrix_data) $bema->setDefaultAttributeId(Mage::helper('core')->jsonDecode($middle_step_matrix_data));
                $defautAttributes = $bema->getAvailableSwatchAttributes($attributeSetId);
                
                if(count($defautAttributes) > 0)
                {
                    foreach($defautAttributes as $parseAttr)
                    {
                        $defautAttribute = $parseAttr['code'];
                        if(isset($ecpm[$defautAttribute][$attribute1Value][$attribute2Value]))
                        {
                            $productData['product'][$defautAttribute] = $ecpm[$defautAttribute][$attribute1Value][$attribute2Value];
                        } elseif(isset($ecpm_default[$defautAttribute]))
                        {
                            $productData['product'][$defautAttribute] = $ecpm_default[$defautAttribute];
                        }
                    }
                }
                
                // set price
                if(isset($ecpm['price'][$attribute1Value][$attribute2Value]) && !empty($ecpm['price'][$attribute1Value][$attribute2Value])){
                    $productData['product']['price'] = $ecpm['price'][$attribute1Value][$attribute2Value];
                }
                
                if ($productData) {
                    // if (!isset($productData['product']['stock_data']['use_config_manage_stock'])) {
                        // $productData['product']['stock_data']['use_config_manage_stock'] = 0;
                    // }
/*                      echo "<pre>";
                     print_r($productData);
                     echo "</pre>";
 */                    
                    // set 2 ecpm attributes
                    $productData['product'][$atrributeCodes[0]] = $attribute1Value;
                    if($atrributeCodes[1] != 'NOATTR') $productData['product'][$atrributeCodes[1]] = $attribute2Value;
                    
                    //print_r($productData);die;
                    
                    $this->getRequest()->setPost($productData);
                    $this->getRequest()->setParam('id', $productData['product']['id']);
                    $this->getRequest()->setParam('type', $productData['product']['type']);
                    $this->getRequest()->setParam('set', $productData['product']['attribute_set']);
                    
                    $product = $this->_initProductSave();

                    // set Category from parent product.
                    if(isset($parentCatIds) && count($parentCatIds)) {
                        $product->setCategoryIds($parentCatIds);
                    }

                    try {
                        // Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                        // echo "<pre>"; 
                        // print_r($product->getData());
                        // echo "</pre>"; 
                        // die('die');
                        $product->save();
                        
                        // set stock item
                        if (!($stockItem = $product->getStockItem())) {
                            $stockItem = Mage::getModel('cataloginventory/stock_item');
                            $stockItem->assignProduct($product)
                                      ->setData('stock_id', 1)
                                      ->setData('store_id', 1);
                        }

                        if($newProductQty=="no_stock"){
                            $stockItem->setData('is_in_stock', 1)
                                      ->setData('manage_stock', 0)
                                      ->setData('use_config_manage_stock', 0)
                                      ->save();
                        }else{
                            $stockItem->setData('qty', $newProductQty)
                                      ->setData('is_in_stock', $newProductQty > 0 ? 1 : 0)
                                      ->setData('manage_stock', 1)
                                      ->setData('use_config_manage_stock', 0)
                                      ->save();
                        }
                        
                        $productId = $product->getId();
                        // $newProductIds[] = $productId;
                        $newProductIdsInSession[] = $productId;
                        $totalProcessedItemCount++;
                        $processedItemCount++;
                        
                        // echo $processedItemCount."<-- processedItemCount after save\n";
                        if($processedItemCount>=$perPageProductInsertLimit){
                            Mage::getSingleton('core/session')->setData('ecpm_newproducts', $newProductIdsInSession);
                            
                            // $msg = '<div id="messages"><ul class="messages"><li class="success-msg"><ul><li><span>'.Mage::helper('catalog')->__("%d are saved from %d products.", count($newProductIdsInSession), $totalProductsCount).'</span></li></ul></li></ul></div>';
                            $this->doReturn($cId, $totalProductsCount);
                            return;
                        }
                        
                        // echo "Product Saved: $productId \n";
                        // die();
                    }catch (Mage_Core_Exception $e) {
                        Mage::logException($e);
                        //echo $e->getMessage();
                        //$this->_getSession()->addError($e->getMessage())
                            //->setProductData($data);
                        $redirectBack = true;
                    }
                    catch (Exception $e) {
                        Mage::logException($e);
                        //echo $e->getMessage();
                        //$this->_getSession()->addError($e->getMessage());
                        $redirectBack = true;
                    }
                }
            }
        }

        Mage::getSingleton('core/session')->setData('ecpm_newproducts', $newProductIdsInSession);
        $this->doReturn($cId, $totalProductsCount);
        return;
        
        // $currentConfigproduct = Mage::getModel('catalog/product')->load($cId);
        // // echo $currentConfigproduct->getId();
        // // echo "<pre>";
        // // print_r($newProductIds);
        // // echo "</pre>";
        // Mage::getResourceModel('catalog/product_type_configurable')
            // ->saveProducts($currentConfigproduct, $newProductIds);
        
        // Mage::app()->cleanCache();
        
        // $currentConfigproduct = Mage::getModel('catalog/product')->load($cId);
        
        // // die('done');
        // $return['success'] = true;
        // $return['msg'] = '<div id="messages"><ul class="messages"><li class="success-msg"><ul><li><span>'.Mage::helper('catalog')->__("Products have been saved.").'</span></li></ul></li></ul></div>';
        
        // Mage::register('current_product', $currentConfigproduct);
        // Mage::register('product', $currentConfigproduct);
        // $return['matrix'] = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config')->toHtml();
        
        // $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
    }
    
    public function saveProductsActionOld()
    {
        // echo "<pre>";
        // print_r($this->getRequest()->getPost());
        // echo "</pre>";
        // die();
        
        // Mage::getSingleton('core/session')->setData('ecpm_newproducts', array());
        // die();
        
        $newProductIdsInSession = Mage::getSingleton('core/session')->getData('ecpm_newproducts');
        if(!$newProductIdsInSession){
            $newProductIdsInSession = array();
        }
        
        $ecpm = $this->getRequest()->getPost('ecpm');
        $cId = $this->getRequest()->getParam('c_id');
        $productPostData = $this->getRequest()->getPost('product');
        $attributeSetId = $this->getRequest()->getParam('set');
        $is_in_stock = 1;
        $weight = 1;
        $status = 1; // 1 for enable and 2 for disable
        $visibility = 1; // 1 for not visible individually, 2 for catalog, 3 for search and 4 for catalog & search.
        $price = 0.00;
        $newProductIds = array();
        
        $ecpm_qty_format = $this->getRequest()->getPost('ecpm_qty_format');
        $atrributeCodes = explode("/", $ecpm_qty_format);
        $attribute1 = Mage::getModel('eav/config')->getAttribute('catalog_product', $atrributeCodes[0]);
        $attribute1Options = $this->getKeyValueOptions($attribute1->getSource()->getAllOptions());
        
        $attribute2 = Mage::getModel('eav/config')->getAttribute('catalog_product', $atrributeCodes[1]);
        $attribute2Options = $this->getKeyValueOptions($attribute2->getSource()->getAllOptions());
        
        // total products need to be created
        $totalProductsCount = 0;
        foreach($ecpm['qty'] as $attribute1Value => $attribute2Values){
            foreach($attribute2Values as $attribute2Value=>$newProductQty){
                if($newProductQty!=""){
                    $totalProductsCount++;
                }
            }
        }

        // echo $totalProductsCount."<--totalProductsCount \n";
        
        $perPageProductInsertLimit = 5;
        $maximumAllowedPages = ceil($totalProductsCount/$perPageProductInsertLimit);
        // echo $maximumAllowedPages."<-- maximumAllowedPages \n";
        $currentPage = $this->getRequest()->getParam('page');
        // echo $currentPage."<-- currentPage \n";
        $currentPageStart = (($currentPage - 1) * $perPageProductInsertLimit) + 1;
        $currentPageEnd = $currentPage * $perPageProductInsertLimit;
        // if($currentPage>=$maximumAllowedPages){
            // $this->doReturn($cId, true);
            // return;
        // }
                    // print_r($product->getData());
                    
        
        $addedProducts = 0;
        foreach($ecpm['qty'] as $attribute1Value => $attribute2Values){
            foreach($attribute2Values as $attribute2Value=>$newProductQty){
                if($newProductQty==""){
                    // $newProductQty = 0;
                    continue;
                }
                
                if($addedProducts < ($currentPageStart-1)){
                    $addedProducts++;
                    // echo $addedProducts."<-- addedProducts\n";
                    continue;
                }
                
                if($newProductQty==0){
                    $is_in_stock = 0;
                }
                
                $newProductName = $productPostData['name'];
                if($newProductName==""){
                    $newProductName = "New Product";
                }
                
                $newProductName .= "_".$attribute1Options[$attribute1Value]."_".$attribute2Options[$attribute2Value];
                $newProductSku = str_replace(" ", "_", strtolower($newProductName));
                
                // set product id if exist
                if(isset($ecpm['p_ids'][$attribute1Value][$attribute2Value]) && !empty($ecpm['p_ids'][$attribute1Value][$attribute2Value])){
                    $productData['product']['id'] = $ecpm['p_ids'][$attribute1Value][$attribute2Value];
                    $productData['product']['entity_id'] = $ecpm['p_ids'][$attribute1Value][$attribute2Value];
                }else{
                    $productData = array(
                        'product' => array(
                            'type'        =>    'simple',
                            'attribute_set'        =>    $attributeSetId,
                            'name'        =>    $newProductName,
                            'sku'        =>    $newProductSku,
                            'description'        =>    $productPostData['description'],
                            'short_description'        =>    $productPostData['short_description'],
                            'weight'    =>    $weight,
                            'status'    =>    $status,
                            'tax_class_id' => $this->getDefaultProductTaxClassId(),
                            'visibility'=>    $visibility,
                            'gift_message_available' => '',
                            'news_from_date'    => '',
                            'news_to_date'    => '',
                            'price'    => $price,
                            'cost' => '',
                            'special_price' => '',
                            'special_from_date' => '',
                            'special_to_date' => '',
                            'enable_googlecheckout' => '',
                            'meta_title' => '',
                            'meta_keyword' => '',
                            'meta_description' => '',
                            'thumbnail' => '',
                            'small_image' => '',
                            'image' => '',
                        ),
                    );
                }
                
                // set product id if exist
                // if(isset($ecpm['p_ids'][$attribute1Value][$attribute2Value]) && !empty($ecpm['p_ids'][$attribute1Value][$attribute2Value])){
                    // $productData['product']['id'] = $ecpm['p_ids'][$attribute1Value][$attribute2Value];
                    // $productData['product']['entity_id'] = $ecpm['p_ids'][$attribute1Value][$attribute2Value];
                // }
                
                // get all matrix fields and if they are set in post then save them in product
                $matrixFieldsArray = Mage::getModel('ecpm/system_config_source_matrixfields')->toArray();
                foreach($matrixFieldsArray as $matrixFieldKey=>$matrixFieldVal){
                    if($matrixFieldKey=="qty"){
                        continue;
                    }
                    
                    if(isset($ecpm[$matrixFieldKey][$attribute1Value][$attribute2Value]) && !empty($ecpm[$matrixFieldKey][$attribute1Value][$attribute2Value])){
                        $productData['product'][$matrixFieldKey] = $ecpm[$matrixFieldKey][$attribute1Value][$attribute2Value];
                    }
                }
                
                // set price
                if(isset($ecpm['price'][$attribute1Value][$attribute2Value]) && !empty($ecpm['price'][$attribute1Value][$attribute2Value])){
                    $productData['product']['price'] = $ecpm['price'][$attribute1Value][$attribute2Value];
                }
                
                if ($productData) {
                    // if (!isset($productData['product']['stock_data']['use_config_manage_stock'])) {
                        // $productData['product']['stock_data']['use_config_manage_stock'] = 0;
                    // }
                    // echo "<pre>";
                    // print_r($productData);
                    // echo "</pre>";
                    
                    // set 2 ecpm attributes
                    $productData['product'][$atrributeCodes[0]] = $attribute1Value;
                    $productData['product'][$atrributeCodes[1]] = $attribute2Value;
                    
                    
                    $this->getRequest()->setPost($productData);
                    $this->getRequest()->setParam('type', $productData['product']['type']);
                    $this->getRequest()->setParam('set', $productData['product']['attribute_set']);
                    $product = $this->_initProductSave();

                    try {
                        // Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                        // echo "<pre>"; 
                        // echo "</pre>"; 
                        // die('die');
                        $product->save();
                        
                        // set stock item
                        if (!($stockItem = $product->getStockItem())) {
                            $stockItem = Mage::getModel('cataloginventory/stock_item');
                            $stockItem->assignProduct($product)
                                      ->setData('stock_id', 1)
                                      ->setData('store_id', 1);
                        }

                        if($newProductQty=="no_stock"){
                            $stockItem->setData('is_in_stock', 1)
                                      ->setData('manage_stock', 0)
                                      ->setData('use_config_manage_stock', 0)
                                      ->save();
                        }else{
                            $stockItem->setData('qty', $newProductQty)
                                      ->setData('is_in_stock', $newProductQty > 0 ? 1 : 0)
                                      ->setData('manage_stock', 1)
                                      ->setData('use_config_manage_stock', 0)
                                      ->save();
                        }
                        
                        $productId = $product->getId();
                        $newProductIds[] = $productId;
                        if(!in_array($productId, $newProductIdsInSession)){
                            $newProductIdsInSession[] = $productId;
                        }
                        
                        //echo count($newProductIdsInSession)."==".$totalProductsCount;
                        if($currentPage==4){
                            return;
                        }
                        
                        if(count($newProductIdsInSession)==$totalProductsCount){
                            Mage::getSingleton('core/session')->setData('ecpm_newproducts', $newProductIdsInSession);
                            $this->doReturn($cId, true);
                            //die('jtre');
                            return;
                        }else{
                            if(count($newProductIdsInSession)%$perPageProductInsertLimit==0){
                                Mage::getSingleton('core/session')->setData('ecpm_newproducts', $newProductIdsInSession);
                            
                                $msg = '<div id="messages"><ul class="messages"><li class="success-msg"><ul><li><span>'.Mage::helper('catalog')->__("%d are saved from %d products.", $addedProducts, $totalProductsCount).'</span></li></ul></li></ul></div>';
                                $this->doReturn($cId, false, $msg);
                                return;
                            }
                        }
                        
                        $addedProducts++;
                        if($addedProducts>=$currentPageEnd){
                            $newProductIdsInSession = Mage::getSingleton('core/session')->getData('ecpm_newproducts');
                            $newProductIdsInSession[] = $productId;
                            
                            // if(!$newProductIdsInSession){
                            // }else{
                                // $newProductIdsInSession = array_merge($newProductIdsInSession, $newProductIds);
                            // }
                            // echo "<pre>";
                            // print_r($newProductIds);
                            // echo "</pre>";
                            // echo "<pre>";
                            // print_r($newProductIdsInSession);
                            // echo "</pre>";
                            $newProductIdsInSession = array_unique($newProductIdsInSession);
                            Mage::getSingleton('core/session')->setData('ecpm_newproducts', $newProductIdsInSession);
                            
                            $msg = '<div id="messages"><ul class="messages"><li class="success-msg"><ul><li><span>'.Mage::helper('catalog')->__("%d are saved from %d products.", $addedProducts, $totalProductsCount).'</span></li></ul></li></ul></div>';
                            $this->doReturn($cId, false, $msg);
                            return;
                        }
                        
                        
                        
                        // echo "Product Saved: $productId \n";
                        // die();
                    }catch (Mage_Core_Exception $e) {
                        Mage::logException($e);
                        //echo $e->getMessage();
                        //$this->_getSession()->addError($e->getMessage())
                            //->setProductData($data);
                        $redirectBack = true;
                    }
                    catch (Exception $e) {
                        Mage::logException($e);
                        //echo $e->getMessage();
                        //$this->_getSession()->addError($e->getMessage());
                        $redirectBack = true;
                    }
                }
            }
        }

        // $currentConfigproduct = Mage::getModel('catalog/product')->load($cId);
        // // echo $currentConfigproduct->getId();
        // // echo "<pre>";
        // // print_r($newProductIds);
        // // echo "</pre>";
        // Mage::getResourceModel('catalog/product_type_configurable')
            // ->saveProducts($currentConfigproduct, $newProductIds);


        // $this->doReturn($cId, true);
        // Mage::app()->cleanCache();
        
        // $currentConfigproduct = Mage::getModel('catalog/product')->load($cId);
        
        // // die('done');
        // $return['success'] = true;
        // $return['msg'] = '<div id="messages"><ul class="messages"><li class="success-msg"><ul><li><span>'.Mage::helper('catalog')->__("Products have been saved.").'</span></li></ul></li></ul></div>';
        
        // Mage::register('current_product', $currentConfigproduct);
        // Mage::register('product', $currentConfigproduct);
        // $return['matrix'] = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config')->toHtml();
        
        // $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
    }
    
    public function loadmatrixAction()
    {
        $cId = $this->getRequest()->getParam('cid');
        $newAttr = $this->getRequest()->getPost('nattr');
        if(is_string($newAttr)) $newAttr = Mage::helper('core')->jsonDecode($newAttr);
        $return = array();
        if($cId > 0) {
            $currentConfigproduct = Mage::getModel('catalog/product')->load($cId);
            Mage::register('current_product', $currentConfigproduct);
            Mage::register('product', $currentConfigproduct);
            $matrix = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config')->setData('default_attribute', $newAttr);
            if($others = $this->getRequest()->getPost('middle_step_matrix_data')){
                $matrix->setData('others_attr', $others);
            } elseif($matrix->isOtherAttrEnabled()) {
                $matrix->setData('others_attr', $others);
            }

            $return['success'] = true;
            $return['matrix'] = $matrix->toHtml();
        } else {
            $return['success'] = false;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
        return;
    }
    
    public function loadotherattrAction()
    {
        $cId = $this->getRequest()->getParam('cid');
        $newAttr = $this->getRequest()->getPost('nattr');
        if(is_string($newAttr)) $newAttr = Mage::helper('core')->jsonDecode($newAttr);
        $return = array();
        if($cId > 0) {
            $currentConfigproduct = Mage::getModel('catalog/product')->load($cId);
            Mage::register('current_product', $currentConfigproduct);
            Mage::register('product', $currentConfigproduct);
            $return['success'] = true;
            $return['matrix'] = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config')->setData('next_default_attribute', $newAttr)->toHtml();
        } else {
            $return['success'] = false;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
        return;
    }
    
    protected function doReturn($cId, $totalProductsCount)
    {
        
        $newProductIdsInSession = Mage::getSingleton('core/session')->getData('ecpm_newproducts');
        $newProductIdsInSession = array_unique($newProductIdsInSession);
        // echo "<pre>";
        // print_r($newProductIdsInSession);
        // echo "</pre>";
        
        $return['msg'] = '<div id="messages"><ul class="messages"><li class="success-msg"><ul><li><span>'.Mage::helper('catalog')->__("%d products are saved.", count($newProductIdsInSession)).'</span></li></ul></li></ul></div>';
        
        // if($isStop){
        if($totalProductsCount==count($newProductIdsInSession)){
            $currentConfigproduct = Mage::getModel('catalog/product')->load($cId);
            // echo "<pre>";
            // print_r($newProductIdsInSession);
            // echo "</pre>";
            //Mage::getSingleton('core/session')->setData('ecpm_newproducts', array());
            
            $allChildProducts = $currentConfigproduct->getTypeInstance()->getUsedProducts($currentConfigproduct);
            $newProductIds = array();
            foreach($allChildProducts as $allChildProduct){
                $newProductIds[] = $allChildProduct->getId();
            }

            $newProductIdsInSession = array_merge($newProductIds, $newProductIdsInSession);
            
            Mage::getResourceModel('catalog/product_type_configurable')
                ->saveProducts($currentConfigproduct, $newProductIdsInSession);
                
            $currentConfigproduct->save();
            $return['stop'] = true;
            Mage::app()->cleanCache();

            $currentConfigproduct = Mage::getModel('catalog/product')->load($cId);
            Mage::register('current_product', $currentConfigproduct);
            Mage::register('product', $currentConfigproduct);
            $returnMatrix = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config');
            if(Mage::getSingleton('core/session')->hasData('next_step_matrix_data')){
                $newAttr = Mage::getSingleton('core/session')->getData('next_step_matrix_data');
                $returnMatrix->setData('default_attribute', Mage::helper('core')->jsonDecode($newAttr));
            }

            if(Mage::getSingleton('core/session')->hasData('middle_step_matrix_data')){
                $newAttr = Mage::getSingleton('core/session')->getData('middle_step_matrix_data');
                $returnMatrix->setData('others_attr', Mage::helper('core')->jsonDecode($newAttr));
            }
        
            $return['matrix'] = $returnMatrix->toHtml();    
            
            //$return['matrix'] = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config')->toHtml();
            $linkp = new Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Super_Config();
            $return['linkp'] = Mage::helper('core')->jsonDecode($linkp->getLinksJson());
            $return['msg'] = '<div id="messages"><ul class="messages"><li class="success-msg"><ul><li><span>'.Mage::helper('catalog')->__("Products have been saved.").'</span></li></ul></li></ul></div>';
        }
        
        // die('done');
        $return['success'] = true;
        
        
        // if($msg){
            // $return['msg'] = $msg;
        // }
        
        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
    }
    
    protected function _initProductSave()
    {
        $product    = $this->_initProduct();
        // echo "<pre>";
        // print_r($product);
        // echo "</pre>";
        // die("InitSave");
        $productData = $this->getRequest()->getPost('product');
        
        // if ($productData && !isset($productData['stock_data']['use_config_manage_stock'])) {
            // $productData['stock_data']['use_config_manage_stock'] = 0;
        // }

        /**
         * Websites
         */
         
        // Sankhalainfo Comment This Below Part to fix website Id removal
        /*if (!isset($productData['website_ids'])) {
            $productData['website_ids'] = array();
            // $productData['website_ids'] = array();
			// $_websites = Mage::app()->getWebsites();

			// foreach($_websites as $website){
				// $productData['website_ids'][] = $website->getId();
			// }
        }*/
        
        $def_media_imgs = $product->getMediaGallery('images');
		Mage::log($product->getId(), null, 'pranav.log');
		Mage::log($productData['media_gallery']['massRemoved'], null, 'pranav.log');
		//@cutomization-------------
		foreach($productData['media_gallery']['massRemoved'] as $massremoved)
		{
			//$k = 0;
			foreach($def_media_imgs as &$d_m_img){
				if(($massremoved-1) == 0){
					if($product->getImage() == $d_m_img['file']){
                         $d_m_img['removed'] = 1;
                    }
				}
				elseif($product->getImage() != $d_m_img['file'])
				{
					$d_m_img['removed'] = 1;
				}
				//$k++;
			}
		}
		unset($productData['media_gallery']['massRemoved']);
		//@cutomization-------------
        /* if($massRemoved = $productData['media_gallery']['massRemoved']){
            foreach($def_media_imgs as &$d_m_img){
                if($massRemoved == 3){
                    if($product->getImage() == $d_m_img['file']){
                         $d_m_img['removed'] = 1;
                    }
                } else if($massRemoved == 2){
                    if($product->getImage() != $d_m_img['file']){
                        $d_m_img['removed'] = 1;
                    }    
                } else {
                    $d_m_img['removed'] = 1;
                }
            }

            unset($productData['media_gallery']['massRemoved']);
        } */

        if($med_is = $productData['media_gallery']['images']){
            foreach($med_is as $med_i){
                $med_i['position'] = count($def_media_imgs);
                $def_media_imgs[] = $med_i;
            }

            $productData['media_gallery']['images'] = Mage::helper('core')->jsonEncode($def_media_imgs);
        }
        
        //print_r($productData);die;

        $wasLockedMedia = false;
        if ($product->isLockedAttribute('media')) {
            $product->unlockAttribute('media');
            $wasLockedMedia = true;
        }

        $product->addData($productData);

        if ($wasLockedMedia) {
            $product->lockAttribute('media');
        }

        if (Mage::app()->isSingleStoreMode()) {
            $product->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
        }

        /**
         * Check "Use Default Value" checkboxes values
         */
        if ($useDefaults = $this->getRequest()->getPost('use_default')) {
            foreach ($useDefaults as $attributeCode) {
                $product->setData($attributeCode, false);
            }
        }

        /**
         * Init product links data (related, upsell, crosssel)
         */
        $links = $this->getRequest()->getPost('links');
        if (isset($links['related']) && !$product->getRelatedReadonly()) {
            $product->setRelatedLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['related']));
        }

        if (isset($links['upsell']) && !$product->getUpsellReadonly()) {
            $product->setUpSellLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['upsell']));
        }

        if (isset($links['crosssell']) && !$product->getCrosssellReadonly()) {
            $product->setCrossSellLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['crosssell']));
        }

        if (isset($links['grouped']) && !$product->getGroupedReadonly()) {
            $product->setGroupedLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['grouped']));
        }

        /**
         * Initialize product categories
         */
        $categoryIds = $this->getRequest()->getPost('category_ids');
        if (null !== $categoryIds) {
            if (empty($categoryIds)) {
                $categoryIds = array();
            }

            $product->setCategoryIds($categoryIds);
        }

        /**
         * Initialize data for configurable product
         */
        if (($data = $this->getRequest()->getPost('configurable_products_data')) && !$product->getConfigurableReadonly()) {
            $product->setConfigurableProductsData(Mage::helper('core')->jsonDecode($data));
        }

        if (($data = $this->getRequest()->getPost('configurable_attributes_data')) && !$product->getConfigurableReadonly()) {
            $product->setConfigurableAttributesData(Mage::helper('core')->jsonDecode($data));
        }

        $product->setCanSaveConfigurableAttributes((bool)$this->getRequest()->getPost('affect_configurable_product_attributes') && !$product->getConfigurableReadonly());

        /**
         * Initialize product options
         */
        if (isset($productData['options']) && !$product->getOptionsReadonly()) {
            $product->setProductOptions($productData['options']);
        }

        $product->setCanSaveCustomOptions((bool)$this->getRequest()->getPost('affect_product_custom_options') && !$product->getOptionsReadonly());

        Mage::dispatchEvent('catalog_product_prepare_save', array('product' => $product, 'request' => $this->getRequest()));

        return $product;
    }
    
    protected function _initProduct()
    {
        //echo "asda->".$this->getRequest()->getParam('type')."<br/>";
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Manage Products'));

        $productId  = (int) $this->getRequest()->getParam('id');
        $product    = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if (!$productId) {
            if ($setId = (int) $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }

            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }
        }

        $product->setData('_edit_mode', true);
        if ($productId) {
            $product->load($productId);
        }

        $attributes = $this->getRequest()->getParam('attributes');
        if ($attributes && $product->isConfigurable() &&
            (!$productId || !$product->getTypeInstance()->getUsedProductAttributeIds())) {
            $product->getTypeInstance()->setUsedProductAttributeIds(
                explode(",", base64_decode(urldecode($attributes)))
            );
        }

        // Required attributes of simple product for configurable creation
        if ($this->getRequest()->getParam('popup')
            && $requiredAttributes = $this->getRequest()->getParam('required')) {
            $requiredAttributes = explode(",", $requiredAttributes);
            foreach ($product->getAttributes() as $attribute) {
                if (in_array($attribute->getId(), $requiredAttributes)) {
                    $attribute->setIsRequired(1);
                }
            }
        }

        if ($this->getRequest()->getParam('popup')
            && $this->getRequest()->getParam('product')
            && !is_array($this->getRequest()->getParam('product'))
            && $this->getRequest()->getParam('id', false) === false) {
            $configProduct = Mage::getModel('catalog/product')
                ->setStoreId(0)
                ->load($this->getRequest()->getParam('product'))
                ->setTypeId($this->getRequest()->getParam('type'));

            /* @var $configProduct Mage_Catalog_Model_Product */
            $data = array();
            foreach ($configProduct->getTypeInstance()->getEditableAttributes() as $attribute) {
                /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
                if(!$attribute->getIsUnique()
                    && $attribute->getFrontend()->getInputType()!='gallery'
                    && $attribute->getAttributeCode() != 'required_options'
                    && $attribute->getAttributeCode() != 'has_options'
                    && $attribute->getAttributeCode() != $configProduct->getIdFieldName()) {
                    $data[$attribute->getAttributeCode()] = $configProduct->getData($attribute->getAttributeCode());
                }
            }

            $product->addData($data)
                ->setWebsiteIds($configProduct->getWebsiteIds());
        }

        // Mage::register('product', $product);
        // Mage::register('current_product', $product);
        return $product;
    }
    
    protected function getKeyValueOptions($optoins)
    {
        $keyValue = array();
        foreach($optoins as $optoin){
            if(empty($optoin['value'])){
                continue;
            }

            $keyValue[$optoin['value']] = $optoin['label'];
        }

        return $keyValue;
    }
    
    public function editsingleAction()
    {    
        $productId = $this->getRequest()->getParam('id');
        $configProductId = $this->getRequest()->getParam('c_id');
        $product = Mage::getModel('catalog/product')->load($productId);
        $return = array();
        // $this->loadLayout();
        $edit_single_form = $this->getLayout()->createBlock('core/template')->setData(array('product'=>$product, 'c_id'=>$configProductId))->setTemplate('b4m/ecpm/edit_single.phtml');
        
        if($middle_step_matrix_data = $this->getRequest()->getPost('next_step_matrix_data')){
            $edit_single_form->setData('default_attribute', Mage::helper('core')->jsonDecode($middle_step_matrix_data));
        }
        
        if($middle_step_matrix_data = $this->getRequest()->getPost('middle_step_matrix_data')){
            $edit_single_form->setData('others_attr', Mage::helper('core')->jsonDecode($middle_step_matrix_data));
        }
        
        $return['edit_single_form'] = $edit_single_form->toHtml();
        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
    }
    
    
    public function deletesingleAction()
    {
        // perform delete action here.
        // delete simple product if it has only single parent
        // if simple product has more than 1 parent then only delete its relation with parent.
        $productId = $this->getRequest()->getParam('id');
        // $product = Mage::getModel('catalog/product')->load($productId);
        $configProductId = $this->getRequest()->getParam('c_id');
        $currentConfigproduct = Mage::getModel('catalog/product')->load($configProductId);
        
        $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
            ->getParentIdsByChild($productId);
        
        if(count($parentIds)>0){
            if(count($parentIds)==1){
                // delete simple product
                $product = Mage::getModel('catalog/product')->load($productId);
                $product->delete();
            }else{
                // delete configurable product and its parent relation
                $allChildProducts = $currentConfigproduct->getTypeInstance()->getUsedProducts($currentConfigproduct);
                $newProductIds = array();
                foreach($allChildProducts as $allChildProduct){
                    if($productId==$allChildProduct->getId()){
                        continue;
                    }

                    $newProductIds[] = $allChildProduct->getId();
                }

                // echo "<pre>";
                // print_r($allChildProducts->getAllIds());
                // echo "</pre>";
                // echo "<pre>";
                // print_r($newProductIds);
                // echo "</pre>";
                // die();
                Mage::app()->cleanCache();
                Mage::getResourceModel('catalog/product_type_configurable')
                    ->saveProducts($currentConfigproduct, $newProductIds);
            }
        }

        // echo "<pre>";
        // print_r($parentIds);
        // echo "</pre>";
        // die();
        
        $currentConfigproduct->save();
        Mage::app()->cleanCache();
        $currentConfigproduct = Mage::getModel('catalog/product')->load($configProductId);
        
        // die('done');
        $return['success'] = true;
        $return['msg'] = '<div id="messages"><ul class="messages"><li class="success-msg"><ul><li><span>'.Mage::helper('catalog')->__("Product has been deleted.").'</span></li></ul></li></ul></div>';
        
        Mage::register('current_product', $currentConfigproduct);
        Mage::register('product', $currentConfigproduct);
        $edit_single_form = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config');
        
        if($middle_step_matrix_data = $this->getRequest()->getPost('next_step_matrix_data')){
            $edit_single_form->setData('default_attribute', Mage::helper('core')->jsonDecode($middle_step_matrix_data));
        }
        
        if($middle_step_matrix_data = $this->getRequest()->getPost('middle_step_matrix_data')){
            $edit_single_form->setData('others_attr', Mage::helper('core')->jsonDecode($middle_step_matrix_data));
        }
        
        $return['matrix'] = $edit_single_form->toHtml();
        //$return['matrix'] = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config')->toHtml();
        $linkp = new Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Super_Config();
        $return['linkp'] = Mage::helper('core')->jsonDecode($linkp->getLinksJson());
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
    }
    
    public function savesingleAction()
    {    
        // echo "<pre>";
        // print_r($this->getRequest()->getPost());
        // echo "</pre>";
        // die();	
        $editSingle = $this->getRequest()->getPost('edit_single');
        $configProductId = $editSingle['config_product_id'];
        if(isset($editSingle['id'])){
            $_product = Mage::getModel("catalog/product")->load($editSingle['id']);
            if(isset($editSingle['name'])){
                $_product->setData('name', $editSingle['name']);
            }
            
            if(isset($editSingle['sku'])){
                $_product->setData('sku', $editSingle['sku']);
            }
            
            if(isset($editSingle['visibility'])){
                $_product->setData('visibility', $editSingle['visibility']);
            }
            
            if(isset($editSingle['status'])){
                $_product->setData('status', $editSingle['status']);
            }
            
            if(isset($editSingle['weight'])){
                $_product->setData('weight', $editSingle['weight']);
            }
            
            if(isset($editSingle['price'])){
                $_product->setData('price', $editSingle['price']);
            }

            if($middle_step_matrix_data = $this->getRequest()->getPost('middle_step_matrix_data')){
                $middle_step_matrix_data = Mage::helper('core')->jsonDecode($middle_step_matrix_data);
                $bema = new Best4Mage_Ecpmpro_Model_Attributes();
                $bema->setDefaultAttributeId($middle_step_matrix_data);
                $defautAttributes = $bema->getAvailableSwatchAttributes($_product->getAttributeSetId());
                
                if(count($defautAttributes) > 0)
                {
                    foreach($defautAttributes as $parseAttr)
                    {
                        $defautAttribute = $parseAttr['code'];
                        if(isset($editSingle[$defautAttribute]))
                        {
                            $_product->setData($defautAttribute, $editSingle[$defautAttribute]);
                        }
                    }
                }
            }
            
            $_product->save();
            
            if((isset($editSingle['qty']) && !empty($editSingle['qty']) ) 
                || (isset($editSingle['is_in_stock']) && !empty($editSingle['is_in_stock'])) ){
                // update stock
                if (!($stockItem = $_product->getStockItem())) {
                    $stockItem = Mage::getModel('cataloginventory/stock_item');
                    $stockItem->assignProduct($_product)
                              ->setData('stock_id', 1)
                              ->setData('store_id', 1);
                }
                
                $qty = $editSingle['qty'];
                $is_in_stock = $editSingle['is_in_stock'];
                $is_in_stock = $qty > 0 ? $is_in_stock : 0;
                
                $stockItem->setData('qty', $qty)
                          ->setData('is_in_stock', $is_in_stock)
                          ->setData('manage_stock', 1)
                          ->setData('use_config_manage_stock', 0)
                          ->save();
            }
        }
        
        $return['success'] = true;
        $return['msg'] = '<div id="messages"><ul class="messages"><li class="success-msg"><ul><li><span>'.Mage::helper('catalog')->__("The product has been saved.").'</span></li></ul></li></ul></div>';
        
        
        $configProduct = Mage::getModel('catalog/product')->load($configProductId);
        Mage::register('current_product', $configProduct);
        Mage::register('product', $configProduct);
        $edit_single_form = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config');
        
        if($middle_step_matrix_data = $this->getRequest()->getPost('next_step_matrix_data')){
            $edit_single_form->setData('default_attribute', Mage::helper('core')->jsonDecode($middle_step_matrix_data));
        }
        
        if($middle_step_matrix_data = $this->getRequest()->getPost('middle_step_matrix_data')){
            $edit_single_form->setData('others_attr', Mage::helper('core')->jsonDecode($middle_step_matrix_data));
        }
        
        $return['matrix'] = $edit_single_form->toHtml();
        //$return['matrix'] = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config')->toHtml();
        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
    }
    
    protected function getDefaultProductTaxClassId()
    {
        return $this->getDataHelper()->getConfig('default_taxclass');
    }
    
    protected $_dataHelper;
    protected $_dataHelperClass="ecpm";
    protected function getDataHelper()
    {
        if(!$this->_dataHelper){
            $this->_dataHelper = Mage::helper('ecpm');
        }

        return $this->_dataHelper;
    }
    
    protected function getShowedFields()
    {
        $flds = $this->getDataHelper()->getConfig('matrix_fields');
        if($flds != '') $arrFlds = explode(',', $flds);
        else $arrFlds = array();
        if(is_string($arrFlds)) $arrFlds = array($arrFlds);
        return array_filter($arrFlds);
    }
    
    protected function errorReturn($str, $msg = false)
    {
        $strMsg = '';
        if($msg == true){
            $strMsg = Mage::helper('catalog')->__($str);
        } else {
            $strMsg = Mage::helper('catalog')->__("'%s' field is required.", ucfirst($str));
        }

        $return['msg'] = '<div id="messages"><ul class="messages"><li class="error-msg"><ul><li><span>'.$strMsg.'</span></li></ul></li></ul></div>';
        $return['success'] = false;
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
    }
    
    public function deletecheckedAction()
    {
        $productIds = explode(',', $this->getRequest()->getPost('ids'));
        $configProductId = $this->getRequest()->getParam('c_id');
        $currentConfigproduct = Mage::getModel('catalog/product')->load($configProductId);
        
        $deletedCount = 0;
        if(count($productIds)>0)
        {
            // delete configurable product and its parent relation
            $allChildProducts = $currentConfigproduct->getTypeInstance()->getUsedProducts($currentConfigproduct);
            $newProductIds = array();
            foreach($allChildProducts as $allChildProduct){
                if(in_array($allChildProduct->getId(), $productIds)){
                    $deletedCount++;
                    continue;
                }

                $newProductIds[] = $allChildProduct->getId();
            }

            Mage::app()->cleanCache();
            Mage::getResourceModel('catalog/product_type_configurable')
                ->saveProducts($currentConfigproduct, $newProductIds);
            
            $currentConfigproduct->save();
        }

        Mage::app()->cleanCache();
        $currentConfigproduct = Mage::getModel('catalog/product')->load($configProductId);
        
        $return['success'] = true;
        if($deletedCount > 0) $msg = "%d Product(s) has been removed.";
        else $msg = "No product found.";
        
        $return['msg'] = '<div id="messages"><ul class="messages"><li class="success-msg"><ul><li><span>'.Mage::helper('catalog')->__($msg, $deletedCount).'</span></li></ul></li></ul></div>';
        Mage::register('current_product', $currentConfigproduct);
        Mage::register('product', $currentConfigproduct);
        $edit_single_form = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config');
        
        if($middle_step_matrix_data = $this->getRequest()->getPost('next_step_matrix_data')){
            $edit_single_form->setData('default_attribute', Mage::helper('core')->jsonDecode($middle_step_matrix_data));
        }
        
        if($middle_step_matrix_data = $this->getRequest()->getPost('middle_step_matrix_data')){
            $edit_single_form->setData('others_attr', Mage::helper('core')->jsonDecode($middle_step_matrix_data));
        }
        
        $return['matrix'] = $edit_single_form->toHtml();
        //$return['matrix'] = $this->getLayout()->createBlock('ecpm/adminhtml_catalog_product_edit_tab_super_config')->toHtml();
        $linkp = new Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Super_Config();
        $return['linkp'] = Mage::helper('core')->jsonDecode($linkp->getLinksJson());
        $return['attributep'] = Mage::helper('core')->jsonDecode($linkp->getLinksJson());
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
    }
}
