<?php 

class Best4Mage_Ecpm_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected $_extensionActive = false;
    public function isExtensionActive()
    {
        if(!$this->_extensionActive){
            $this->_extensionActive = $this->getConfig('active');
        }

        return $this->_extensionActive;
    }
    
    protected $_base = "ecpm";
    protected $_middle = "basic_options";
    public function getConfig($node, $middle = false)
    {
        if(!$middle){
            $middle = $this->_middle;
        }
        
        return Mage::getStoreConfig($this->_base."/".$middle."/".$node);
    }
    
    public function addNextStepFor()
    {
        return 0;
    }
    
    public function isUsePreselection()
    {
        return ($this->getConfig('preselection') == 1);
    }
    
}
