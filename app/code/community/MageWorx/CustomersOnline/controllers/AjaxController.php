<?php
/**
 * MageWorx
 * Customers Online Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_CustomersOnline
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */
class MageWorx_CustomersOnline_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $helper = Mage::helper('mageworx_customersonline');
        $ref = $helper->getHttpRefferer();
        if (!empty($ref)) {
            $productId = (int)$this->getRequest()->get('id');
            $data = array();
            if ($productId > 0) {
                $product = Mage::getModel('catalog/product')->load($productId);
                if ($product->getId() && stripos($ref, $product->getUrlPath()) > 0) {
                    $model = Mage::getModel('mageworx_customersonline/online')->loadForProduct($product);
                    if ($helper->isDisplayCustomersOnline()) {
                        if ($model->getCountOnlineCustomers() && $model->getCountOnlineCustomers() >= $helper->getOnlineCustomersShowLimit()) {
                            $data['online'] = $helper->getCustomersMessage($model->getCountOnlineCustomers());
                        }
                    }

                    if ($helper->isDisplayRecentOrders()) {
                        if ($model->getLastOrderTimeAgo()) {
                            $data['order'] = $helper->getOrdersMessage($model->getLastOrderTimeAgo(), $model->getLastOrderCountry());
                        }
                     }
                }
            }
            echo json_encode($data);
        }
        exit;
    }

}

