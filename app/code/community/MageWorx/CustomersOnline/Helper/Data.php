<?php
/**
 * MageWorx
 * Customers Online Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_CustomersOnline
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */
class MageWorx_CustomersOnline_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Seconds in minute
     */
    const TIME_MINUTE = 60;

    /**
     * Seconds in hour
     */
    const TIME_HOUR   = 3600;

    /**
     * Seconds in day
     */
    const TIME_DAY    = 86400;

    /**
     * Config display customers online
     *
     * @return bool
     */
    public function isDisplayCustomersOnline()
    {
        return (bool)Mage::getStoreConfig('mageworx_customersonline/main/display_customers_online');
    }

    /**
     * Config display recent orders
     *
     * @return bool
     */
    public function isDisplayRecentOrders()
    {
        return (bool)Mage::getStoreConfig('mageworx_customersonline/main/display_recent_orders');
    }

    /**
     * Get value for popup orders show limit
     *
     * @return int
     */
    public function getLastOrderShowLimit()
    {
        return (int)Mage::getStoreConfig('mageworx_customersonline/main/last_order_limit');
    }

    /**
     * Get value for popup customers show limit
     *
     * @return int
     */
    public function getOnlineCustomersShowLimit()
    {
        return (int)Mage::getStoreConfig('mageworx_customersonline/main/customers_online_limit');
    }

    /**
     * Get value as seconds for hide popup
     *
     * @return int
     */
    public function getHideAfter()
    {
        return (int)Mage::getStoreConfig('mageworx_customersonline/main/hide_after');
    }

    /**
     * Check config setting
     *
     * @return int
     */
    public function isMobileDisable()
    {
        return (bool)Mage::getStoreConfig('mageworx_customersonline/main/mobile_disable');
    }

    /**
     * Check for mobile useragent
     *
     * @return bool
     */
    public function isMobile()
    {
        return Zend_Http_UserAgent_Mobile::match(Mage::helper('core/http')->getHttpUserAgent(), $_SERVER);
    }

    /**
     * Get customers message
     *
     * @param int $number
     * @return string
     */
    public function getCustomersMessage($number)
    {
        return str_replace("[online_number]", $number, (string)Mage::getStoreConfig('mageworx_customersonline/main/customers_message'));
    }

    /**
     * Get recent orders message
     * @param int $time
     * @param string $country
     * @return string
     */
    public function getOrdersMessage($time, $country)
    {
        if ($time < self::TIME_HOUR) {
            $recentDateString = intval($time / self::TIME_MINUTE) . $this->__(" minute(s)");
        } else if ($time < self::TIME_DAY) {
            $recentDateString = intval($time / self::TIME_HOUR) . $this->__(" hour(s)");
        } else {
            $recentDateString = intval($time / self::TIME_DAY) . $this->__(" day(s)");
        }

        return str_replace(array('[recent_date]', '[recent_location]'), array($recentDateString, $country), (string)Mage::getStoreConfig('mageworx_customersonline/main/orders_message'));
    }

    /**
     * Get refferer
     *
     * @return string
     */
    public function getHttpRefferer()
    {
        return strtolower(Mage::helper('core/http')->getHttpReferer(true));
    }

    /**
     * Get order statuses
     *
     * @return array
     */
    public function getOrderStatuses()
    {
        return explode(',', Mage::getStoreConfig('mageworx_customersonline/main/order_status'));
    }
}