<?php
/**
 * MageWorx
 * Customers Online Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_CustomersOnline
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */
class MageWorx_CustomersOnline_Block_Product extends Mage_Core_Block_Template
{
    /**
     * Get URL for ajax request
     *
     * @return string
     */
    public function getAjaxRequestUrl()
    {
        $product = Mage::registry('current_product');
        return Mage::getUrl('customersonline/ajax', array('id' => ($product) ? $product->getId() : 0));
    }
}