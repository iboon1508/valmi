<?php
/**
 * MageWorx
 * Customers Online Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_CustomersOnline
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */
class MageWorx_CustomersOnline_Model_Resource_Online extends Mage_Core_Model_Resource_Db_Abstract
{

    public function _construct()
    {
        $this->_setResource(null);
    }

    /**
     * Load data for product
     *
     * @param MageWorx_CustomersOnline_Model_Online $object
     * @param Mage_Catalog_Model_Product $product
     * @return MageWorx_CustomersOnline_Model_Resource_Online
     */
    public function loadForProduct(MageWorx_CustomersOnline_Model_Online $object, $product)
    {
        $this->_initOnlineCustomers($object, $product)
            ->_initLastOrder($object, $product);
        return $this;
    }

    /**
     * Init online customers
     *
     * @param MageWorx_CustomersOnline_Model_Online $object
     * @param Mage_Catalog_Model_Product $product
     * @return MageWorx_CustomersOnline_Model_Resource_Online
     */
    protected function _initOnlineCustomers(MageWorx_CustomersOnline_Model_Online $object, $product)
    {
        $lastDate = Mage::getModel('core/date')->gmtTimestamp() - Mage::getSingleton('log/visitor_online')->getOnlineInterval() * MageWorx_CustomersOnline_Helper_Data::TIME_MINUTE;
        $readAdapter = $this->_getReadAdapter();

        $select = $readAdapter->select()
            ->from($this->getTable('log/visitor'), array('visitor_id', 'first_visit_at', 'last_visit_at', 'last_url_id'))
            ->where('last_visit_at >= ?', $readAdapter->formatDate($lastDate));

        $query = $readAdapter->query($select);
        $lastUrls = array();
        $path = explode('//', rtrim(Mage::getUrl('catalog/product/view', array('id' => $product->getId())), '/'));
        $count = 0;

        while ($row = $query->fetch()) {
            $lastUrls[$row['last_url_id']] = $row['visitor_id'];
        }

        $select = $readAdapter->select()
            ->from($this->getTable('log/url_info_table'), array('url_id', 'url'))
            ->where('url_id IN(?)', array_keys($lastUrls));

        $query = $readAdapter->query($select);
        while ($row = $query->fetch()) {
            if (strpos($row['url'], $path[1]) > 0) {
                $count++;
            }
        }
        $object->setCountOnlineCustomers($count);
        return $this;
    }

    /**
     * Init last orders
     *
     * @param MageWorx_CustomersOnline_Model_Online $object
     * @param Mage_Catalog_Model_Product $product
     * @return MageWorx_CustomersOnline_Model_Resource_Online
     */
    protected function _initLastOrder(MageWorx_CustomersOnline_Model_Online $object, $product)
    {
        $readAdapter = $this->_getReadAdapter();
        $select = $readAdapter->select()
            ->from($this->getTable('sales/order'), array('created_at', 'entity_id'))
            ->join(array('order_item' => $this->getTable('sales/order_item')), 'order_item.order_id = sales_flat_order.entity_id and order_item.product_id =' . $product->getId(), '')
            ->where('status in(?)', Mage::helper('mageworx_customersonline')->getOrderStatuses())
            ->where('sales_flat_order.store_id = ?', Mage::app()->getStore()->getId())
            ->order('sales_flat_order.created_at DESC')
            ->group('sales_flat_order.entity_id')
            ->limit(1);

        $result = $readAdapter->fetchRow($select);

        if (is_array($result)) {
            $time = Mage::getModel('core/date')->gmtTimestamp() - strtotime($result['created_at']);
            if ($time <= MageWorx_CustomersOnline_Helper_Data::TIME_DAY || intval($time / MageWorx_CustomersOnline_Helper_Data::TIME_DAY) <= Mage::helper('mageworx_customersonline')->getLastOrderShowLimit()) {
                $object->setLastOrderTimeAgo($time);
                $select = $readAdapter->select()
                    ->from($this->getTable('sales/order_address'), array('country_id'))
                    ->where('parent_id = ?', $result['entity_id'])
                    ->where('address_type = ?', 'billing');
                $countryCode = $readAdapter->fetchOne($select);

                $select = $readAdapter->select()
                    ->from($this->getTable('sales/order'), array('remote_ip'))
                    ->where('entity_id = ?', $result['entity_id']);

                $customerIp = $readAdapter->fetchOne($select);
                if ($customerIp) {
                    $geoIp = Mage::getSingleton('mageworx_geoip/geoip');
                    if ($geoIp->getLocation($customerIp)['country']) {
                        $geoIpHelper = Mage::helper('mageworx_geoip');
                        if ($geoIpHelper->isCityDbType()) {
                            if ($geoIp->getLocation($customerIp)['city']) {
                                $object->setLastOrderCountry($geoIp->getLocation($customerIp)['city'] . ', ' . $geoIp->getLocation($customerIp)['country']);
                            } else {
                                $object->setLastOrderCountry($geoIp->getLocation($customerIp)['country']);
                            }
                        } else {
                            $object->setLastOrderCountry($geoIp->getLocation($customerIp)['country']);
                        }
                    }
                } else {
                    if ($countryCode) {
                        $country = Mage::getModel('directory/country')->loadByCode($countryCode);
                        $object->setLastOrderCountry($country->getName());
                    }
                }
            }
        }
        return $this;
    }

}