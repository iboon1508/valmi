<?php
/**
 * MageWorx
 * Customers Online Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_CustomersOnline
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 *
 *
 * @method string getCountOnlineCustomers()
 * @method MageWorx_CustomersOnline_Model_Online setCountOnlineCustomers(int $value)
 * @method string getLastOrderCountry()
 * @method MageWorx_CustomersOnline_Model_Online setLastOrderCountry(string $value)
 * @method string getLastOrderTimeAgo()
 * @method MageWorx_CustomersOnline_Model_Online setLastOrderTimeAgo(int $value)
 *
 */
class MageWorx_CustomersOnline_Model_Online extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('mageworx_customersonline/online');
    }

    public function loadForProduct($product) {
        $this->_getResource()->loadForProduct($this, $product);
        return $this;
    }

}
