<?php
/**
 * MageWorx
 * Customers Online Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_CustomersOnline
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */
class MageWorx_CustomersOnline_Model_System_Config_Source_OrderStatus
{

    public function toOptionArray()
    {
        $collection = Mage::getResourceModel('sales/order_status_collection');
        return $collection->toOptionArray();
    }

}

