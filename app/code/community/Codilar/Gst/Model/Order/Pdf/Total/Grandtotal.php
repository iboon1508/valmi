<?php
/**
 * Codilar Technologies Pvt. Ltd.
 * @category    GST Module
 * @package     GST
 * @copyright   Copyright (c) 20170 .Codilar. (http://www.codilar.com)
 * @purpose     [BRIEF ABOUT THE FILE]
 * @author       Codilar Team
 **/

class Codilar_Gst_Model_Order_Pdf_Total_Grandtotal extends Mage_Tax_Model_Sales_Pdf_Grandtotal
{
    /**
     * Get array of arrays with totals information for display in PDF
     * array(
     *  $index => array(
     *      'amount'   => $amount,
     *      'label'    => $label,
     *      'font_size'=> $font_size
     *  )
     * )
     * @return array
     */
    public function getTotalsForDisplay()
    {
        $store = $this->getOrder()->getStore();
        $store_id =  $this->getOrder()->getStoreId();
        $gst_status = Mage::getStoreConfig('general/gst_options/gst_status',$store_id);
        $config= Mage::getSingleton('tax/config');
        if (!$config->displaySalesTaxWithGrandTotal($store)) {
            return parent::getTotalsForDisplay();
        }
        $amount = $this->getOrder()->formatPriceTxt($this->getAmount());
        $amountExclTax = $this->getAmount() - $this->getSource()->getTaxAmount();
        $amountExclTax = ($amountExclTax > 0) ? $amountExclTax : 0;
        $amountExclTax = $this->getOrder()->formatPriceTxt($amountExclTax);
        $tax = $this->getOrder()->formatPriceTxt($this->getSource()->getTaxAmount());
        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;
        if($gst_status == 1){
            $totals = array(array(
                'amount'    => $this->getAmountPrefix().$amountExclTax,
                'label'     => Mage::helper('tax')->__('Grand Total (Excl. GST)') . ':',
                'font_size' => $fontSize
            ));

           if ($config->displaySalesFullSummary($store)) {
               $totals = array_merge($totals, $this->getFullTaxInfo());
           }

            $totals[] = array(
            'amount'    => $this->getAmountPrefix().$tax,
            'label'     => Mage::helper('tax')->__('Total GST') . ':',
            'font_size' => $fontSize
            );

            $totals[] = array(
                'amount'    => $this->getAmountPrefix().$amount,
                'label'     => Mage::helper('tax')->__('Grand Total (Incl. GST)') . ':',
                'font_size' => $fontSize
            );
            return $totals;
        }else{
            $totals = array(array(
                'amount'    => $this->getAmountPrefix().$amountExclTax,
                'label'     => Mage::helper('tax')->__('Grand Total (Excl. Tax)') . ':',
                'font_size' => $fontSize
            ));

           if ($config->displaySalesFullSummary($store)) {
               $totals = array_merge($totals, $this->getFullTaxInfo());
           }

            $totals[] = array(
            'amount'    => $this->getAmountPrefix().$tax,
            'label'     => Mage::helper('tax')->__('Total Tax') . ':',
            'font_size' => $fontSize
            );

            $totals[] = array(
                'amount'    => $this->getAmountPrefix().$amount,
                'label'     => Mage::helper('tax')->__('Grand Total (Incl. Tax)') . ':',
                'font_size' => $fontSize
            );
            return $totals;
        }
    }


    public function getFullTaxInfo()
    {   
        $fontSize       = $this->getFontSize() ? $this->getFontSize() : 7;
        $taxClassAmount = $this->_getCalculatedTaxes();
        $shippingTax    = $this->_getShippingTax();
        $taxClassAmount = array_merge($taxClassAmount, $shippingTax);
        
        $_order = $this->getOrder();
        $store_id =  $_order->getStoreId();
        $_shippingAddress = $_order->getShippingAddress();
        $shipping_city = $_shippingAddress->getRegion() ;
        $shipping_tax =  $_order->getShippingTaxAmount();
        $tax_inclship = $_order->getTaxAmount();
        $production_state = Mage::getStoreConfig('general/gst_options/gst_state', $store_id);
        $gst_status = Mage::getStoreConfig('general/gst_options/gst_status',$store_id);
        $shipgst_status = Mage::getStoreConfig('general/gst_options/shipgst_status',$store_id);
        if($shipgst_status == 1){
            $tax_exclship = $tax_inclship - $shipping_tax;
        }else{
            $tax_exclship = $tax_inclship;
        }

        if($gst_status == 1){
            if($shipping_city == $production_state){
                $gst_tax_sub = $tax_exclship/2;
                $tax_info[0] = array(
                                'amount'    => $this->getAmountPrefix() . $this->getOrder()->formatPriceTxt($gst_tax_sub),
                                'label'     => 'CGST :',
                                'font_size' => $fontSize
                            );
                $tax_info[1] = array(
                                'amount'    => $this->getAmountPrefix() . $this->getOrder()->formatPriceTxt($gst_tax_sub),
                                'label'     => 'SGST :',
                                'font_size' => $fontSize
                            ); 
                if($shipgst_status == 1){
                    $tax_info[2] = array(
                                'amount'    => $this->getAmountPrefix() . $this->getOrder()->formatPriceTxt($shipping_tax),
                                'label'     => 'Shipping GST :',
                                'font_size' => $fontSize
                            ); 
                }
                $taxClassAmount = $tax_info;
            }else{
                $gst_tax_sub = $tax_exclship;
                $tax_info[0] = array(
                                'amount'    => $this->getAmountPrefix() . $this->getOrder()->formatPriceTxt($gst_tax_sub),
                                'label'     => 'IGST :',
                                'font_size' => $fontSize
                            );
                if($shipgst_status == 1){
                    $tax_info[1] = array(
                                'amount'    => $this->getAmountPrefix() . $this->getOrder()->formatPriceTxt($shipping_tax),
                                'label'     => 'Shipping GST :',
                                'font_size' => $fontSize
                            ); 
                }
                $taxClassAmount = $tax_info;
            }
        }else{
            if (!empty($taxClassAmount)) {
                foreach ($taxClassAmount as &$tax) {
                    $percent          = $tax['percent'] ? ' (' . $tax['percent']. '%)' : '';
                    $tax['amount']    = $this->getAmountPrefix() . $this->getOrder()->formatPriceTxt($tax['tax_amount']);
                    $tax['label']     = $this->_getTaxHelper()->__($tax['title']) . $percent . ':';
                    $tax['font_size'] = $fontSize;
                }
            } else {
                $fullInfo = $this->_getFullRateInfo();
                $tax_info = array();

                if ($fullInfo) {
                    foreach ($fullInfo as $info) {
                        if (isset($info['hidden']) && $info['hidden']) {
                            continue;
                        }

                        $_amount = $info['amount'];

                        foreach ($info['rates'] as $rate) {
                            $percent = $rate['percent'] ? ' (' . $rate['percent']. '%)' : '';

                            $tax_info[] = array(
                                'amount'    => $this->getAmountPrefix() . $this->getOrder()->formatPriceTxt($_amount),
                                'label'     => $this->_getTaxHelper()->__($rate['title']) . $percent . ':',
                                'font_size' => $fontSize
                            );
                        }
                    }
                }
                $taxClassAmount = $tax_info;
            }
    }
        return $taxClassAmount;
    }



}
