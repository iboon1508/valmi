<?php
/**
 * Codilar Technologies Pvt. Ltd.
 * @category    GST Module
 * @package     GST
 * @copyright   Copyright (c) 20170 .Codilar. (http://www.codilar.com)
 * @purpose     [BRIEF ABOUT THE FILE]
 * @author       Codilar Team
 **/

class Codilar_Gst_Model_Observer
{
    /**
     * @param $event
     * @throws Exception
     */
    public function gstAdd($event)
    {

            $order = $event->getEvent()->getOrder();
            $id= $order->getId();
            $store_id =  $order->getStoreId();
            $_shippingAddress = $order->getShippingAddress();
            $shipping_city = $_shippingAddress->getRegion();
            $storeId_site = Mage::app()->getStore()->getStoreId();
            $gst_status = Mage::getStoreConfig('general/gst_options/gst_status',$storeId_site);
            $production_state = Mage::getStoreConfig('general/gst_options/gst_state', $store_id);
            $tax = Mage::getModel('sales/order_tax')->load($id, 'order_id');
            $tax_amount =  $tax->getAmount();
            if(strcasecmp($shipping_city,$production_state) == 0 ){
                $tax_finale = $tax_amount/2;
                $tax_Igst = 0;
            }else{
                $tax_finale = 0;
                $tax_Igst = $tax_amount;
            }
            if($gst_status == 1) {
                $tax->setIgst($tax_Igst)
                    ->setCgst($tax_finale)
                    ->setSgst($tax_finale);
                $tax->save();
            }
        }
}